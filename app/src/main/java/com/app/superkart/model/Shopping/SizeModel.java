package com.app.superkart.model.Shopping;

public class SizeModel {
    String id,size,product_id, attr_name, attr_val, attr_lable, attr_field, added_on,variant_name,variant_price,variant_mrp,variant_qty,status;
    String color,variant_image;

    public String getVariant_image() {
        return variant_image;
    }

    public void setVariant_image(String variant_image) {
        this.variant_image = variant_image;
    }

    boolean hasColor;

    public boolean isHasColor() {
        return hasColor;
    }

    public void setHasColor(boolean hasColor) {
        this.hasColor = hasColor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public String getVariant_price() {
        return variant_price;
    }

    public void setVariant_price(String variant_price) {
        this.variant_price = variant_price;
    }

    public String getVariant_mrp() {
        return variant_mrp;
    }

    public void setVariant_mrp(String variant_mrp) {
        this.variant_mrp = variant_mrp;
    }

    public String getVariant_qty() {
        return variant_qty;
    }

    public void setVariant_qty(String variant_qty) {
        this.variant_qty = variant_qty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SizeModel(String size) {
        this.size = size;
    }

    public SizeModel() {

    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getAttr_name() {
        return attr_name;
    }

    public void setAttr_name(String attr_name) {
        this.attr_name = attr_name;
    }

    public String getAttr_val() {
        return attr_val;
    }

    public void setAttr_val(String attr_val) {
        this.attr_val = attr_val;
    }

    public String getAttr_lable() {
        return attr_lable;
    }

    public void setAttr_lable(String attr_lable) {
        this.attr_lable = attr_lable;
    }

    public String getAttr_field() {
        return attr_field;
    }

    public void setAttr_field(String attr_field) {
        this.attr_field = attr_field;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
