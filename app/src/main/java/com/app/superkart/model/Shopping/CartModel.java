package com.app.superkart.model.Shopping;

public class CartModel {


    String product_id, cart_id, qty, product_name, product_image, regular_price, sale_price, sku, cgst, sgst, igst, description, specification, is_fav, price_off, item_array;
    String vendor_id,c_name,coupon_id,coupon_code,Discount_Type,Discount_Price,coupon_image,From_Date,To_Date,array_product,coupon,userlp,item_price,item_total,array_coupon;
    String loyaltyPoints,requestLP,totalWithoutDisc,discountTotal,promocode,remainingLp;

    public String getRemainingLp() {
        return remainingLp;
    }

    public void setRemainingLp(String remainingLp) {
        this.remainingLp = remainingLp;
    }

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public String getTotalWithoutDisc() {
        return totalWithoutDisc;
    }

    public void setTotalWithoutDisc(String totalWithoutDisc) {
        this.totalWithoutDisc = totalWithoutDisc;
    }

    public String getDiscountTotal() {
        return discountTotal;
    }

    public void setDiscountTotal(String discountTotal) {
        this.discountTotal = discountTotal;
    }

    public String getRequestLP() {
        return requestLP;
    }

    public void setRequestLP(String requestLP) {
        this.requestLP = requestLP;
    }

    public String getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(String loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public String getArray_coupon() {
        return array_coupon;
    }

    public void setArray_coupon(String array_coupon) {
        this.array_coupon = array_coupon;
    }

    public String getItem_price() {
        return item_price;
    }

    public void setItem_price(String item_price) {
        this.item_price = item_price;
    }

    public String getItem_total() {
        return item_total;
    }

    public void setItem_total(String item_total) {
        this.item_total = item_total;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getUserlp() {
        return userlp;
    }

    public void setUserlp(String userlp) {
        this.userlp = userlp;
    }

    public String getArray_product() {
        return array_product;
    }

    public void setArray_product(String array_product) {
        this.array_product = array_product;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getDiscount_Type() {
        return Discount_Type;
    }

    public void setDiscount_Type(String discount_Type) {
        Discount_Type = discount_Type;
    }

    public String getDiscount_Price() {
        return Discount_Price;
    }

    public void setDiscount_Price(String discount_Price) {
        Discount_Price = discount_Price;
    }

    public String getCoupon_image() {
        return coupon_image;
    }

    public void setCoupon_image(String coupon_image) {
        this.coupon_image = coupon_image;
    }

    public String getFrom_Date() {
        return From_Date;
    }

    public void setFrom_Date(String from_Date) {
        From_Date = from_Date;
    }

    public String getTo_Date() {
        return To_Date;
    }

    public void setTo_Date(String to_Date) {
        To_Date = to_Date;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getRegular_price() {
        return regular_price;
    }

    public void setRegular_price(String regular_price) {
        this.regular_price = regular_price;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getIs_fav() {
        return is_fav;
    }

    public void setIs_fav(String is_fav) {
        this.is_fav = is_fav;
    }

    public String getPrice_off() {
        return price_off;
    }

    public void setPrice_off(String price_off) {
        this.price_off = price_off;
    }

    public String getItem_array() {
        return item_array;
    }

    public void setItem_array(String item_array) {
        this.item_array = item_array;
    }

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }
}
