package com.app.superkart.model.Food;

public class Food_Item_Model {
    String price, name;

    public Food_Item_Model(String name,String price) {
        this.name = name;
        this.price = price;

    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
