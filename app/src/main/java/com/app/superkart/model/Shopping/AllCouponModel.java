package com.app.superkart.model.Shopping;

public class AllCouponModel {
    int img_coupon;
    String name;
    String id,newImage_coupon, vendor_id,coupon_code, Discount_Price, Discount_Type, Status, To_Date,From_Date,coupon_pro_id,category_id,Coupon_Type,disc,sellername;

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getSellername() {
        return sellername;
    }

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public String getCoupon_Type() {
        return Coupon_Type;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public void setCoupon_Type(String coupon_Type) {
        Coupon_Type = coupon_Type;
    }

    public String getCoupon_pro_id() {
        return coupon_pro_id;
    }

    public void setCoupon_pro_id(String coupon_pro_id) {
        this.coupon_pro_id = coupon_pro_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getFrom_Date() {
        return From_Date;
    }

    public void setFrom_Date(String from_Date) {
        From_Date = from_Date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }


    public String getDiscount_Price() {
        return Discount_Price;
    }

    public void setDiscount_Price(String discount_Price) {
        Discount_Price = discount_Price;
    }

    public String getDiscount_Type() {
        return Discount_Type;
    }

    public void setDiscount_Type(String discount_Type) {
        Discount_Type = discount_Type;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getTo_Date() {
        return To_Date;
    }

    public void setTo_Date(String to_Date) {
        To_Date = to_Date;
    }

    public String getNewImage_coupon() {
        return newImage_coupon;
    }

    public void setNewImage_coupon(String newImage_coupon) {
        this.newImage_coupon = newImage_coupon;
    }

    public AllCouponModel() {

    }

    public void setImg_coupon(int img_coupon) {
        this.img_coupon = img_coupon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getImg_coupon() {
        return img_coupon;
    }

    public AllCouponModel(int img_coupon, String name) {
        this.img_coupon = img_coupon;
        this.name = name;
    }


}
