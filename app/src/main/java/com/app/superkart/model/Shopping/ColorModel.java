package com.app.superkart.model.Shopping;

public class ColorModel {

    String id, product_id, attr_name, attr_val, attr_lable, attr_field, added_on;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getAttr_name() {
        return attr_name;
    }

    public void setAttr_name(String attr_name) {
        this.attr_name = attr_name;
    }

    public String getAttr_val() {
        return attr_val;
    }

    public void setAttr_val(String attr_val) {
        this.attr_val = attr_val;
    }

    public String getAttr_lable() {
        return attr_lable;
    }

    public void setAttr_lable(String attr_lable) {
        this.attr_lable = attr_lable;
    }

    public String getAttr_field() {
        return attr_field;
    }

    public void setAttr_field(String attr_field) {
        this.attr_field = attr_field;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }
}
