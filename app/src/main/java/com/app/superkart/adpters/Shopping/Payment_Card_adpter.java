package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;

import java.util.List;

public class Payment_Card_adpter extends RecyclerView.Adapter<Payment_Card_adpter.MyViewHolder> {

    private List<String> arrayList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;


        public MyViewHolder(View view) {
            super(view);
            //img_item = view.findViewById(R.id.img_item);
        }
    }

    public Payment_Card_adpter(Context context, List<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpter_payment_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

