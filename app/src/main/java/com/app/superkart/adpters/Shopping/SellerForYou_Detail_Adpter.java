package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.SellerForyouModel;
import com.app.superkart.ui.activity.Shopping.Home_ViewAll_Category_List;
import com.app.superkart.ui.activity.Shopping.Home_View_Fashion_Detail;
import com.app.superkart.utils.AppConstant;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SellerForYou_Detail_Adpter extends RecyclerView.Adapter<SellerForYou_Detail_Adpter.MyViewHolder> {

    private List<SellerForyouModel> arrayList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        TextView txt_name,txt_price;
        CardView card_main1;


        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_name = view.findViewById(R.id.txt_name);
            card_main1 = view.findViewById(R.id.card_main1);
            txt_price = view.findViewById(R.id.txt_price);
        }
    }

    public SellerForYou_Detail_Adpter(Context context, List<SellerForyouModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpter_seller_for_you_detail, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SellerForyouModel model = arrayList.get(position);
        if (model.getProduct_name().equals("")) {
            holder.txt_name.setText("");
        } else {
            holder.txt_name.setText(model.getProduct_name());
        }

        if (model.getSale_price().equals("")) {
            holder.txt_price.setText("");
        } else {
            holder.txt_price.setText("₹"+model.getSale_price());
        }


        if (model.getProduct_image().equals("")) {

        } else {
           /* Picasso.with(context)
                    .load(model.getProduct_image())
                    .into(holder.img_item);
*/
            Glide
                    .with(context)
                    .load(model.getProduct_image())
                    .centerCrop()
                    .into(holder.img_item);
        }
        holder.card_main1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, Home_View_Fashion_Detail.class);
                AppConstant.product_id = model.getProduct_id();
                i.putExtra("product_id", model.getProduct_id());
                i.putExtra("is_fav", "0");
                i.putExtra("storename", model.getStorename());
                i.putExtra("selling_type", model.getSelling_type());
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
