package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HomeBannerAdpter extends PagerAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    ArrayList<String> listimage = new ArrayList<>();

    public HomeBannerAdpter(Context context) {
        this.mContext = context;
    }

   /* public HomeBannerAdpter(FragmentActivity activity, ArrayList<HomeCategoryModel> listimage) {
        this.mContext = activity;
        this.listimage = listimage;
    }*/

    public HomeBannerAdpter(FragmentActivity activity, ArrayList<String> listimage_cateBanner) {
        this.mContext = activity;
        this.listimage = listimage_cateBanner;
    }

    @Override
    public int getCount() {
        return listimage.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.banner_item_adpter, null);
        ImageView imgPhoto = (ImageView) view.findViewById(R.id.image);
        String imageload = listimage.get(position).toString();

       /* Picasso.with(mContext)
                .load(imageload)
                .into(imgPhoto);*/

        if (imageload.equals("") || imageload.equals("null") || imageload.equals(null)) {

        } else {
            Glide
                    .with(mContext)
                    .load(imageload)
                    .centerCrop()
                    .into(imgPhoto);
            // ((ViewPager) container).addView(view);
            container.addView(view, 0);
        }


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}

