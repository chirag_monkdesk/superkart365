package com.app.superkart.adpters.Food;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Food.Food_Item_Model;

import java.util.List;

public class FoodDetailCateAdpter extends RecyclerView.Adapter<FoodDetailCateAdpter.MyViewHolder> {

    private List<Food_Item_Model> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_name, txt_price;
        private RelativeLayout rl_bg;
        private ImageView img_option;

        public MyViewHolder(View view) {
            super(view);
            txt_name = view.findViewById(R.id.txt_name);
            txt_price = view.findViewById(R.id.txt_price);
            rl_bg = view.findViewById(R.id.rl_bg);
            img_option = view.findViewById(R.id.img_option);
        }
    }

    public FoodDetailCateAdpter(Context context, List<Food_Item_Model> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_detail_1_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Food_Item_Model model = arrayList.get(position);
        holder.txt_name.setText(model.getName());
        holder.txt_price.setText(model.getPrice());
        if (pos == position) {
            holder.txt_name.setTextColor(Color.parseColor("#1576CE"));
            holder.img_option.setBackgroundResource(R.drawable.round_bg_blue);
        } else {
            holder.txt_name.setTextColor(Color.parseColor("#424242"));
            holder.img_option.setBackgroundResource(R.drawable.circle_border);
        }
        holder.rl_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}



