package com.app.superkart.adpters.Food;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.ui.activity.Food.RestuarantDetailActivity;

import java.util.List;

public class NearByRastaurantAdpter extends RecyclerView.Adapter<NearByRastaurantAdpter.MyViewHolder> {

    private List<String> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        LinearLayout li_bg;
        public MyViewHolder(View view) {
            super(view);
            li_bg = view.findViewById(R.id.li_bg);
        }
    }

    public NearByRastaurantAdpter(Context context, List<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.nearby_restaurant_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
      /*  AllCouponModel model = arrayList.get(position);

        Glide
                .with(context)
                .load(model.getImg_coupon())
                .centerCrop()
                .into(holder.img_item);
        holder.txt_item.setText(model.getName());*/
        holder.li_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, RestuarantDetailActivity.class);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}




