package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.model.Shopping.OfferModel;
import com.app.superkart.ui.activity.Shopping.Home_View_Fashion_Detail;
import com.app.superkart.utils.AppConstant;
import com.squareup.picasso.Picasso;

import java.util.List;

/*
public class BestBuys_Adpter extends RecyclerView.Adapter<BestBuys_Adpter.MyViewHolder> {

    private List<OfferModel> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_item;
        LinearLayout li_bg;


        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_item = view.findViewById(R.id.txt_item);
            li_bg = view.findViewById(R.id.li_bg);

        }
    }

    public BestBuys_Adpter(Context context, List<OfferModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.best_on_superkart_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        OfferModel model = arrayList.get(position);

        if (model.getProduct_image().equals("")) {

        } else {
            Picasso.with(context)
                    .load(model.getProduct_image())
                    .into(holder.img_item);
        }
        if (model.getProduct_name().equals("")) {
            holder.txt_item.setText("");
        } else {
            holder.txt_item.setText(model.getProduct_name());
        }

        holder.li_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.check_flow="fromHome";
                Intent i = new Intent(context, Home_View_Fashion_Detail.class);
                i.putExtra("product_id", arrayList.get(position).getProduct_id());
                i.putExtra("storename", arrayList.get(position).getStorename());
                i.putExtra("selling_type", arrayList.get(position).getSelling_type());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}




*/
