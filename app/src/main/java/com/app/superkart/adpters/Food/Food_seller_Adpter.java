package com.app.superkart.adpters.Food;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.model.Shopping.SellerForyouModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Food_seller_Adpter extends RecyclerView.Adapter<Food_seller_Adpter.MyViewHolder> {

    private List<SellerForyouModel> arrayList;
    private Context context;
   // int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_storeName;
        LinearLayout li_bg;
        CardView card_main1;

        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_storeName = view.findViewById(R.id.txt_storeName);
            li_bg = view.findViewById(R.id.li_bg);
            card_main1 = view.findViewById(R.id.card_main1);

        }
    }

    public Food_seller_Adpter(Context context, List<SellerForyouModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_food_seller_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        SellerForyouModel model = arrayList.get(position);


        Picasso.with(context)
                .load(model.getSeller_image())
                .into(holder.img_item);
        holder.txt_storeName.setText(model.getSeller_name());

    /*    if (pos == position) {
            holder.card_main1.setBackgroundResource(R.drawable.food_bg);
            holder.li_bg.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
            holder.txt_storeName.setTextColor(Color.parseColor("#FFFFFFFF"));
        } else {
            holder.card_main1.setBackgroundResource(R.drawable.food_bg_white);
            holder.li_bg.setBackgroundColor(Color.parseColor("#ECECEC"));
            holder.txt_storeName.setTextColor(Color.parseColor("#FF000000"));
        }*/

        holder.card_main1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              // pos = position;
                //notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

