package com.app.superkart.adpters.Food;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.AllCouponModel;
import com.app.superkart.ui.activity.Shopping.Home_View_Fashion_Detail;
import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Random;

public class FoodCouponAdpter extends RecyclerView.Adapter<FoodCouponAdpter.MyViewHolder> {

    private List<AllCouponModel> arrayList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_1, txt_2;
        LinearLayout li_bg;

        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_1 = view.findViewById(R.id.txt_1);
            txt_2 = view.findViewById(R.id.txt_2);
            li_bg = view.findViewById(R.id.li_bg);

        }
    }

    public FoodCouponAdpter(Context context, List<AllCouponModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_coupon_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AllCouponModel model = arrayList.get(position);

          /*  Picasso.with(context)
                    .load(model.getNewImage_coupon())
                    .into(holder.img_item);*/

        Random r = new Random();
        int red = r.nextInt(255 - 0 + 1) + 0;
        int green = r.nextInt(255 - 0 + 1) + 0;
        int blue = r.nextInt(255 - 0 + 1) + 0;



        GradientDrawable draw2 = new GradientDrawable();
        draw2.setShape(GradientDrawable.OVAL);
        draw2.setColor(Color.rgb(red, green, blue));
        holder.li_bg.setBackgroundColor(Color.rgb(red, green, blue));


        Glide
                .with(context)
                .load(model.getImg_coupon())
                .centerCrop()
                .into(holder.img_item);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

