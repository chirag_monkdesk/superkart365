package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.ProductListModel;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Fashion_Address_Item_Adpter extends RecyclerView.Adapter<Fashion_Address_Item_Adpter.MyViewHolder> {

    private List<ProductListModel> arrayList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_item;
        private TextView txt_qty, txt_name, txt_total_price;

        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_qty = view.findViewById(R.id.txt_qty);
            txt_name = view.findViewById(R.id.txt_name);
            txt_total_price = view.findViewById(R.id.txt_total_price);
        }
    }

    public Fashion_Address_Item_Adpter(Context context, List<ProductListModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpter_fashion_address_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ProductListModel model = arrayList.get(position);
        if (model.getProduct_image().equals("")) {

        } else {
           /* Picasso.with(context)
                    .load(model.getProduct_image())
                    .into(holder.img_item);*/

            Glide
                    .with(context)
                    .load(model.getProduct_image())
                    .centerCrop()
                    .into(holder.img_item);
        }

        if (model.getProduct_name().equals("")) {
            holder.txt_name.setText("");
        } else {
            holder.txt_name.setText(model.getProduct_name());
        }

        if (model.getQty().equals("")) {
            holder.txt_qty.setText("");
        } else {
            holder.txt_qty.setText(model.getQty());
        }

        if (model.getSale_price().equals("")) {
            holder.txt_total_price.setText("");
        } else {
            holder.txt_total_price.setText("₹" + model.getSale_price());
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
