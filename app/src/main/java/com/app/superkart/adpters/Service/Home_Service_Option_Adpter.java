package com.app.superkart.adpters.Service;

import android.content.Context;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Service.ServiceModel;


import java.util.List;

public class Home_Service_Option_Adpter extends RecyclerView.Adapter<Home_Service_Option_Adpter.MyViewHolder> {

    private List<ServiceModel> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_item;
        LinearLayout li_bg;


        public MyViewHolder(View view) {
            super(view);
            txt_item = view.findViewById(R.id.txt_item);
            li_bg = view.findViewById(R.id.li_bg);

        }
    }

    public Home_Service_Option_Adpter(Context context, List<ServiceModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_service_detail_option2, parent, false);

        return new Home_Service_Option_Adpter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Home_Service_Option_Adpter.MyViewHolder holder, int position) {
        ServiceModel model = arrayList.get(position);


        holder.txt_item.setText(model.getName());

        if (pos == position) {
            holder.li_bg.setBackgroundResource(R.drawable.box_corner_blue);
            holder.txt_item.setTextColor(Color.parseColor("#FFFFFFFF"));
        } else {
            holder.li_bg.setBackgroundResource(R.drawable.bg_corner_gray);
            holder.txt_item.setTextColor(Color.parseColor("#9e9e9e"));
        }

        holder.li_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                notifyDataSetChanged();

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}



