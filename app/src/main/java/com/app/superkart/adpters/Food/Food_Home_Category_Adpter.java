package com.app.superkart.adpters.Food;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;

import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

public class Food_Home_Category_Adpter extends RecyclerView.Adapter<Food_Home_Category_Adpter.MyViewHolder> {

    private List<HomeCategoryModel> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_item;
        LinearLayout li_bg;
        CardView card_main1, card_item;

        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_item = view.findViewById(R.id.txt_item);
            li_bg = view.findViewById(R.id.li_bg);
            card_main1 = view.findViewById(R.id.card_main1);
            card_item = view.findViewById(R.id.card_item);

        }
    }

    public Food_Home_Category_Adpter(Context context, List<HomeCategoryModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_home_category_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        HomeCategoryModel model = arrayList.get(position);


        Picasso.with(context)
                .load(model.getImgg_category())
                .into(holder.img_item);
        holder.txt_item.setText(model.getItem_name());

        if (pos == position) {
            holder.card_main1.setBackgroundResource(R.drawable.food_bg);
            holder.li_bg.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
            holder.txt_item.setTextColor(Color.parseColor("#FFFFFFFF"));
        } else {
            holder.card_main1.setBackgroundResource(R.drawable.food_bg_white);
            holder.li_bg.setBackgroundColor(Color.parseColor("#ECECEC"));
            holder.txt_item.setTextColor(Color.parseColor("#FF000000"));
        }

        holder.card_main1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
               notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
