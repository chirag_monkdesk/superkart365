package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.model.Shopping.SellerForyouModel;
import com.app.superkart.ui.activity.Shopping.AllSubProductActivity;
import com.app.superkart.ui.activity.Shopping.Home_ViewAll_Category_List;
import com.app.superkart.ui.activity.Shopping.SellerForyouActivity;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class Tab_Category_Shopping_Adpter extends RecyclerView.Adapter<Tab_Category_Shopping_Adpter.MyViewHolder> {

    private List<HomeCategoryModel> arrayList;
    List<HomeCategoryModel> local_array = null;
    private Context context;
    int pos = 0;
    private ItemFilter mFilter = new ItemFilter();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_item;
        LinearLayout li_bg;


        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_item = view.findViewById(R.id.txt_item);
            li_bg = view.findViewById(R.id.li_bg);

        }
    }

    public Tab_Category_Shopping_Adpter(Context context, ArrayList<HomeCategoryModel> list) {
        this.context = context;
        this.arrayList = list;
        this.local_array = list;
    }

    @Override
    public Tab_Category_Shopping_Adpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpter_tab_category, parent, false);

        return new Tab_Category_Shopping_Adpter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Tab_Category_Shopping_Adpter.MyViewHolder holder, int position) {
        HomeCategoryModel model = local_array.get(position);


        Glide
                .with(context)
                .load(local_array.get(position).getCate_image())
                .centerCrop()
                .into(holder.img_item);


      /*  Picasso.with(context)
                .load(local_array.get(position).getCate_image())
                .into(holder.img_item);*/

        holder.txt_item.setText(local_array.get(position).getItem_name());

        if (pos == position) {
            holder.li_bg.setBackgroundResource(R.drawable.border_blue);
            holder.txt_item.setTextColor(Color.parseColor("#1576CE"));
        } else {
            holder.li_bg.setBackgroundResource(R.drawable.border_gray);
            holder.txt_item.setTextColor(Color.parseColor("#FF000000"));
        }

        holder.li_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                notifyDataSetChanged();
                if (model.getIs_parent().equals("1")) {
                    Intent i = new Intent(context, AllSubProductActivity.class);
                    i.putExtra("category_id", model.getCategory_id());
                    i.putExtra("offer_title", model.getItem_name());
                    context.startActivity(i);
                } else {
                    Intent i = new Intent(context, Home_ViewAll_Category_List.class);
                    i.putExtra("category_id", model.getCategory_id());
                    i.putExtra("offer_title", model.getItem_name());
                    context.startActivity(i);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return local_array.size();
    }

    public void resetData() {
        local_array = arrayList;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<HomeCategoryModel> list = arrayList;

            int count = list.size();
            final ArrayList<HomeCategoryModel> nlist = new ArrayList<>(count);

            if (constraint.length() == 0) {
                nlist.addAll(arrayList);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final HomeCategoryModel mWords : arrayList) {
                    if ((mWords.getItem_name().toLowerCase().contains(filterPattern))) {
                        nlist.add(mWords);
                    }
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            local_array = (List<HomeCategoryModel>) results.values;
            notifyDataSetChanged();
        }

    }
}


