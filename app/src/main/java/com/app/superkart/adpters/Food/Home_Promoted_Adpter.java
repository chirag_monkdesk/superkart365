package com.app.superkart.adpters.Food;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.AllCouponModel;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.ui.activity.Food.RestuarantDetailActivity;
import com.bumptech.glide.Glide;

import java.util.List;

public class Home_Promoted_Adpter extends RecyclerView.Adapter<Home_Promoted_Adpter.MyViewHolder> {

    private List<AllCouponModel> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_item;
        public TextView txt_item;
        LinearLayout li_bg;

        public MyViewHolder(View view) {
            super(view);

            img_item = view.findViewById(R.id.img_item);
            txt_item = view.findViewById(R.id.txt_item);
            li_bg = view.findViewById(R.id.li_bg);
        }
    }

    public Home_Promoted_Adpter(Context context, List<AllCouponModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.promoted_restaurant_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AllCouponModel model = arrayList.get(position);

        Glide
                .with(context)
                .load(model.getImg_coupon())
                .centerCrop()
                .into(holder.img_item);
        holder.txt_item.setText(model.getName());

        holder.li_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, RestuarantDetailActivity.class);
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}



