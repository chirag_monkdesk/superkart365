package com.app.superkart.adpters.Food;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.app.superkart.R;
import java.util.List;

public class AddFoodItemsAdpter extends RecyclerView.Adapter<AddFoodItemsAdpter.MyViewHolder> {

    private List<String> arrayList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        public MyViewHolder(View view) {

            super(view);

        }
    }

    public AddFoodItemsAdpter(Context context, List<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public AddFoodItemsAdpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_food_items_list_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}


