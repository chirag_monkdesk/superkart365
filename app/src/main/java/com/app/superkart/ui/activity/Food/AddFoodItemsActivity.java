package com.app.superkart.ui.activity.Food;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.adpters.Food.AddFoodItemsAdpter;
import com.app.superkart.adpters.Food.FoodDetailAdpter;
import com.app.superkart.ui.activity.Shopping.Fashion_Addresss_Activity;
import com.app.superkart.utils.Utils;

import java.util.ArrayList;

public class AddFoodItemsActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private RecyclerView recycle_item;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<String> arrayList = new ArrayList<>();
    private AddFoodItemsAdpter addFoodItemsAdpter;
    private TextView btn_placeOrder, txt_delivery, txt_take_away;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.add_food_items_list_layout);
        Log.e("AddFoodItemsActivity", "AddFoodItemsActivity");
        init();
        for (int i = 0; i < 2; i++) {
            arrayList.add("a");
        }
        recycle_item = findViewById(R.id.recycle_item);
        linearLayoutManager = new LinearLayoutManager(AddFoodItemsActivity.this, LinearLayoutManager.VERTICAL, false);
        recycle_item.setLayoutManager(linearLayoutManager);
        addFoodItemsAdpter = new AddFoodItemsAdpter(AddFoodItemsActivity.this, arrayList);
        recycle_item.setAdapter(addFoodItemsAdpter);
        txt_delivery.setBackgroundResource(R.drawable.bg_corner_yellow);
        txt_take_away.setBackgroundResource(R.drawable.boc_corner);
        txt_delivery.setTextColor(Color.parseColor("#FFFFFFFF"));
        txt_take_away.setTextColor(Color.parseColor("#FF000000"));
    }

    public void init() {
        img_back = findViewById(R.id.img_back);
        btn_placeOrder = findViewById(R.id.btn_placeOrder);
        txt_delivery = findViewById(R.id.txt_delivery);
        txt_take_away = findViewById(R.id.txt_take_away);
        img_back.setOnClickListener(this);
        btn_placeOrder.setOnClickListener(this);
        txt_delivery.setOnClickListener(this);
        txt_take_away.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.txt_delivery:
                txt_delivery.setBackgroundResource(R.drawable.bg_corner_yellow);
                txt_take_away.setBackgroundResource(R.drawable.boc_corner);
                txt_delivery.setTextColor(Color.parseColor("#FFFFFFFF"));
                txt_take_away.setTextColor(Color.parseColor("#FF000000"));
                break;
            case R.id.txt_take_away:
                txt_delivery.setBackgroundResource(R.drawable.boc_corner);
                txt_take_away.setBackgroundResource(R.drawable.bg_corner_yellow);
                txt_delivery.setTextColor(Color.parseColor("#FF000000"));
                txt_take_away.setTextColor(Color.parseColor("#FFFFFFFF"));
                Intent intent = new Intent(AddFoodItemsActivity.this, Food_Take_Away_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn_placeOrder:
                Intent i = new Intent(AddFoodItemsActivity.this, Food_Payemnt_Activity.class);
                startActivity(i);
                break;
        }
    }
}

