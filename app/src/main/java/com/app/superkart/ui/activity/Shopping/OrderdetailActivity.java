package com.app.superkart.ui.activity.Shopping;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.OrderModel;
import com.app.superkart.model.Shopping.ShipModel;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.Signup.LoginActivity;
import com.app.superkart.ui.activity.WebView_Activity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderdetailActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private TextView txt_orderid, txt_sellerName, txt_TotalAmount, txt_paymentmode, txt_ShippingCharge, txt_Discount, txt_lp, txt_order_status;
    private RecyclerView recycle_list;
    private LinearLayoutManager linearLayoutManager;

    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private ArrayList<OrderModel> arrayList = new ArrayList();
    private ArrayList<ShipModel> arrayList_track = new ArrayList();
    private OrderListDeatilAdpter orderListDeatilAdpter;
    private OrderTrackAdpter orderTrackAdpter;
    private String order_id = "", order_id_full = "", payment_mode = "", order_status = "";
    private ArrayList<ShipModel> arrayList_ship = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.order_detail_layout);
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        Log.e("OrderdetailActivity", "OrderdetailActivity");
        init();
        intentData();
        if (Utils.isNetworkAvailable(OrderdetailActivity.this)) {
            orderdetail();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
    }

    public void init() {
        txt_orderid = findViewById(R.id.txt_orderid);
        txt_sellerName = findViewById(R.id.txt_sellerName);
        txt_TotalAmount = findViewById(R.id.txt_TotalAmount);
        txt_paymentmode = findViewById(R.id.txt_paymentmode);
        txt_ShippingCharge = findViewById(R.id.txt_ShippingCharge);
        txt_Discount = findViewById(R.id.txt_Discount);
        txt_order_status = findViewById(R.id.txt_order_status);
        txt_lp = findViewById(R.id.txt_lp);
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_list = findViewById(R.id.recycle_list);
        linearLayoutManager = new LinearLayoutManager(OrderdetailActivity.this, RecyclerView.VERTICAL, false);
        recycle_list.setLayoutManager(linearLayoutManager);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

    public void intentData() {
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            order_id_full = (String) bd.get("order_id_full");
            payment_mode = (String) bd.get("payment_mode");
            order_status = (String) bd.get("order_status");
            Log.e("order_status",""+order_status);
            if (order_status.equals("0")) {
                txt_order_status.setText("Cancelled");
                txt_order_status.setVisibility(View.VISIBLE);
            } else {
                txt_order_status.setVisibility(View.GONE);
            }
            txt_orderid.setText("Order ID: " + order_id_full);
            txt_paymentmode.setText(payment_mode);
            if (payment_mode.equals("online")) {
                txt_paymentmode.setTextColor(Color.parseColor("#8fe66e"));
            }

        }
    }

    private void orderdetail() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.orderdetail, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            OrderModel model = new OrderModel();
                            model.setId(array_result.optJSONObject(i).optString("id"));
                            model.setOrder_id(array_result.optJSONObject(i).optString("order_id"));
                            model.setProduct_name(array_result.optJSONObject(i).optString("product_name"));
                            model.setProduct_image(array_result.optJSONObject(i).optString("product_image"));
                            model.setOrder_date(array_result.optJSONObject(i).optString("order_date"));
                            model.setPrice(array_result.optJSONObject(i).optString("price"));
                            model.setObj_shipping(array_result.optJSONObject(i).optString("shipping"));
                            model.setIs_return(array_result.optJSONObject(i).optString("is_return"));
                            model.setIs_shipping(array_result.optJSONObject(i).optString("is_shipping"));
                            model.setTrack_order_url(array_result.optJSONObject(i).optString("track_order_url"));
                            model.setAttrubute(array_result.optJSONObject(i).optString("attrubute"));
                            arrayList.add(model);
                        }

                        if (arrayList.size() > 0) {
                            recycle_list.setVisibility(View.VISIBLE);
                            orderListDeatilAdpter = new OrderListDeatilAdpter(OrderdetailActivity.this, arrayList);
                            recycle_list.setAdapter(orderListDeatilAdpter);
                        } else {
                            recycle_list.setVisibility(View.GONE);
                        }

                        if (json_main.optString("storename").equals("") || json_main.optString("storename").equals("null") || json_main.optString("storename").equals(null)) {
                            txt_sellerName.setText("");
                            txt_sellerName.setVisibility(View.GONE);
                        } else {
                            txt_sellerName.setVisibility(View.VISIBLE);
                            txt_sellerName.setText("Seller: " + json_main.optString("storename"));
                        }

                        if (json_main.optString("discount_amount").equals("") || json_main.optString("discount_amount").equals("null") || json_main.optString("discount_amount").equals(null)) {
                            txt_Discount.setText("Discount: " + "₹0");
                        } else {
                            txt_Discount.setText("Discount: " + "₹" + json_main.optString("discount_amount"));
                        }

                        if (json_main.optString("shipping_charge").equals("") || json_main.optString("shipping_charge").equals("null") || json_main.optString("shipping_charge").equals(null)) {
                            txt_ShippingCharge.setText("Shipping Charges: ₹0");
                        } else {
                            txt_ShippingCharge.setText("Shipping Charges: " + "₹" + json_main.optString("shipping_charge"));
                        }

                        if (json_main.optString("loyaltyPoint").equals("") || json_main.optString("loyaltyPoint").equals("null") || json_main.optString("loyaltyPoint").equals(null)) {
                            txt_lp.setText("Loyalty Points: " + "₹0");
                        } else {
                            txt_lp.setText("Loyalty Points: " + "₹" + json_main.optString("loyaltyPoint"));
                        }

                        if (json_main.optString("total").equals("") || json_main.optString("total").equals("null") || json_main.optString("total").equals(null)) {
                            txt_TotalAmount.setText("");
                        } else {
                            txt_TotalAmount.setText("Total: " + "₹" + json_main.optString("total"));
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("order_id", AppConstant.order_id_deatil);
                Log.e("params", "" + Constants.orderdetail + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.orderdetail);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class OrderListDeatilAdpter extends RecyclerView.Adapter<OrderListDeatilAdpter.MyViewHolder> {

        private List<OrderModel> arrayList;
        private Context context;
        private LinearLayoutManager linearLayoutManager1;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_item;
            private TextView txt_productName, txt_price, txt_Orderdate, txt_sellerName, txt_status, txt_color, txt_size, btn_track_order, btn_return_order;
            private RecyclerView recycle_trackOrder;
            private LinearLayout li_attribute;

            public MyViewHolder(View view) {
                super(view);
                txt_productName = view.findViewById(R.id.txt_productName);
                img_item = view.findViewById(R.id.img_item);
                txt_price = view.findViewById(R.id.txt_price);
                txt_Orderdate = view.findViewById(R.id.txt_Orderdate);
                txt_sellerName = view.findViewById(R.id.txt_sellerName);
                recycle_trackOrder = view.findViewById(R.id.recycle_trackOrder);
                txt_status = view.findViewById(R.id.txt_status);
                li_attribute = view.findViewById(R.id.li_attribute);
                txt_color = view.findViewById(R.id.txt_color);
                txt_size = view.findViewById(R.id.txt_size);
                btn_track_order = view.findViewById(R.id.btn_track_order);
                btn_return_order = view.findViewById(R.id.btn_return_order);
            }
        }

        public OrderListDeatilAdpter(Context context, List<OrderModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_detail_item_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            OrderModel model = arrayList.get(position);
            if (model.getProduct_name().equals("") | model.getProduct_name().equals("null") || model.getProduct_name().equals(null)) {
                holder.txt_productName.setText("");
            } else {
                holder.txt_productName.setText(model.getProduct_name());
            }
            if (model.getProduct_image().equals("") | model.getProduct_image().equals("null") || model.getProduct_image().equals(null)) {

            } else {
                /*Picasso.with(context)
                        .load(model.getProduct_image())
                        .into(holder.img_item);*/
                Glide
                        .with(context)
                        .load(model.getProduct_image())
                        .centerCrop()
                        .into(holder.img_item);

            }
            if (model.getPrice().equals("") | model.getPrice().equals("null") || model.getPrice().equals(null)) {
                holder.txt_price.setText("");
            } else {
                holder.txt_price.setText("Price: " + "₹" + model.getPrice());
            }

            if (model.getOrder_date().equals("") | model.getOrder_date().equals("null") || model.getOrder_date().equals(null)) {
                holder.txt_Orderdate.setText("");
            } else {
                holder.txt_Orderdate.setText("Order Date: " + model.getOrder_date());
            }

            if (model.getAttrubute().equals("") || model.getAttrubute().equals("null") || model.getAttrubute().equals(null)) {
                holder.li_attribute.setVisibility(View.GONE);
            } else {
                holder.li_attribute.setVisibility(View.VISIBLE);
                try {
                    JSONObject obj = new JSONObject(model.getAttrubute());
                    if (obj.optBoolean("hasColor") == true) {
                        holder.txt_color.setVisibility(View.VISIBLE);
                        holder.txt_color.setBackgroundColor(Color.parseColor(obj.optString("color")));
                    } else {
                        holder.txt_color.setVisibility(View.GONE);
                    }

                    if (obj.optString("variant_name").equals("") || obj.optString("variant_name").equals("null")) {
                        holder.txt_size.setVisibility(View.GONE);
                    } else {
                        holder.txt_size.setVisibility(View.VISIBLE);
                        holder.txt_size.setText(obj.optString("variant_name"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (model.getIs_shipping().equals("Delivered") || model.getIs_shipping().equals("Delivery")) {
                holder.btn_return_order.setVisibility(View.VISIBLE);
            } else {
                holder.btn_return_order.setVisibility(View.GONE);
            }

            if (model.getIs_return().equals("1")) {
                holder.btn_return_order.setVisibility(View.VISIBLE);
                holder.btn_return_order.setEnabled(false);
                Toast.makeText(context, "This product is already returned.", Toast.LENGTH_LONG).show();
            } else {
                holder.btn_return_order.setEnabled(true);
                holder.btn_return_order.setVisibility(View.GONE);
            }

            if (order_status.equals("0")) {
                holder.btn_track_order.setEnabled(false);
                holder.btn_track_order.setTextColor(Color.parseColor("#e0e0e0"));
            } else {
                holder.btn_track_order.setEnabled(true);
                holder.btn_track_order.setTextColor(Color.parseColor("#2cb02c"));
            }

            holder.btn_track_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.order_track_url = model.getTrack_order_url();
                    Intent i = new Intent(OrderdetailActivity.this, WebView_Activity.class);
                    startActivity(i);
                }
            });

            holder.btn_return_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isNetworkAvailable(OrderdetailActivity.this)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(OrderdetailActivity.this, R.style.AppCompatAlertDialogStyle);
                        builder.setTitle(R.string.app_name);
                        builder.setMessage("Are you sure want to return this product?");
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                if (Utils.isNetworkAvailable(OrderdetailActivity.this)) {
                                    returnOrder();
                                } else {
                                    Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface arg0) {
                                alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
                                alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                                //dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.black));
                            }
                        });
                        alert.show();

                    } else {
                        Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                    }
                }
            });

            try {
                arrayList_ship.clear();
                JSONObject obj_ship = new JSONObject(model.getObj_shipping());
                JSONArray array = obj_ship.optJSONArray("shippment_data");
                for (int i = 0; i < array.length(); i++) {
                    ShipModel shipModel = new ShipModel();
                    shipModel.setOrder_status(array.optJSONObject(i).optString("order_status"));
                    shipModel.setShippment_status(array.optJSONObject(i).optString("shippment_status"));
                    shipModel.setDate(array.optJSONObject(i).optString("date"));
                    shipModel.setTime(array.optJSONObject(i).optString("time"));
                    shipModel.setMsg(array.optJSONObject(i).optString("msg"));
                    arrayList_ship.add(shipModel);
                }
                if (arrayList_ship.size() > 0) {
                    linearLayoutManager1 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                    holder.recycle_trackOrder.setLayoutManager(linearLayoutManager1);
                    orderTrackAdpter = new OrderTrackAdpter(OrderdetailActivity.this, arrayList_ship);
                    holder.recycle_trackOrder.setAdapter(orderTrackAdpter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }


    private void returnOrder() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.returnOrder, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("order_id", AppConstant.order_id_deatil);
                Log.e("params", "" + Constants.returnOrder + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.returnOrder);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class OrderTrackAdpter extends RecyclerView.Adapter<OrderTrackAdpter.MyViewHolder> {

        private List<ShipModel> arrayList;
        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_status, txt_line;
            private TextView txt_status, txt_date;

            public MyViewHolder(View view) {
                super(view);
                img_status = view.findViewById(R.id.img_status);
                txt_line = view.findViewById(R.id.txt_line);
                txt_status = view.findViewById(R.id.txt_status);
                txt_date = view.findViewById(R.id.txt_date);
            }
        }

        public OrderTrackAdpter(Context context, List<ShipModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_track_order_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ShipModel model = arrayList.get(position);

            if (model.getOrder_status().equals("Accepted")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setBackgroundResource(R.drawable.line_green);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            } else if (model.getOrder_status().equals("Process")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setBackgroundResource(R.drawable.line_green);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            } else if (model.getOrder_status().equals("Packed")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setBackgroundResource(R.drawable.line_green);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            } else if (model.getOrder_status().equals("Shipped")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setBackgroundResource(R.drawable.line_green);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            } else if (model.getOrder_status().equals("Delivered")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setVisibility(View.INVISIBLE);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            }

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

}
