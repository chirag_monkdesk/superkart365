package com.app.superkart.ui.activity.Shopping;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.OrderModel;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.Signup.LoginActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyOrderListActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private RecyclerView recycle_list;
    private LinearLayoutManager linearLayoutManager;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private ArrayList<OrderModel> arrayList = new ArrayList();
    private OrderListAdpter orderListAdpter;
    private ImageView img_back;
    private LinearLayout li_error_msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.my_order_list_layout);
        Log.e("MyOrderListActivity", "MyOrderListActivity");
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        init();
        if (Utils.isNetworkAvailable(MyOrderListActivity.this)) {
            orderhistory();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
    }

    public void init() {
        img_back = findViewById(R.id.img_back);
        li_error_msg = findViewById(R.id.li_error_msg);
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_list = findViewById(R.id.recycle_list);
        linearLayoutManager = new LinearLayoutManager(MyOrderListActivity.this, RecyclerView.VERTICAL, false);
        recycle_list.setLayoutManager(linearLayoutManager);

        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                if (AppConstant.chk_payment_view.equals("check")) {
                    Intent intent = new Intent(MyOrderListActivity.this, BaseActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    AppConstant.chk_payment_view = "";
                    finish();
                }

        }
    }

    private void orderhistory() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.orderhistory, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    li_error_msg.setVisibility(View.GONE);
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            OrderModel model = new OrderModel();
                            model.setId(array_result.optJSONObject(i).optString("id"));
                            model.setOrder_id(array_result.optJSONObject(i).optString("order_id"));
                            model.setItem(array_result.optJSONObject(i).optString("item"));
                            model.setOrder_date(array_result.optJSONObject(i).optString("order_date"));
                            model.setPayment_mode(array_result.optJSONObject(i).optString("payment_mode"));
                            model.setOrder_status(array_result.optJSONObject(i).optString("order_status"));
                            arrayList.add(model);
                        }
                        if (arrayList.size() > 0) {
                            recycle_list.setVisibility(View.VISIBLE);
                            orderListAdpter = new OrderListAdpter(MyOrderListActivity.this, arrayList);
                            recycle_list.setAdapter(orderListAdpter);
                            orderListAdpter.notifyDataSetChanged();
                        } else {
                            recycle_list.setVisibility(View.GONE);
                            li_error_msg.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recycle_list.setVisibility(View.GONE);
                        li_error_msg.setVisibility(View.VISIBLE);

                        //Toast.makeText(MyOrderListActivity.this, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.orderhistory + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.orderhistory);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    public class OrderListAdpter extends RecyclerView.Adapter<OrderListAdpter.MyViewHolder> {

        private List<OrderModel> arrayList;
        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_status, img_product;
            private TextView txt_productName, txt_date, txt_orderid, txt_item, txt_cancel_btn;
            private CardView card_main1;

            public MyViewHolder(View view) {
                super(view);
                img_status = view.findViewById(R.id.img_status);
                img_product = view.findViewById(R.id.img_product);
                txt_productName = view.findViewById(R.id.txt_productName);
                txt_date = view.findViewById(R.id.txt_date);
                txt_orderid = view.findViewById(R.id.txt_orderid);
                txt_item = view.findViewById(R.id.txt_item);
                txt_cancel_btn = view.findViewById(R.id.txt_cancel_btn);
                card_main1 = view.findViewById(R.id.card_main1);
            }
        }

        public OrderListAdpter(Context context, List<OrderModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.my_orderlist_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            OrderModel model = arrayList.get(position);


            if (model.getItem().equals("") || model.getItem().equals("null") || model.getItem().equals(null)) {
                holder.txt_item.setText("");
            } else {
                holder.txt_item.setText(model.getItem());
            }

            if (model.getOrder_date().equals("") || model.getOrder_date().equals("null") || model.getOrder_date().equals(null)) {
                holder.txt_date.setText("");
            } else {
                holder.txt_date.setText(model.getOrder_date());
            }

            if (model.getOrder_id().equals("") || model.getOrder_id().equals("null") || model.getOrder_id().equals(null)) {
                holder.txt_orderid.setText("");
            } else {
                holder.txt_orderid.setText(model.getOrder_id());
            }

            if (model.getOrder_status().equals("0")) {
                holder.txt_cancel_btn.setBackgroundColor(Color.parseColor("#EA4335"));
                holder.txt_cancel_btn.setEnabled(false);
            } else {
                holder.txt_cancel_btn.setBackgroundColor(Color.parseColor("#3eb51b"));
                holder.txt_cancel_btn.setEnabled(true);
            }


            holder.card_main1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.order_id_deatil = model.getId();
                    Intent i = new Intent(context, OrderdetailActivity.class);
                    i.putExtra("order_id_full", model.getOrder_id());
                    i.putExtra("payment_mode", model.getPayment_mode());
                    i.putExtra("order_status", model.getOrder_status());
                    context.startActivity(i);
                }
            });
            holder.txt_cancel_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MyOrderListActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle(R.string.app_name);
                    builder.setMessage("Are you sure want to cancel this order?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();

                            if (Utils.isNetworkAvailable(MyOrderListActivity.this)) {
                                orderCancel(model.getId());
                            } else {
                                Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                            }

                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            AppConstant.check_cart_view = "";
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
                            alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                            //dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.black));
                        }
                    });
                    alert.show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }


    private void orderCancel(String order_id) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.orderCancel, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        Toast.makeText(MyOrderListActivity.this, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                        orderhistory();
                    } else {
                        Toast.makeText(MyOrderListActivity.this, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("order_id", order_id);
                Log.e("params", "" + Constants.orderCancel + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.orderCancel);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


}
