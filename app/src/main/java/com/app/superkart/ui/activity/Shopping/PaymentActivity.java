package com.app.superkart.ui.activity.Shopping;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.adpters.Shopping.Fashion_Address_Item_Adpter;
import com.app.superkart.model.Shopping.ProductListModel;
import com.app.superkart.retrofit_config.APi;
import com.app.superkart.retrofit_config.Res;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.Signup.LoginActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.cashfree.pg.CFPaymentService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.cashfree.pg.CFPaymentService.PARAM_APP_ID;
import static com.cashfree.pg.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.cashfree.pg.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.cashfree.pg.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_CURRENCY;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_ID;
import static com.cashfree.pg.CFPaymentService.PARAM_ORDER_NOTE;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {
    private Context context;
    private ImageView img_back, img_Online, img_COD;
    private TextView txt_btn_continue, txt_btn_address, txt_total_MRP, txt_discount, txt_shipp_charge, txt_grandTotal, txt_amount, txt_loyaltyPoints,txt_item_amount;
    private RecyclerView recycle_item;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<ProductListModel> arrayList = new ArrayList<>();
    private Fashion_Address_Item_Adpter fashion_address_item_adpter;
    private TextView txt_userName, txt_address, txt_phone;
    private SharedPreferences preferences;
    private SharedPreferences pref;
    private ProgressDialog progressDialog;
    private SharedPreferences pref_city;
    private String payment_Type = "", payment_mode = "", order_id = "", shippingCharge = "", referenceId = "", txStatus = "", txMsg = "", txTime = "", signature = "", paymentMode = "", strMsg = "";
    private JSONArray json_array;
    private JSONObject obj_data = new JSONObject();
    private ArrayList<String> payment_data = new ArrayList<>();

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        context = this;
        Utils.showTitle(window, context);
        setContentView(R.layout.fashion_address_layout);
        Log.e("PaymentActivity", "PaymentActivity");
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        pref = getSharedPreferences("ShipingAddress", MODE_PRIVATE);
        pref_city = getSharedPreferences("city", Context.MODE_PRIVATE);
        init();
        json_array = new JSONArray();
        txt_userName.setText(AppConstant.name + " (Default)");
        txt_address.setText(AppConstant.Addressline1 + "," + AppConstant.Addressline2 + "\n" + AppConstant.city + "," + AppConstant.state_address + "," + "\n" + AppConstant.zip);
        txt_phone.setText("Mobile: " + AppConstant.phone);


        payment_mode = "online";
        img_Online.setBackgroundResource(R.drawable.tick_button);
        img_COD.setBackgroundResource(R.drawable.cheched);
        //Log.e("order_id", "" + AppConstant.order_id);
        String[] separated = AppConstant.order_id.split("_");
        for (int i = 0; i < separated.length; i++) {
            order_id = separated[i];
            // Log.e("order_id", "" + order_id);
        }


    }

    public void init() {

        progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        recycle_item = findViewById(R.id.recycle_item);
        linearLayoutManager = new LinearLayoutManager(PaymentActivity.this, LinearLayoutManager.VERTICAL, false);
        recycle_item.setLayoutManager(linearLayoutManager);

        img_back = findViewById(R.id.img_back);
        img_Online = findViewById(R.id.img_Online);
        img_COD = findViewById(R.id.img_COD);
        txt_userName = findViewById(R.id.txt_userName);
        txt_address = findViewById(R.id.txt_address);
        txt_phone = findViewById(R.id.txt_phone);
        txt_btn_continue = findViewById(R.id.txt_btn_continue);
        txt_btn_address = findViewById(R.id.txt_btn_address);
        txt_total_MRP = findViewById(R.id.txt_total_MRP);
        txt_discount = findViewById(R.id.txt_discount);
        txt_amount = findViewById(R.id.txt_amount);
        txt_shipp_charge = findViewById(R.id.txt_shipp_charge);
        txt_grandTotal = findViewById(R.id.txt_grandTotal);
        txt_loyaltyPoints = findViewById(R.id.txt_loyaltyPoints);
        txt_item_amount = findViewById(R.id.txt_item_amount);
        img_back.setOnClickListener(this);
        txt_btn_continue.setOnClickListener(this);
        txt_btn_address.setOnClickListener(this);
        img_Online.setOnClickListener(this);
        img_COD.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.txt_btn_continue:
                if (payment_mode.equals("online")) {
                    payment_Type = "online";
                    if (Utils.isNetworkAvailable(PaymentActivity.this)) {
                        initiatepayment();
                    } else {
                        Toast.makeText(PaymentActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    payment_Type = "cod";
                    successorder(referenceId, txStatus, paymentMode, txMsg, txTime, signature);
                }

                break;
            case R.id.txt_btn_address:
                Intent intent = new Intent(PaymentActivity.this, AddressListActivity.class);
                startActivity(intent);
                break;
            case R.id.img_Online:
                payment_mode = "online";
                txt_btn_continue.setText("Pay Now");
                img_Online.setBackgroundResource(R.drawable.tick_button);
                img_COD.setBackgroundResource(R.drawable.cheched);
                paymentshippingcharges(payment_mode);
                break;

            case R.id.img_COD:
                payment_mode = "cod";
                txt_btn_continue.setText("Place Order");
                img_Online.setBackgroundResource(R.drawable.cheched);
                img_COD.setBackgroundResource(R.drawable.tick_button);
                paymentshippingcharges(payment_mode);
                break;
        }
    }

    private void revieworder() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.revieworder, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    arrayList.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            ProductListModel model = new ProductListModel();
                            model.setProduct_image(array_result.optJSONObject(i).optString("productimage"));
                            model.setProduct_name(array_result.optJSONObject(i).optString("product_name"));
                            model.setQty(array_result.optJSONObject(i).optString("qty"));
                            model.setSale_price(array_result.optJSONObject(i).optString("price"));
                            arrayList.add(model);
                        }
                        if (arrayList.size() > 0) {
                            recycle_item.setVisibility(View.VISIBLE);
                            fashion_address_item_adpter = new Fashion_Address_Item_Adpter(PaymentActivity.this, arrayList);
                            recycle_item.setAdapter(fashion_address_item_adpter);
                        } else {
                            recycle_item.setVisibility(View.GONE);
                        }
                        if (json_main.optString("grandtotal").equals("") || json_main.optString("grandtotal").equals("null") || json_main.optString("grandtotal").equals(null)) {
                            txt_total_MRP.setText("₹0");
                            txt_grandTotal.setText("₹0");
                            txt_amount.setText("₹0");
                            txt_discount.setText("₹0");
                            txt_item_amount.setText("₹0");
                        } else {
                            txt_item_amount.setText("₹" + json_main.optString("item_amount"));
                            txt_total_MRP.setText("₹" + json_main.optString("withDiscountTotal"));
                            txt_grandTotal.setText("₹" + json_main.optString("grandtotal"));
                            txt_amount.setText("₹" + json_main.optString("grandtotal"));
                            txt_discount.setText("-₹" + json_main.optString("discountamt"));
                            AppConstant.amount = json_main.optString("withDiscountTotal");
                            txt_loyaltyPoints.setText("-₹" + json_main.optString("requestLp"));
                        }

                        if (json_main.optString("chargeship").equals("") || json_main.optString("chargeship").equals("null") || json_main.optString("chargeship").equals(null)) {
                            txt_shipp_charge.setText("₹ 0.0");
                        } else {
                            txt_shipp_charge.setText("₹" + json_main.optString("chargeship"));
                        }

                        JSONObject obj_cashfree = json_main.optJSONObject("cashfree");
                        AppConstant.secretKey = obj_cashfree.optString("secretKey");
                        AppConstant.appId = obj_cashfree.optString("appId");
                        Log.e("appId", "" + obj_cashfree.optString("appId"));
                        AppConstant.payment_mode_cashfree_url = obj_cashfree.optString("endpoint_url");
                        Log.e("endpoint_url", "" + AppConstant.payment_mode_cashfree_url);
                        cashfree_token_api();
                        paymentshippingcharges(payment_mode);
                    } else {
                        progressDialog.dismiss();
                      //  Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                        NotServiceProvided(json_main.optString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("user_city", pref_city.getString("city", ""));
                //params.put("user_city", "Ambala");
                params.put("payment_mode", payment_mode);
                params.put("address_id", AppConstant.address_id);
                Log.e("params", "" + Constants.revieworder + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.revieworder);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void paymentshippingcharges(String payment_mode) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.paymentshippingcharges, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    arrayList.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        if (json_main.optString("grandtotal").equals("") || json_main.optString("grandtotal").equals("null") || json_main.optString("grandtotal").equals(null)) {
                            txt_total_MRP.setText("₹0");
                            txt_grandTotal.setText("₹0");
                            txt_amount.setText("₹0");
                        } else {
                            //  txt_total_MRP.setText("₹" + json_main.optString("grandtotal"));
                            txt_grandTotal.setText("₹" + json_main.optString("withDiscountTotal"));
                            txt_amount.setText("₹" + json_main.optString("grandtotal"));
                            txt_total_MRP.setText("₹" + json_main.optString("withDiscountTotal"));
                            AppConstant.amount = json_main.optString("withDiscountTotal");
                            txt_loyaltyPoints.setText("-₹" + json_main.optString("requestLp"));
                            txt_discount.setText("-₹" + json_main.optString("discountamt"));
                        }
                        if (json_main.optString("chargeship").equals("") || json_main.optString("chargeship").equals("null") || json_main.optString("chargeship").equals(null)) {
                            txt_shipp_charge.setText("₹ 0.0");
                        } else {
                            txt_shipp_charge.setText("₹" + json_main.optString("chargeship"));
                            shippingCharge = json_main.optString("chargeship");
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("user_city", pref_city.getString("city", ""));
                //  params.put("user_city", "Ambala");
                params.put("payment_mode", payment_mode);
                params.put("address_id", AppConstant.address_id);
                Log.e("params", "" + Constants.paymentshippingcharges + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.paymentshippingcharges);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("m", "API Response : ");
        //Prints all extras. Replace with app logic.
        if (data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null)
                for (String key : bundle.keySet()) {
                    if (bundle.getString(key) != null) {
                        Log.d("resp", key + " " + " : " + bundle.getString(key));
                        try {
                            obj_data.put(key, bundle.getString(key));
                            Log.e("obj_data", "" + obj_data.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            json_array.put(obj_data);
            Log.e("json_array", "" + json_array);
        }
        for (int i = 0; i < json_array.length(); i++) {
            referenceId = json_array.optJSONObject(i).optString("referenceId");
            txStatus = json_array.optJSONObject(i).optString("txStatus");
            paymentMode = json_array.optJSONObject(i).optString("paymentMode");
            txMsg = json_array.optJSONObject(i).optString("txMsg");
            strMsg = json_array.optJSONObject(i).optString("txMsg");
            txTime = json_array.optJSONObject(i).optString("txTime");
            signature = json_array.optJSONObject(i).optString("signature");
            successorder(referenceId, txStatus, paymentMode, txMsg, txTime, signature);
        }
    }

    private void initiatepayment() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.New_BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        final APi service = retrofit.create(APi.class);
        progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setMessage("Registering......");
        progressDialog.setCancelable(false);
        progressDialog.show();
        retrofit2.Call<Res> call = service.getToken(AppConstant.amount, AppConstant.order_id, "INR");
        call.enqueue(new Callback<Res>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(Call<Res> call, retrofit2.Response<Res> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    Log.e("generate_cftoken response", "" + response);
                    if (response.body().getMessage().equals("Token generated")) {
                        String token = response.body().getCftoken();
                        Log.e("token", "" + token);
                        Map<String, String> params = new HashMap<>();
                        params.put(PARAM_APP_ID, AppConstant.appId);
                        params.put(PARAM_ORDER_ID, AppConstant.order_id);
                        params.put(PARAM_ORDER_AMOUNT, AppConstant.amount);
                        params.put(PARAM_ORDER_NOTE, AppConstant.note);
                        params.put(PARAM_CUSTOMER_NAME, AppConstant.name);
                        params.put(PARAM_CUSTOMER_PHONE, AppConstant.phone);
                        params.put(PARAM_CUSTOMER_EMAIL, AppConstant.email);
                        params.put(PARAM_ORDER_CURRENCY, "INR");
                        Log.e("params", "" + params);
                        for (Map.Entry entry : params.entrySet()) {
                            Log.d("CFSKDSample", entry.getKey() + " " + entry.getValue());
                        }
                        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
                        cfPaymentService.setOrientation(0);
                        cfPaymentService.doPayment(PaymentActivity.this, params, token, "PROD", "#F8A31A", "#FFFFFF", false);
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(PaymentActivity.this, "msg2" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("msg2", "" + response.body().getMessage());
                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(PaymentActivity.this, "msg3" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("msg3", "" + response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<Res> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PaymentActivity.this, "msg4" + t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("lll", "" + t.getMessage());
                Log.e("msg4", "" + t.getMessage());
            }
        });
    }

    private void cashfree_token_api() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.cashfree_token_api, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    arrayList.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        Log.e("json_main", "" + json_main);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("appId", AppConstant.appId);
                params.put("orderId", AppConstant.order_id);
                params.put("orderAmount", AppConstant.amount);
                params.put("customerName", AppConstant.name);
                params.put("customerPhone", AppConstant.phone);
                params.put("customerEmail", AppConstant.email);
                params.put("orderNote", AppConstant.note);
                params.put("orderCurrency", "INR");
                Log.e("params", "" + Constants.cashfree_token_api + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.cashfree_token_api);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void successorder(String referenceId, String txStatus, String paymentMode, String txMsg, String txTime, String signature) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.successorder, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response successorder", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    arrayList.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        Toast.makeText(PaymentActivity.this, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PaymentActivity.this, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    if (payment_Type.equals("online")) {
                        OrderConfirm_Dailog();
                    } else {
                        OrderConfirm_Dailog1();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                if (payment_mode.equals("online")) {
                    params.put("user_city", pref_city.getString("city", ""));
                    params.put("appId", AppConstant.appId);
                    params.put("orderId", AppConstant.order_id);
                    params.put("orderAmount", AppConstant.amount);
                    params.put("referenceId", referenceId);
                    params.put("txStatus", txStatus);
                    params.put("paymentMode", paymentMode);
                    params.put("txMsg", txMsg);
                    params.put("txTime", txTime);
                    params.put("signature", signature);
                    params.put("paymenttype", payment_Type);
                } else {
                    params.put("orderId", AppConstant.order_id);
                    params.put("orderAmount", AppConstant.amount);
                }
                params.put("shippingCharge", shippingCharge);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("paymenttype", payment_Type);
                Log.e("params", "" + Constants.successorder + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.successorder);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void OrderConfirm_Dailog() {
        Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_dailog_alert);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView txt_dialog_orderid = (TextView) dialog.findViewById(R.id.txt_dialog_orderid);
        TextView txt_email = (TextView) dialog.findViewById(R.id.txt_email);
        TextView btn_home = (TextView) dialog.findViewById(R.id.btn_home);
        TextView txt_order_history = (TextView) dialog.findViewById(R.id.txt_order_history);
        TextView txt_status = (TextView) dialog.findViewById(R.id.txt_status);
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_amount = (TextView) dialog.findViewById(R.id.txt_amount);
        ImageView imageView5 = (ImageView) dialog.findViewById(R.id.imageView5);
        LinearLayout li_content = (LinearLayout) dialog.findViewById(R.id.li_content);
        txt_amount.setText("Your order is placed " + "₹" + AppConstant.amount);
        txt_dialog_orderid.setText("Order Id:" + AppConstant.order_id);
        txt_email.setText(preferences.getString(PreferenceManager.email_id, ""));

        if (txStatus.equalsIgnoreCase("Success")) {
            txt_status.setText(txStatus + "!");
            imageView5.setBackgroundResource(R.drawable.check_round);
            txt_order_history.setVisibility(View.VISIBLE);
            li_content.setVisibility(View.VISIBLE);
        } else {
            txt_status.setText(txStatus + "!");
            txt_msg.setText("Something went wrong,Please try again!");
            imageView5.setBackgroundResource(R.drawable.failed);
            txt_order_history.setVisibility(View.GONE);
            li_content.setVisibility(View.GONE);
        }
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // change.onhome();
                SharedPreferences cart_count = getSharedPreferences("cart_count", 0);
                cart_count.edit().clear().commit();
                Intent intent = new Intent(PaymentActivity.this, BaseActivity.class);
                startActivity(intent);
                dialog.hide();
                finish();
            }
        });
        txt_order_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.chk_payment_view = "check";
                SharedPreferences cart_count = getSharedPreferences("cart_count", 0);
                cart_count.edit().clear().commit();
                Intent intent = new Intent(PaymentActivity.this, MyOrderListActivity.class);
                startActivity(intent);
                dialog.hide();
                finish();
            }
        });
        dialog.show();
    }

    public void OrderConfirm_Dailog1() {
        Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_dailog_alert);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView txt_dialog_orderid = (TextView) dialog.findViewById(R.id.txt_dialog_orderid);
        TextView txt_email = (TextView) dialog.findViewById(R.id.txt_email);
        TextView btn_home = (TextView) dialog.findViewById(R.id.btn_home);
        TextView txt_order_history = (TextView) dialog.findViewById(R.id.txt_order_history);
        TextView txt_status = (TextView) dialog.findViewById(R.id.txt_status);
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_amount = (TextView) dialog.findViewById(R.id.txt_amount);
        ImageView imageView5 = (ImageView) dialog.findViewById(R.id.imageView5);
        LinearLayout li_content = (LinearLayout) dialog.findViewById(R.id.li_content);
        txt_dialog_orderid.setText("Order Id:" + AppConstant.order_id);
        txt_amount.setText("Your order is placed " + "₹" + AppConstant.amount);
        txt_email.setText(preferences.getString(PreferenceManager.email_id, ""));
        imageView5.setBackgroundResource(R.drawable.check_round);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // change.onhome();
                SharedPreferences cart_count = getSharedPreferences("cart_count", 0);
                cart_count.edit().clear().commit();
                Intent intent = new Intent(PaymentActivity.this, BaseActivity.class);
                startActivity(intent);
                finish();
            }
        });
        txt_order_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.chk_payment_view = "check";
                SharedPreferences cart_count = getSharedPreferences("cart_count", 0);
                cart_count.edit().clear().commit();
                Intent intent = new Intent(PaymentActivity.this, MyOrderListActivity.class);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }

    public void NotServiceProvided(String msg) {
        Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dailog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView btn_home = (TextView) dialog.findViewById(R.id.btn_home);
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        txt_msg.setText(msg);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(PaymentActivity.this, BaseActivity.class);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }

    @Override
    protected void onResume() {
        if (Utils.isNetworkAvailable(PaymentActivity.this)) {
            revieworder();
        } else {
            Toast.makeText(PaymentActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }
        super.onResume();
    }
}

