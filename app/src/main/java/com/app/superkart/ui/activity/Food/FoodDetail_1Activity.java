package com.app.superkart.ui.activity.Food;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.adpters.Food.FoodDetailAdpter;
import com.app.superkart.adpters.Food.FoodDetailCateAdpter;
import com.app.superkart.adpters.Food.FoodDetailSizeAdpter;
import com.app.superkart.model.Food.Food_Item_Model;
import com.app.superkart.utils.Utils;

import java.util.ArrayList;

public class FoodDetail_1Activity extends AppCompatActivity implements View.OnClickListener {
    Context mcontext;
    private ImageView img_back;
    private RecyclerView recycle_item_size, recycle_item_cate;
    private ArrayList<Food_Item_Model> arrayList = new ArrayList<>();
    private ArrayList<Food_Item_Model> arrayList_cate = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager, linearLayoutManager1;
    private FoodDetailSizeAdpter foodDetailSizeAdpter;
    private FoodDetailCateAdpter foodDetailCateAdpter;
    private LinearLayout li_btn_addToBag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.food_detail_1_layout);
        Log.e("FoodDetail_1Activity", "FoodDetail_1Activity");
        init();
        setSize();
        setCate();
    }

    public void init() {
        img_back = findViewById(R.id.img_back);
        li_btn_addToBag = findViewById(R.id.li_btn_addToBag);
        img_back.setOnClickListener(this);
        li_btn_addToBag.setOnClickListener(this);
    }

    private void setSize() {
        arrayList.clear();
        Food_Item_Model model;

        model = new Food_Item_Model("Giant Slice", "₹ 225");
        arrayList.add(model);

        model = new Food_Item_Model("Regular (7 Inch)", "₹ 245");
        arrayList.add(model);

        model = new Food_Item_Model("Medium (10 Inch)", "₹ 590");
        arrayList.add(model);

        model = new Food_Item_Model("Large (13 Inch)", "₹ 760");
        arrayList.add(model);

        recycle_item_size = findViewById(R.id.recycle_item_size);
        linearLayoutManager = new LinearLayoutManager(FoodDetail_1Activity.this, LinearLayoutManager.VERTICAL, false);
        recycle_item_size.setLayoutManager(linearLayoutManager);
        foodDetailSizeAdpter = new FoodDetailSizeAdpter(FoodDetail_1Activity.this, arrayList);
        recycle_item_size.setAdapter(foodDetailSizeAdpter);
    }

    private void setCate() {
        arrayList_cate.clear();
        Food_Item_Model model;

        model = new Food_Item_Model("Onion", "₹ 50");
        arrayList_cate.add(model);

        model = new Food_Item_Model("Capsicum", "₹ 80");
        arrayList_cate.add(model);

        model = new Food_Item_Model("Paneer", "₹ 80");
        arrayList_cate.add(model);

        model = new Food_Item_Model("Olives", "₹ 60");
        arrayList_cate.add(model);

        model = new Food_Item_Model("Jalapenos", "₹ 55");
        arrayList_cate.add(model);

        model = new Food_Item_Model("Red Paprika", "₹ 75");
        arrayList_cate.add(model);

        model = new Food_Item_Model("Pineapple", "₹ 85");
        arrayList_cate.add(model);

        recycle_item_cate = findViewById(R.id.recycle_item_cate);
        linearLayoutManager1 = new LinearLayoutManager(FoodDetail_1Activity.this, LinearLayoutManager.VERTICAL, false);
        recycle_item_cate.setLayoutManager(linearLayoutManager1);
        foodDetailCateAdpter = new FoodDetailCateAdpter(FoodDetail_1Activity.this, arrayList_cate);
        recycle_item_cate.setAdapter(foodDetailCateAdpter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.li_btn_addToBag:
                Intent i = new Intent(FoodDetail_1Activity.this, AddFoodItemsActivity.class);
                startActivity(i);
                break;
        }
    }
}
