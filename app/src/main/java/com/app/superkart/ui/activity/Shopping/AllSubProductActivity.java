package com.app.superkart.ui.activity.Shopping;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.adpters.Shopping.HomeAllCategoryAdpter;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AllSubProductActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mcontext;
    private RecyclerView recycle_category;
    private GridLayoutManager gridLayoutManager;
    private ArrayList<HomeCategoryModel> arrayList_homecategory = new ArrayList<>();
    private AllSubProductAdpter allSubProductAdpter;
    private ImageView img_back;
    private ProgressDialog progressDialog;
    private TextView txt_title;
    View view = null;
    String category_id = "", offer_title = "";
    private SwipeRefreshLayout swipeRefreshLayout;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.home_all_category);
        Log.e("AllSubProductActivity", "AllSubProductActivity");
        init();
        intentData();
        if (Utils.isNetworkAvailable(AllSubProductActivity.this)) {
            allsubcats();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                if (Utils.isNetworkAvailable(AllSubProductActivity.this)) {
                    allsubcats();
                } else {
                    Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_category = findViewById(R.id.recycle_category);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        img_back.setOnClickListener(this);
    }

    public void intentData() {
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            category_id = (String) bd.get("category_id");
            offer_title = (String) bd.get("offer_title");
            txt_title.setText(offer_title);
        }
    }


    private void allsubcats() {

        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList_homecategory.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.allsubcats, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONArray json_result = json_main.optJSONArray("result");
                        bindCategoryData(json_result);
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("category_id", category_id);
                Log.e("params", "" + Constants.allsubcats + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.allsubcats);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void bindCategoryData(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            HomeCategoryModel homeCategoryModel = new HomeCategoryModel();
            homeCategoryModel.setCategory_id(jsonArray.optJSONObject(i).optString("category_id"));
            homeCategoryModel.setItem_name(jsonArray.optJSONObject(i).optString("category_name"));
            homeCategoryModel.setCate_image(jsonArray.optJSONObject(i).optString("category_image"));
            arrayList_homecategory.add(homeCategoryModel);
        }
        if(arrayList_homecategory.size()>0){
            recycle_category = findViewById(R.id.recycle_category);
            gridLayoutManager = new GridLayoutManager(AllSubProductActivity.this, 2);
            recycle_category.setLayoutManager(gridLayoutManager);
            allSubProductAdpter = new AllSubProductAdpter(AllSubProductActivity.this, arrayList_homecategory);
            recycle_category.setAdapter(allSubProductAdpter);
            allSubProductAdpter.notifyDataSetChanged();
        }

    }


    public class AllSubProductAdpter extends RecyclerView.Adapter<AllSubProductAdpter.MyViewHolder> {

        private List<HomeCategoryModel> arrayList;
        private Context context;
        int pos = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item;
            public TextView txt_item;
            LinearLayout li_bg;
            CardView card_main1;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_item = view.findViewById(R.id.txt_item);
                li_bg = view.findViewById(R.id.li_bg);
            }
        }

        public AllSubProductAdpter(Context context, List<HomeCategoryModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adpter_all_category, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            HomeCategoryModel model = arrayList.get(position);

            if (model.getItem_name().equals("")) {
            } else {
                holder.txt_item.setText(model.getItem_name());
            }
            if (model.getCate_image().equals("")) {

            } else {
           /* Picasso.with(context)
                    .load(model.getCate_image())
                    .into(holder.img_item);*/

                Glide
                        .with(context)
                        .load(model.getCate_image())
                        .centerCrop()
                        .into(holder.img_item);
            }
            if (pos == position) {
                holder.li_bg.setBackgroundResource(R.drawable.border_blue);
                holder.txt_item.setTextColor(Color.parseColor("#1576CE"));
            } else {
                holder.li_bg.setBackgroundResource(R.drawable.border_gray);
                holder.txt_item.setTextColor(Color.parseColor("#FF000000"));
            }
            holder.li_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = position;
                    notifyDataSetChanged();
                    Intent i = new Intent(context, Home_ViewAll_Category_List.class);
                    i.putExtra("category_id", model.getCategory_id());
                    i.putExtra("offer_title", model.getItem_name());
                    context.startActivity(i);
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }
}

