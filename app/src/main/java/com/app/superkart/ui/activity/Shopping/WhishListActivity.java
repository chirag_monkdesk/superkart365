package com.app.superkart.ui.activity.Shopping;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.ProductListModel;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WhishListActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private RecyclerView recycle_list;
    GridLayoutManager gridLayoutManager;
    SharedPreferences preferences;
    private ProgressDialog progressDialog;
    WhishListAdpter whishListAdpter;
    private ArrayList<ProductListModel> arrayList = new ArrayList<>();
    private ImageView img_back;
    private LinearLayout li_error_msg;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.whishlist_layout);
        Log.e("WhishListActivity", "WhishListActivity");
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        Log.e("user_id", "" + preferences.getString(PreferenceManager.user_id, ""));
        init();
        if (Utils.isNetworkAvailable(WhishListActivity.this)) {
            favoritelist();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                if (Utils.isNetworkAvailable(WhishListActivity.this)) {
                    favoritelist();
                } else {
                    Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_list = findViewById(R.id.recycle_list);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        img_back = findViewById(R.id.img_back);
        li_error_msg = findViewById(R.id.li_error_msg);
        img_back.setOnClickListener(this);
    }

    private void favoritelist() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.favoritelist, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        li_error_msg.setVisibility(View.GONE);
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            ProductListModel model = new ProductListModel();
                            model.setProduct_id(array_result.optJSONObject(i).optString("product_id"));
                            model.setIs_fav(array_result.optJSONObject(i).optString("is_fav"));
                            model.setProduct_name(array_result.optJSONObject(i).optString("product_name"));
                            model.setProduct_image(array_result.optJSONObject(i).optString("product_image"));
                            model.setRegular_price(array_result.optJSONObject(i).optString("regular_price"));
                            model.setSale_price(array_result.optJSONObject(i).optString("sale_price"));
                            model.setDescription(array_result.optJSONObject(i).optString("description"));
                            model.setStorename(array_result.optJSONObject(i).optString("storename"));
                            model.setSelling_type(array_result.optJSONObject(i).optString("selling_type"));
                            arrayList.add(model);
                        }
                        if (arrayList.size() > 0) {
                            li_error_msg.setVisibility(View.GONE);
                            gridLayoutManager = new GridLayoutManager(WhishListActivity.this, 2);
                            recycle_list.setLayoutManager(gridLayoutManager);
                            whishListAdpter = new WhishListAdpter(WhishListActivity.this, arrayList);
                            recycle_list.setAdapter(whishListAdpter);
                            whishListAdpter.notifyDataSetChanged();
                        } else {
                            recycle_list.setVisibility(View.GONE);
                            li_error_msg.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recycle_list.setVisibility(View.GONE);
                        li_error_msg.setVisibility(View.VISIBLE);
                        // Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                    li_error_msg.setVisibility(View.VISIBLE);
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    li_error_msg.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.favoritelist + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.favoritelist);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    public void BindData(JSONArray array_result) {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

    public class WhishListAdpter extends RecyclerView.Adapter<WhishListAdpter.MyViewHolder> {

        private List<ProductListModel> arrayList;
        private Context context;
        private int pos = 0;
        SharedPreferences preferences;


        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item, img_fav;
            public TextView txt_desc, txt_item, txt_salePrice, txt_RegularPrice, txt_label;
            LinearLayout li_bg;
            CardView card_main1;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                img_fav = view.findViewById(R.id.img_fav);
                txt_item = view.findViewById(R.id.txt_item);
                txt_desc = view.findViewById(R.id.txt_desc);
                txt_salePrice = view.findViewById(R.id.txt_salePrice);
                txt_RegularPrice = view.findViewById(R.id.txt_RegularPrice);
                txt_label = view.findViewById(R.id.txt_label);
                li_bg = view.findViewById(R.id.li_bg);
                card_main1 = view.findViewById(R.id.card_main1);
            }
        }

        public WhishListAdpter(Context context, List<ProductListModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adpter_home_all_sub_category_list, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ProductListModel model = arrayList.get(position);
            preferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            if (model.getProduct_image().equals("")) {

            } else {
               /* Picasso.with(context)
                        .load(model.getProduct_image())
                        .into(holder.img_item);
*/
                Glide
                        .with(context)
                        .load(model.getProduct_image())
                        .centerCrop()
                        .into(holder.img_item);

            }

            if (model.getProduct_name().equals("")) {

            } else {
                holder.txt_item.setText(model.getProduct_name());
            }

            if (model.getDescription().equals("")) {

            } else {
                holder.txt_desc.setText(model.getDescription());
            }
            if (model.getSale_price().equals("")) {

            } else {
                holder.txt_salePrice.setText("₹" + model.getSale_price());
            }
            if (model.getRegular_price().equals("")) {

            } else {
                holder.txt_RegularPrice.setText("₹" + model.getRegular_price());
            }
            if (model.getIs_fav().equals("0")) {
                holder.img_fav.setBackgroundResource(R.drawable.dislike);
            } else {
                holder.img_fav.setBackgroundResource(R.drawable.like);
            }

            if (model.getSelling_type().equals("in-store")) {
                holder.txt_label.setVisibility(View.VISIBLE);
                holder.txt_label.setText(model.getSelling_type());
            } else {
                holder.txt_label.setVisibility(View.GONE);
            }


            holder.img_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                        Utils.showSnackBar(v, "Please Login to add this product whistlist.");
                    } else {
                        if (Utils.isNetworkAvailable(WhishListActivity.this)) {
                            if (model.getIs_fav().equals("0")) {
                                addtowishlist(model.getProduct_id());
                            } else {
                                removewishlist(model.getProduct_id());
                            }

                        } else {
                            Toast.makeText(context, R.string.check_internet, Toast.LENGTH_LONG).show();
                        }

                    }

                }
            });
            holder.card_main1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.product_id =model.getProduct_id();
                    Intent i = new Intent(WhishListActivity.this, Home_View_Fashion_Detail.class);
                    i.putExtra("product_id", model.getProduct_id());
                    i.putExtra("is_fav", model.getIs_fav());
                    i.putExtra("storename", model.getStorename());
                    i.putExtra("selling_type", model.getSelling_type());
                    startActivity(i);
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

    private void addtowishlist(String product_id) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.addtowishlist, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        favoritelist();
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", product_id);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.addtowishlist + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.addtowishlist);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void removewishlist(String product_id) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.removewishlist, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        favoritelist();
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", product_id);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.removewishlist + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.removewishlist);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}
