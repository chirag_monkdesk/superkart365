package com.app.superkart.ui.activity.Food;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.app.superkart.R;
import com.app.superkart.utils.Utils;

public class Food_Take_Away_Activity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.food_take_away_layout);
        Log.e("Food_Take_Away_Activity", "Food_Take_Away_Activity");
        init();
    }

    public void init() {
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }
}
