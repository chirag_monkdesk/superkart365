package com.app.superkart.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.adpters.Food.Tab_Category_Food_Adpter;
import com.app.superkart.adpters.Service.Tab_Category_Service_Adpter;
import com.app.superkart.adpters.Shopping.HomeAllCategoryAdpter;
import com.app.superkart.adpters.Shopping.Tab_Category_Shopping_Adpter;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.Shopping.HomeAll_CategoryActivity;
import com.app.superkart.ui.activity.Shopping.SellerForyouActivity;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class CategoryFragment extends Fragment {

    private RecyclerView recycle_shopping, recycle_food, recycle_service;
    GridLayoutManager gridLayoutManager_shopping, gridLayoutManager_food, gridLayoutManager_service;
    private ArrayList<HomeCategoryModel> arrayList_shopping = new ArrayList<>();
    private ArrayList<HomeCategoryModel> arrayList_food = new ArrayList<>();
    private ArrayList<HomeCategoryModel> arrayList_service = new ArrayList<>();
    private Tab_Category_Shopping_Adpter tab_category_shopping_adpter;
    private Tab_Category_Food_Adpter tab_category_food_adpter;
    private Tab_Category_Service_Adpter tab_category_service_adpter;
    private ProgressDialog progressDialog;
    private Context mcontext;
    private EditText et_search;
    private SharedPreferences preferences;
    private SwipeRefreshLayout swipeRefreshLayout;

    public CategoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_category, container, false);
        Log.e("CategoryFragment", "CategoryFragment");
        mcontext = getActivity();
        init(view);
        preferences = getActivity().getSharedPreferences(getActivity().getString(R.string.app_name), Context.MODE_PRIVATE);
        bindFoodgData(view);
        bindServiceData(view);
        if (Utils.isNetworkAvailable(getActivity())) {
            AllCategory(view);
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                if (Utils.isNetworkAvailable(getActivity())) {
                    if (Utils.isNetworkAvailable(getActivity())) {
                        AllCategory(view);
                    } else {
                        Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                    }

                } else {
                    Utils.showSnackBar(view, getResources().getString(R.string.check_internet));
                }
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    tab_category_shopping_adpter.getFilter().filter(s);
                } else {
                    try {
                        tab_category_shopping_adpter.resetData();
                        tab_category_shopping_adpter.notifyDataSetChanged();
                        Utils.hideKeyboard(getActivity());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    public void init(View view) {
        et_search = view.findViewById(R.id.et_search);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
    }

    public void AllCategory(View view) {
        String tag_string_req = "req";
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList_shopping.clear();
        final StringRequest strReq = new StringRequest(Request.Method.GET, Constants.AllCategory, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONArray json_result = json_main.optJSONArray("result");
                        bindShoppingData(view, json_result);
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.AllCategory);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    public void bindShoppingData(View view, JSONArray jsonArray) {

        arrayList_shopping.clear();
        for (int i = 0; i < jsonArray.length(); i++) {
            HomeCategoryModel homeCategoryModel = new HomeCategoryModel();
            homeCategoryModel.setCategory_id(jsonArray.optJSONObject(i).optString("category_id"));
            homeCategoryModel.setItem_name(jsonArray.optJSONObject(i).optString("category_name"));
            homeCategoryModel.setCate_image(jsonArray.optJSONObject(i).optString("category_image"));
            homeCategoryModel.setIs_parent(jsonArray.optJSONObject(i).optString("is_parent"));
            arrayList_shopping.add(homeCategoryModel);
        }
        if (arrayList_shopping.size() > 0) {
            recycle_shopping = view.findViewById(R.id.recycle_shopping);
            gridLayoutManager_shopping = new GridLayoutManager(getActivity(), 3);
            recycle_shopping.setLayoutManager(gridLayoutManager_shopping);
            tab_category_shopping_adpter = new Tab_Category_Shopping_Adpter(getActivity(), arrayList_shopping);
            recycle_shopping.setAdapter(tab_category_shopping_adpter);
        }

    }

    public void bindFoodgData(View view) {

        arrayList_food.clear();
        HomeCategoryModel homeCategoryModel;

        homeCategoryModel = new HomeCategoryModel(R.drawable.pizza, "Pizza");
        arrayList_food.add(homeCategoryModel);


        homeCategoryModel = new HomeCategoryModel(R.drawable.burger, "Burger");
        arrayList_food.add(homeCategoryModel);


        homeCategoryModel = new HomeCategoryModel(R.drawable.noodle, "Noodles");
        arrayList_food.add(homeCategoryModel);


        homeCategoryModel = new HomeCategoryModel(R.drawable.coffee, "Coffee");
        arrayList_food.add(homeCategoryModel);


        homeCategoryModel = new HomeCategoryModel(R.drawable.lunch, "Lunch");
        arrayList_food.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.pizza, "Pizza");
        arrayList_food.add(homeCategoryModel);

        recycle_food = view.findViewById(R.id.recycle_food);

        gridLayoutManager_food = new GridLayoutManager(getActivity(), 3);
        recycle_food.setLayoutManager(gridLayoutManager_food);
        tab_category_food_adpter = new Tab_Category_Food_Adpter(getActivity(), arrayList_food);
        recycle_food.setAdapter(tab_category_food_adpter);
    }

    public void bindServiceData(View view) {

        arrayList_service.clear();
        HomeCategoryModel homeCategoryModel;

        homeCategoryModel = new HomeCategoryModel(R.drawable.haircutting, "Haircutting");
        arrayList_service.add(homeCategoryModel);


        homeCategoryModel = new HomeCategoryModel(R.drawable.cleaning, "Cleaing");
        arrayList_service.add(homeCategoryModel);


        homeCategoryModel = new HomeCategoryModel(R.drawable.geyser, "Geyser Service");
        arrayList_service.add(homeCategoryModel);


        homeCategoryModel = new HomeCategoryModel(R.drawable.plumber, "Plumber");
        arrayList_service.add(homeCategoryModel);


        homeCategoryModel = new HomeCategoryModel(R.drawable.salon, "Salon");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.elctrician, "Electrician");
        arrayList_service.add(homeCategoryModel);

        recycle_service = view.findViewById(R.id.recycle_service);

        gridLayoutManager_service = new GridLayoutManager(getActivity(), 3);
        recycle_service.setLayoutManager(gridLayoutManager_service);
        tab_category_service_adpter = new Tab_Category_Service_Adpter(getActivity(), arrayList_service);
        recycle_service.setAdapter(tab_category_service_adpter);
    }

    @Override
    public void onResume() {

        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
            BaseActivity.slide_login.setVisibility(View.VISIBLE);
            BaseActivity.slide_logout.setVisibility(View.GONE);
        } else {
            BaseActivity.slide_login.setVisibility(View.GONE);
            BaseActivity.slide_logout.setVisibility(View.VISIBLE);
        }
        if (preferences.getString(PreferenceManager.profile_pic, "").equals("") || preferences.getString(PreferenceManager.profile_pic, "").equals("null") || preferences.getString(PreferenceManager.profile_pic, "").equals(null)) {

        } else {
            Glide
                    .with(getActivity())
                    .load(preferences.getString(PreferenceManager.profile_pic, ""))
                    .centerCrop()
                    .into(BaseActivity.profile_image);
        }
        super.onResume();
    }
}
