package com.app.superkart.ui.activity.Shopping;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.AddressModel;
import com.app.superkart.model.Shopping.AllCouponModel;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class CityWiseCouponActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private TextView txt_title, txt_add, txt_msg;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private RecyclerView recycle_list;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<AllCouponModel> arrayList_coupon = new ArrayList<>();
    SharedPreferences pref_city;
    private Coupon_Adpter coupon_adpter;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.citywise_coupon_list);
        Log.e("CityWiseCouponActivity", "CityWiseCouponActivity");
        pref_city = getSharedPreferences("city", Context.MODE_PRIVATE);
        init();

        if (Utils.isNetworkAvailable(CityWiseCouponActivity.this)) {
            cityWiseCoupon();
        } else {
            Toast.makeText(CityWiseCouponActivity.this, "Please check your internert connection", Toast.LENGTH_LONG).show();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                if (Utils.isNetworkAvailable(CityWiseCouponActivity.this)) {
                    cityWiseCoupon();
                } else {
                    Toast.makeText(CityWiseCouponActivity.this, "Please check your internert connection", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);

        recycle_list = findViewById(R.id.recycle_addressList);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        linearLayoutManager = new LinearLayoutManager(CityWiseCouponActivity.this, RecyclerView.VERTICAL, false);
        recycle_list.setLayoutManager(linearLayoutManager);
        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        txt_msg = findViewById(R.id.txt_msg);
        txt_add = findViewById(R.id.txt_add);
        txt_title.setText("All Coupons");
        img_back.setOnClickListener(this);
        txt_add.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;

        }
    }

    private void cityWiseCoupon() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.cityWiseCoupon, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    arrayList_coupon.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            AllCouponModel model = new AllCouponModel();
                            model.setId(array_result.optJSONObject(i).optString("Coupon_Id"));
                            model.setCoupon_code(array_result.optJSONObject(i).optString("Coupon_Code"));
                            model.setNewImage_coupon(array_result.optJSONObject(i).optString("sellerimage"));
                            model.setDiscount_Price(array_result.optJSONObject(i).optString("Discount_Price"));
                            model.setDiscount_Type(array_result.optJSONObject(i).optString("Discount_Type"));
                            model.setDisc(array_result.optJSONObject(i).optString("disc"));
                            model.setSellername(array_result.optJSONObject(i).optString("sellername"));
                            model.setVendor_id(array_result.optJSONObject(i).optString("vendor_id"));
                            arrayList_coupon.add(model);
                        }
                        if (arrayList_coupon.size() > 0) {
                            recycle_list.setVisibility(View.VISIBLE);
                            txt_msg.setVisibility(View.VISIBLE);
                            coupon_adpter = new Coupon_Adpter(CityWiseCouponActivity.this, arrayList_coupon);
                            recycle_list.setAdapter(coupon_adpter);
                        } else {
                            recycle_list.setVisibility(View.GONE);
                            txt_msg.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recycle_list.setVisibility(View.GONE);
                        txt_msg.setVisibility(View.VISIBLE);
                        txt_msg.setText(json_main.optString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_city", pref_city.getString("city", ""));
                params.put("state", pref_city.getString("state", ""));
               //params.put("user_city", "Churu");
                Log.e("params", "" + Constants.cityWiseCoupon + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.cityWiseCoupon);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class Coupon_Adpter extends RecyclerView.Adapter<Coupon_Adpter.MyViewHolder> {

        private List<AllCouponModel> arrayList;
        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item;
            public TextView txt_code, txt_disc, txt_storeName;
            LinearLayout li_bg;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_code = view.findViewById(R.id.txt_code);
                txt_disc = view.findViewById(R.id.txt_disc);
                txt_storeName = view.findViewById(R.id.txt_storeName);
                li_bg = view.findViewById(R.id.li_bg);

            }
        }

        public Coupon_Adpter(Context context, List<AllCouponModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public Coupon_Adpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.citywise_coupon_adpter, parent, false);

            return new Coupon_Adpter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            AllCouponModel model = arrayList.get(position);

          /*  Picasso.with(context)
                    .load(model.getNewImage_coupon())
                    .into(holder.img_item);*/
            if (model.getNewImage_coupon().equals("") || model.getNewImage_coupon().equals("null") || model.getNewImage_coupon().equals(null)) {

            } else {
                Glide
                        .with(context)
                        .load(model.getNewImage_coupon())
                        .centerCrop()
                        .into(holder.img_item);
            }
            if (model.getCoupon_code().equals("") || model.getCoupon_code().equals("null") || model.getCoupon_code().equals(null)) {

            } else {
                holder.txt_code.setText(model.getCoupon_code());
            }

            if (model.getDisc().equals("") || model.getDisc().equals("null") || model.getDisc().equals(null)) {

            } else {
                holder.txt_disc.setText(model.getDisc());
            }


            if (model.getSellername().equals("") || model.getSellername().equals("null") || model.getSellername().equals(null)) {

            } else {
                holder.txt_storeName.setText(model.getSellername());
            }
            holder.li_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, SellerForyouDetailActivity.class);
                    i.putExtra("seller_id", model.getVendor_id());
                    context.startActivity(i);
                }
            });



        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

}
