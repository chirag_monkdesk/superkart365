package com.app.superkart.ui.activity.Service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.adpters.Service.Home_Visit_Palce_Adpter;
import com.app.superkart.model.Service.ServiceModel;
import com.app.superkart.utils.Utils;

import java.util.ArrayList;

public class PalaceOrder_HomeVisitActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ArrayList<ServiceModel> arrayList = new ArrayList<>();
    private RecyclerView recycle_item;
    private LinearLayoutManager linearLayoutManager;
    private Home_Visit_Palce_Adpter home_visit_palce_adpter;
    private ImageView img_back;
    private TextView txt_shop, txt_home;
    private TextView btn_placeOrder;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.place_order_home_visit);
        Log.e("PalaceOrder_HomeVisitActivity", "PalaceOrder_HomeVisitActivity");
        init();
        txt_shop.setBackgroundResource(R.drawable.bg_btn);
        txt_home.setBackgroundResource(R.drawable.boc_corner);
        txt_shop.setTextColor(Color.parseColor("#FFFFFFFF"));
        txt_home.setTextColor(Color.parseColor("#FF000000"));
        BinSubdData();
    }

    public void init() {
        txt_shop = findViewById(R.id.txt_shop);
        txt_home = findViewById(R.id.txt_home);
        img_back = findViewById(R.id.img_back);
        btn_placeOrder = findViewById(R.id.btn_placeOrder);
        img_back.setOnClickListener(this);
        txt_shop.setOnClickListener(this);
        txt_home.setOnClickListener(this);
        btn_placeOrder.setOnClickListener(this);
    }

    public void BinSubdData() {
        arrayList.clear();
        ServiceModel model;

        model = new ServiceModel(R.drawable.waste_pipe, "Waste Pipe");
        arrayList.add(model);

        model = new ServiceModel(R.drawable.washbasin_repair, "Washbasin Repair");
        arrayList.add(model);

        recycle_item = findViewById(R.id.recycle_item);
        linearLayoutManager = new LinearLayoutManager(PalaceOrder_HomeVisitActivity.this, LinearLayoutManager.VERTICAL, false);
        recycle_item.setLayoutManager(linearLayoutManager);
        home_visit_palce_adpter = new Home_Visit_Palce_Adpter(PalaceOrder_HomeVisitActivity.this, arrayList);
        recycle_item.setAdapter(home_visit_palce_adpter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.txt_shop:
                txt_shop.setBackgroundResource(R.drawable.bg_btn);
                txt_home.setBackgroundResource(R.drawable.boc_corner);
                txt_shop.setTextColor(Color.parseColor("#FFFFFFFF"));
                txt_home.setTextColor(Color.parseColor("#FF000000"));
                break;
            case R.id.txt_home:
                txt_shop.setBackgroundResource(R.drawable.boc_corner);
                txt_home.setBackgroundResource(R.drawable.bg_btn);
                txt_shop.setTextColor(Color.parseColor("#FF000000"));
                txt_home.setTextColor(Color.parseColor("#FFFFFFFF"));
                Intent i = new Intent(mcontext, HomeVisitActivity.class);
                startActivity(i);
                break;
            case R.id.btn_placeOrder:
                Intent intent = new Intent(mcontext, ServicePalaceOrderActivity.class);
                startActivity(intent);
                break;
        }
    }
}
