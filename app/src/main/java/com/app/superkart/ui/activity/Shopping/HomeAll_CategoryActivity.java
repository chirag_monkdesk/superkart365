package com.app.superkart.ui.activity.Shopping;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.adpters.Shopping.HomeAllCategoryAdpter;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeAll_CategoryActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private RecyclerView recycle_category;
    GridLayoutManager gridLayoutManager;
    private ArrayList<HomeCategoryModel> arrayList_homecategory = new ArrayList<>();
    HomeAllCategoryAdpter homeCategoryAdpter;
    private ImageView img_back;
    private ProgressDialog progressDialog;
    View view = null;
    private SwipeRefreshLayout swipeRefreshLayout;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.home_all_category);

        Log.e("HomeAll_CategoryActivity", "HomeAll_CategoryActivity");
        init();
        if (Utils.isNetworkAvailable(HomeAll_CategoryActivity.this)) {
            AllCategory();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                if (Utils.isNetworkAvailable(HomeAll_CategoryActivity.this)) {
                    AllCategory();
                } else {
                    Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_category = findViewById(R.id.recycle_category);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }

    private void AllCategory() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList_homecategory.clear();
        Log.e("AllCategory", "" + Constants.AllCategory);
        final StringRequest strReq = new StringRequest(Request.Method.GET, Constants.AllCategory, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONArray json_result = json_main.optJSONArray("result");
                        bindCategoryData(json_result);
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.New_BASE_URL);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void bindCategoryData(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            HomeCategoryModel homeCategoryModel = new HomeCategoryModel();
            homeCategoryModel.setCategory_id(jsonArray.optJSONObject(i).optString("category_id"));
            homeCategoryModel.setItem_name(jsonArray.optJSONObject(i).optString("category_name"));
            homeCategoryModel.setCate_image(jsonArray.optJSONObject(i).optString("category_image"));
            homeCategoryModel.setIs_parent(jsonArray.optJSONObject(i).optString("is_parent"));
            arrayList_homecategory.add(homeCategoryModel);
        }

        recycle_category = findViewById(R.id.recycle_category);
        gridLayoutManager = new GridLayoutManager(HomeAll_CategoryActivity.this, 2);
        recycle_category.setLayoutManager(gridLayoutManager);
        homeCategoryAdpter = new HomeAllCategoryAdpter(HomeAll_CategoryActivity.this, arrayList_homecategory);
        recycle_category.setAdapter(homeCategoryAdpter);
        homeCategoryAdpter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }
}
