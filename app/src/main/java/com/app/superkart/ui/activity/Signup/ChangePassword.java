package com.app.superkart.ui.activity.Signup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.superkart.R;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private LinearLayout li_back;
    private TextView btn_save;
    private EditText et_oldPass, et_Newpassword, et_confirmPass;
    public String str_oldPass = "", str_password = "", str_confirm_pass = "";
    private SharedPreferences preferences;
    private CoordinatorLayout coordinatorLayout;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.change_password_layout);
        Log.e("ChangePassword", "ChangePassword");
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        init();
    }

    public void init() {
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        et_oldPass = findViewById(R.id.et_oldPass);
        et_Newpassword = findViewById(R.id.et_Newpassword);
        et_confirmPass = findViewById(R.id.et_confirmPass);
        img_back = findViewById(R.id.img_back);
        li_back = findViewById(R.id.li_back);
        btn_save = findViewById(R.id.btn_save);
        li_back.setOnClickListener(this);
        btn_save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.li_back:
                Utils.hideKeyboard(ChangePassword.this);
                finish();
                break;
            case R.id.btn_save:
                Utils.hideKeyboard(ChangePassword.this);
                str_oldPass = et_oldPass.getText().toString().trim();
                str_password = et_Newpassword.getText().toString().trim();
                str_confirm_pass = et_confirmPass.getText().toString().trim();
                if (checkValidations1(v)) {
                    if (Utils.isNetworkAvailable(ChangePassword.this)) {
                        ChangePassword(v, str_oldPass, str_confirm_pass);
                    } else {
                        Utils.showSnackBarr(v, getResources().getString(R.string.check_internet), snackbar, coordinatorLayout);
                    }
                }

                break;
        }
    }

    public void ChangePassword(View v, String oldPass, String CPassword) {
        ProgressDialog progressDialog_main = new ProgressDialog(ChangePassword.this, R.style.AppCompatAlertDialogStyle);
        progressDialog_main.setMessage("Loading...");
        progressDialog_main.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog_main.setIndeterminate(true);
        progressDialog_main.setCancelable(false);
        progressDialog_main.show();
        String url = Constants.changepassword;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog_main.dismiss();
                            Utils.hideKeyboard(ChangePassword.this);
                            JSONObject obj_main = new JSONObject(response);
                            Log.e("response", response);
                            if (obj_main.optString("status").equals("1")) {
                                Toast.makeText(ChangePassword.this, obj_main.optString("msg"), Toast.LENGTH_LONG).show();
                                et_oldPass.setText("");
                                et_Newpassword.setText("");
                                et_confirmPass.setText("");
                                finish();
                            } else {
                                Utils.showSnackBarr(v, obj_main.optString("msg"), snackbar, coordinatorLayout);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog_main.isShowing())
                                progressDialog_main.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (progressDialog_main.isShowing())
                            progressDialog_main.dismiss();
                        String message = "";
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            // message = getResources().getString(R.string.internet_error);
                        }
                        // Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Utils.showSnackBarr(v, message, snackbar, coordinatorLayout);
                        //   showSnack(message);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                //params.put("Content-Type", "multipart/form-data");
                //  params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("oldpassword", oldPass);
                params.put("newpassword", CPassword);
                Log.e("params", "" + url + params);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private boolean checkValidations1(View v) {
        boolean result = true;
        if (et_oldPass.getText().toString().equals("")) {
            Utils.showSnackBarr(v, "Please enter your current password.", snackbar, coordinatorLayout);
            result = false;
        } else if (et_Newpassword.getText().toString().equals("")) {
            Utils.showSnackBarr(v, "Please enter your new password.", snackbar, coordinatorLayout);
            result = false;
        } else if (et_confirmPass.getText().toString().equals("")) {
            Utils.showSnackBarr(v, "Please enter confirm password.", snackbar, coordinatorLayout);
            result = false;
        } else if (!et_Newpassword.getText().toString().equals(et_confirmPass.getText().toString())) {
            Utils.showSnackBarr(v, "Your password mismatched.", snackbar, coordinatorLayout);
            result = false;
        }
        return result;
    }

    @Override
    protected void onResume() {
        Utils.hideKeyboard(ChangePassword.this);
        super.onResume();
    }
}
