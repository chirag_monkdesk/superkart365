package com.app.superkart.ui.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.app.superkart.R;
import com.app.superkart.ui.activity.Shopping.AddAdressActivity;
import com.app.superkart.ui.activity.Shopping.Add_To_Bag_Activity;
import com.app.superkart.ui.activity.Shopping.AddressListActivity;
import com.app.superkart.ui.activity.Shopping.Home_ViewAll_Category_List;
import com.app.superkart.ui.activity.Shopping.MyOrderListActivity;
import com.app.superkart.ui.activity.Shopping.UserLoyaltyPointsActivity;
import com.app.superkart.ui.activity.Shopping.WhishListActivity;
import com.app.superkart.ui.activity.Signup.LoginActivity;
import com.app.superkart.ui.fragment.CartFragment;
import com.app.superkart.ui.fragment.CategoryFragment;
import com.app.superkart.ui.fragment.HomeFragmentNew;
import com.app.superkart.ui.fragment.ProfileFragment;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.BottomNavigationViewBehavior;
import com.app.superkart.utils.GPSTracker;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.ScannerActvity;
import com.app.superkart.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.app.superkart.ui.fragment.HomeFragmentNew.txt_location;


public class BaseActivity extends AppCompatActivity implements LocationListener {
    
    private TextView txt_ScreenTitile;
    public static TextView txt_cart_count, txt_name, txt_noti_count;
    public static RelativeLayout rl_notification;
    public static BottomNavigationView navigation;
    Menu menu;
    RelativeLayout rl_header, rel_add_popup;
    public static LinearLayout li_scan, li_serach;
    private DrawerLayout Drawer_layout;
    ImageView drawer_menu, img_rupee, img_cancel, img_popclick;
    private ImageView img_home, img_hot_deal, img_bag, img_cart, img_whishlist, img_prfile, img_loyality, img_coupon, img_card, img_location, img_help, img_setting, img_category, img_cartTab, img_ProfileTab;
    public LinearLayout slide_home, slide_deal, slide_yr_order, slide_buy_again, slide_wishlist, slide_yr_account, slide_loyal_points,
            slide_my_coupon, slide_save_card, slide_address, slide_help, slide_setting, slide_category, slide_cart, slide_profile;
    public static LinearLayout slide_login, slide_logout;
    String latitude, longitude, address, city, state, country, postalCode;
    List<Address> addresses = new ArrayList<>();
    private final int REQUEST_LOCATION_PERMISSION = 1;
    public PreferenceManager preferenceManager;
    public static CardView card_bottom_Currency_btn;
    SharedPreferences preferences;
    SharedPreferences preferences_token;
    Context mcontext;
    private IntentIntegrator qrScan;
    private GoogleApiClient googleApiClient;
    final static int REQUEST_LOCATION = 199;

    final String TAG = "GPS";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    SharedPreferences pref_city;
    Window window;
    SharedPreferences pref_cart;
    public static TextView et_global_search;
    public static ImageView profile_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.activity_base_new);
        Log.e("BaseActivity", "BaseActivity");
        preferenceManager = new PreferenceManager(this);
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        preferences_token = getSharedPreferences("X", Context.MODE_PRIVATE);
        pref_cart = getSharedPreferences("cart_count", Context.MODE_PRIVATE);
        init();

        Log.e("DeivceToken", "" + preferences_token.getString(PreferenceManager.DeivceToken, ""));
        Log.e("user_id", "" + preferences.getString(PreferenceManager.user_id, ""));

//        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        //  layoutParams.setBehavior(new BottomNavigationViewBehavior());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            }
        }
        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);


        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            showSettingsAlert();
            getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            // check permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    Log.d(TAG, "Permission requests");
                    canGetLocation = false;
                }
            }

            // get location
            getLocation();
        }

        if (savedInstanceState == null) {
            HomeFragmentNew fm = new HomeFragmentNew();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_layout, fm, "Home_fragment")
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit();
        }
        initComponent();
        loadFragment(new HomeFragmentNew());

        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
            slide_login.setVisibility(View.VISIBLE);
            slide_logout.setVisibility(View.GONE);
        } else {
            slide_login.setVisibility(View.GONE);
            slide_logout.setVisibility(View.VISIBLE);
        }

        if (preferences.getString(PreferenceManager.totalnotification, "").equals("") || preferences.getString(PreferenceManager.totalnotification, "").equals("null") ||
                preferences.getString(PreferenceManager.totalnotification, "").equals(null) || preferences.getString(PreferenceManager.totalnotification, "") == null ||
                preferences.getString(PreferenceManager.totalnotification, "").equals("0")) {
            txt_noti_count.setVisibility(View.INVISIBLE);
        } else {
            txt_noti_count.setVisibility(View.VISIBLE);
            txt_noti_count.setText(preferences.getString(PreferenceManager.totalnotification, ""));
        }

        if (preferences.getString(PreferenceManager.username, "").equals("") || preferences.getString(PreferenceManager.username, "").equals("null") ||
                preferences.getString(PreferenceManager.username, "").equals(null) || preferences.getString(PreferenceManager.username, "") == null) {
            txt_name.setText(preferences.getString(PreferenceManager.email_id, ""));
            txt_name.setText("--");
        } else {
            txt_name.setText(preferences.getString(PreferenceManager.username, ""));
        }
        if (preferences.getString(PreferenceManager.profile_pic, "").equals("") || preferences.getString(PreferenceManager.profile_pic, "").equals("null") ||
                preferences.getString(PreferenceManager.profile_pic, "").equals(null) || preferences.getString(PreferenceManager.profile_pic, "") == null) {

        } else {
            Picasso.with(BaseActivity.this)
                    .load(preferences.getString(PreferenceManager.profile_pic, ""))
                    .into(profile_image);

        }

        qrScan = new IntentIntegrator(this);
        li_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // qrScan.initiateScan();
                /*SharedPreferences cart_count = getSharedPreferences("cart_count", 0);
                cart_count.edit().clear().commit();*/
                AppConstant.scan_open = "1";
                AppConstant.scannning_view = "1";
                rel_add_popup.setVisibility(View.GONE);
                img_rupee.setVisibility(View.VISIBLE);
                img_cancel.setVisibility(View.GONE);
                Intent i = new Intent(BaseActivity.this, ScannerActvity.class);
                startActivity(i);

            }
        });
        img_rupee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showTitle3(window, mcontext);
                rel_add_popup.setVisibility(View.VISIBLE);
                img_rupee.setVisibility(View.GONE);
                img_cancel.setVisibility(View.VISIBLE);
            }
        });
        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showTitle(window, mcontext);
                rel_add_popup.setVisibility(View.GONE);
                img_rupee.setVisibility(View.VISIBLE);
                img_cancel.setVisibility(View.GONE);
            }
        });
        img_popclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        img_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.scannning_view = "0";
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_order = new Intent(BaseActivity.this, Add_To_Bag_Activity.class);
                    startActivity(i_order);
                }
            }
        });

        rl_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_order = new Intent(BaseActivity.this, NotificationListActivity.class);
                    startActivity(i_order);
                }
            }
        });
    }

    public void init() {
        et_global_search = findViewById(R.id.et_global_search);
        txt_name = findViewById(R.id.txt_name);
        li_scan = findViewById(R.id.li_scan);
        img_rupee = findViewById(R.id.img_rupee);
        img_cancel = findViewById(R.id.img_cancel);
        img_popclick = findViewById(R.id.img_popclick);
        rel_add_popup = findViewById(R.id.rel_add_popup);
        li_serach = findViewById(R.id.li_serach);
        txt_ScreenTitile = findViewById(R.id.txt_ScreenTitile);
        txt_cart_count = findViewById(R.id.txt_cart_count);
        profile_image = findViewById(R.id.profile_image);
        txt_noti_count = findViewById(R.id.txt_noti_count);
        rl_notification = findViewById(R.id.rl_notification);

        card_bottom_Currency_btn = findViewById(R.id.card_bottom_Currency_btn);
        navigation = findViewById(R.id.nav_view12);
        navigation.setItemIconTintList(null);
        rl_header = findViewById(R.id.rl_header);
        menu = navigation.getMenu();
        menu.findItem(R.id.navigation_home).setIcon(R.drawable.home_selected);

        Drawer_layout = findViewById(R.id.Drawer_layout);
        drawer_menu = findViewById(R.id.drawer_menu);
        drawer_menu.setOnClickListener(onClickListenerBase);

        slide_home = findViewById(R.id.slide_home);
        slide_deal = findViewById(R.id.slide_deal);
        slide_yr_order = findViewById(R.id.slide_yr_order);
        slide_buy_again = findViewById(R.id.slide_buy_again);
        slide_wishlist = findViewById(R.id.slide_wishlist);
        slide_yr_account = findViewById(R.id.slide_yr_account);
        slide_loyal_points = findViewById(R.id.slide_loyal_points);
        slide_my_coupon = findViewById(R.id.slide_my_coupon);
        slide_save_card = findViewById(R.id.slide_save_card);
        slide_address = findViewById(R.id.slide_address);
        slide_help = findViewById(R.id.slide_help);
        slide_setting = findViewById(R.id.slide_setting);
        slide_login = findViewById(R.id.slide_login);
        slide_logout = findViewById(R.id.slide_logout);
        slide_category = findViewById(R.id.slide_category);
        slide_cart = findViewById(R.id.slide_cart);
        slide_profile = findViewById(R.id.slide_profile);

        img_home = findViewById(R.id.img_home);
        img_hot_deal = findViewById(R.id.img_hot_deal);
        img_bag = findViewById(R.id.img_bag);
        img_cart = findViewById(R.id.img_cart);
        img_whishlist = findViewById(R.id.img_whishlist);
        img_prfile = findViewById(R.id.img_prfile);
        img_loyality = findViewById(R.id.img_loyality);
        img_coupon = findViewById(R.id.img_coupon);
        img_card = findViewById(R.id.img_card);
        img_location = findViewById(R.id.img_location);
        img_help = findViewById(R.id.img_help);
        img_setting = findViewById(R.id.img_setting);
        img_category = findViewById(R.id.img_category);
        img_cartTab = findViewById(R.id.img_cartTab);
        img_ProfileTab = findViewById(R.id.img_ProfileTab);


    }

    @SuppressLint("ResourceType")
    private void loadFragment(Fragment fragment) {
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_layout, fragment)
                .commitAllowingStateLoss();
    }


    private void initComponent() {
        navigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    img_cart.setEnabled(true);
                    AppConstant.check_cart_view = "";
                    AppConstant.check_cart = "";
                    AppConstant.scannning_view = "0";
                    li_serach.setVisibility(View.VISIBLE);
                    rl_header.setVisibility(View.VISIBLE);
                    txt_ScreenTitile.setVisibility(View.GONE);
                    slide_home.setVisibility(View.VISIBLE);
                    slide_category.setVisibility(View.GONE);
                    slide_cart.setVisibility(View.GONE);
                    slide_profile.setVisibility(View.GONE);
                    img_home.setBackgroundResource(R.drawable.home_blue);
                    menu.findItem(R.id.navigation_home).setIcon(R.drawable.home_selected);
                    menu.findItem(R.id.navigation_cate).setIcon(R.drawable.cate_unselect);
                    menu.findItem(R.id.navigation_cart).setIcon(R.drawable.cart_unselect);
                    menu.findItem(R.id.navigation_Profile).setIcon(R.drawable.profile_unselect);
                    loadFragment(new HomeFragmentNew());
                    return true;
                case R.id.navigation_cate:
                    img_cart.setEnabled(true);
                    AppConstant.check_cart_view = "";
                    AppConstant.check_cart = "";
                    AppConstant.scannning_view = "0";
                    li_serach.setVisibility(View.GONE);
                    rl_header.setVisibility(View.VISIBLE);
                    txt_ScreenTitile.setVisibility(View.VISIBLE);
                    txt_ScreenTitile.setText("All Categories");
                    slide_home.setVisibility(View.GONE);
                    slide_category.setVisibility(View.VISIBLE);
                    slide_cart.setVisibility(View.GONE);
                    slide_profile.setVisibility(View.GONE);
                    img_category.setBackgroundResource(R.drawable.cate_selected);
                    menu.findItem(R.id.navigation_home).setIcon(R.drawable.home_unselect);
                    menu.findItem(R.id.navigation_cate).setIcon(R.drawable.cate_selected);
                    menu.findItem(R.id.navigation_cart).setIcon(R.drawable.cart_unselect);
                    menu.findItem(R.id.navigation_Profile).setIcon(R.drawable.profile_unselect);
                    loadFragment(new CategoryFragment());
                    return true;
                case R.id.navigation_cart:
                    img_cart.setEnabled(false);
                    AppConstant.check_cart_view = "view";
                    AppConstant.check_cart = "view";
                    AppConstant.scannning_view = "0";
                    li_serach.setVisibility(View.GONE);
                    rl_header.setVisibility(View.VISIBLE);
                    txt_ScreenTitile.setVisibility(View.VISIBLE);
                    txt_ScreenTitile.setText("Cart");
                    slide_home.setVisibility(View.GONE);
                    slide_category.setVisibility(View.GONE);
                    slide_cart.setVisibility(View.VISIBLE);
                    slide_profile.setVisibility(View.GONE);
                    img_cartTab.setBackgroundResource(R.drawable.cart_selected);
                    menu.findItem(R.id.navigation_home).setIcon(R.drawable.home_unselect);
                    menu.findItem(R.id.navigation_cate).setIcon(R.drawable.cate_unselect);
                    menu.findItem(R.id.navigation_cart).setIcon(R.drawable.cart_selected);
                    menu.findItem(R.id.navigation_Profile).setIcon(R.drawable.profile_unselect);
                    if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                        Intent i_cart = new Intent(BaseActivity.this, LoginActivity.class);
                        startActivity(i_cart);
                    } else {
                        loadFragment(new CartFragment());
                    }

                    return true;
                case R.id.navigation_Profile:
                    img_cart.setEnabled(true);
                    AppConstant.check_cart_view = "";
                    AppConstant.check_cart = "";
                    AppConstant.scannning_view = "0";
                    li_serach.setVisibility(View.GONE);
                    rl_header.setVisibility(View.VISIBLE);
                    txt_ScreenTitile.setVisibility(View.VISIBLE);
                    txt_ScreenTitile.setText("Profile");
                    slide_home.setVisibility(View.GONE);
                    slide_category.setVisibility(View.GONE);
                    slide_cart.setVisibility(View.GONE);
                    slide_profile.setVisibility(View.VISIBLE);
                    img_prfile.setBackgroundResource(R.drawable.profile_selected);
                    menu.findItem(R.id.navigation_home).setIcon(R.drawable.home_unselect);
                    menu.findItem(R.id.navigation_cate).setIcon(R.drawable.cate_unselect);
                    menu.findItem(R.id.navigation_cart).setIcon(R.drawable.cart_unselect);
                    menu.findItem(R.id.navigation_Profile).setIcon(R.drawable.profile_selected);
                    if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                        Intent i_addreess = new Intent(BaseActivity.this, LoginActivity.class);
                        startActivity(i_addreess);
                    } else {
                        loadFragment(new ProfileFragment());
                    }
                    return true;

            }
            return false;
        });
    }

    View.OnClickListener onClickListenerBase = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.drawer_menu:
                    openDrawer();
                    break;
            }
        }
    };

    public void openDrawer() {
        if (Drawer_layout.isDrawerVisible(GravityCompat.START)) {
            Drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            Drawer_layout.openDrawer(GravityCompat.START);
        }
    }

    public void SlideMenu(View v) {
        Drawer_layout.closeDrawers();
        switch (v.getId()) {
            case R.id.slide_home:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home_blue);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                //img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                //loadFragment(new HomeFragmentNew());
                break;
            case R.id.slide_deal:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal_blue);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                //   img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_yr_order:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_blue);
                // img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_order = new Intent(BaseActivity.this, MyOrderListActivity.class);
                    startActivity(i_order);
                }
                break;
            case R.id.slide_buy_again:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                //   img_cart.setBackgroundResource(R.drawable.cart_blue);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_wishlist:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                //  img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist_blue);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_whish = new Intent(BaseActivity.this, WhishListActivity.class);
                    startActivity(i_whish);
                }
                break;
            case R.id.slide_yr_account:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                // img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile_blue);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_loyal_points:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                //    img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty_blue);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);

                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_addreess = new Intent(BaseActivity.this, UserLoyaltyPointsActivity.class);
                    startActivity(i_addreess);
                }
                break;
            case R.id.slide_my_coupon:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                //  img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons_blue);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_save_card:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                //   img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card_blue);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_address:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                //  img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location_blue);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_addreess = new Intent(BaseActivity.this, AddressListActivity.class);
                    startActivity(i_addreess);
                }

                break;
            case R.id.slide_help:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                // img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help_blue);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_setting:
                AppConstant.check_cart_view = "";
                AppConstant.check_cart = "";
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                // img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings_blue);
                break;
            case R.id.slide_login:
                AppConstant.check_cart_view = "";
                AppConstant.scannning_view = "0";
                AppConstant.check_cart = "";
                Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                startActivity(i);
                break;
            case R.id.slide_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this, R.style.AppCompatAlertDialogStyle);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Do you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        AppConstant.scannning_view = "0";
                        SharedPreferences preferences = getSharedPreferences(mcontext.getString(R.string.app_name), 0);
                        preferences.edit().clear().commit();
                        SharedPreferences cart_count = getSharedPreferences("cart_count", 0);
                        cart_count.edit().clear().commit();
                        SharedPreferences cart_count_offine = getSharedPreferences("cart_offinr_count", 0);
                        cart_count_offine.edit().clear().commit();
                        slide_login.setVisibility(View.VISIBLE);
                        slide_logout.setVisibility(View.GONE);
                        txt_name.setText("--");
                        AppConstant.check_cart_view = "";
                        AppConstant.check_cart = "";
                        Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                        startActivity(i);
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        AppConstant.check_cart_view = "";
                    }
                });
                AlertDialog alert = builder.create();
                alert.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
                        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                        //dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.black));
                    }
                });
                alert.show();

                break;
        }
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                //Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                try {
                    //converting the data to json
                    JSONObject obj = new JSONObject(result.getContents());
                  /*  //setting values to textviews
                    textViewName.setText(obj.getString("name"));
                    textViewAddress.setText(obj.getString("address"));*/
                } catch (JSONException e) {
                    e.printStackTrace();
                    //if control comes here
                    //that means the encoded format not matches
                    //in this case you can display whatever data is available on the qrcode
                    //to a toast
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    private void clearBackStack() {

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment instanceof HomeFragmentNew) {
                continue;
            } else {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
        }
    }

    public void Replace_Fragment(Fragment fr, String Name) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.frame, fr, Name)
                .addToBackStack(Name)
                .commitAllowingStateLoss();
    }

    @Override
    protected void onResume() {
        Utils.hideKeyboard(BaseActivity.this);
        Utils.showTitle(window, mcontext);

        img_home.setBackgroundResource(R.drawable.home_blue);
        img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
        img_bag.setBackgroundResource(R.drawable.shopping_bag);
        //  img_cart.setBackgroundResource(R.drawable.cart_new_gray);
        img_whishlist.setBackgroundResource(R.drawable.wishlist);
        img_prfile.setBackgroundResource(R.drawable.profile);
        img_loyality.setBackgroundResource(R.drawable.loyalty);
        img_coupon.setBackgroundResource(R.drawable.my_coupons);
        img_card.setBackgroundResource(R.drawable.card);
        img_location.setBackgroundResource(R.drawable.location);
        img_help.setBackgroundResource(R.drawable.help);
        img_setting.setBackgroundResource(R.drawable.settings);
        pref_cart = getSharedPreferences("cart_count", Context.MODE_PRIVATE);
        if (pref_cart.getString(PreferenceManager.Cart_Count, "").equals("") || pref_cart.getString(PreferenceManager.Cart_Count, "").equals("0")) {
            txt_cart_count.setVisibility(View.INVISIBLE);
        } else {
            txt_cart_count.setText(pref_cart.getString(PreferenceManager.Cart_Count, ""));
            txt_cart_count.setVisibility(View.VISIBLE);
        }

        if (preferences.getString(PreferenceManager.totalnotification, "").equals("") || preferences.getString(PreferenceManager.totalnotification, "").equals("null") ||
                preferences.getString(PreferenceManager.totalnotification, "").equals(null) || preferences.getString(PreferenceManager.totalnotification, "") == null ||
                preferences.getString(PreferenceManager.totalnotification, "").equals("0")) {
            txt_noti_count.setVisibility(View.INVISIBLE);
        } else {
            txt_noti_count.setVisibility(View.VISIBLE);
            txt_noti_count.setText(preferences.getString(PreferenceManager.totalnotification, ""));
        }

        if (preferences.getString(PreferenceManager.username, "").equals("") || preferences.getString(PreferenceManager.username, "").equals("null") ||
                preferences.getString(PreferenceManager.username, "").equals(null) || preferences.getString(PreferenceManager.username, "") == null) {
            txt_name.setText(preferences.getString(PreferenceManager.email_id, ""));
            txt_name.setText("--");
        } else {
            txt_name.setText(preferences.getString(PreferenceManager.username, ""));
        }
        if (preferences.getString(PreferenceManager.profile_pic, "").equals("") || preferences.getString(PreferenceManager.profile_pic, "").equals("null") ||
                preferences.getString(PreferenceManager.profile_pic, "").equals(null) || preferences.getString(PreferenceManager.profile_pic, "") == null) {

        } else {
            Picasso.with(BaseActivity.this)
                    .load(preferences.getString(PreferenceManager.profile_pic, ""))
                    .into(profile_image);

        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        Log.d(TAG, "onLocationChanged");
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else {
                    loc.setLatitude(0);
                    loc.setLongitude(0);
                    updateUI(loc);
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }
                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(BaseActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void updateUI(Location loc) {
        Log.d(TAG, "updateUI");
        // Log.e("base getLatitude", "" + Double.toString(loc.getLatitude()));
        //   Log.e("base getLongitude", "" + Double.toString(loc.getLongitude()));
        if (AppConstant.Current_Location.equals("0")) {
            GPSTracker();
            AppConstant.Current_Location = "1";
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    public void GPSTracker() {
        GPSTracker mGPS = new GPSTracker(BaseActivity.this);
        if (mGPS.canGetLocation) {
            mGPS.getLocation();

            latitude = String.valueOf(mGPS.getLatitude());
            longitude = String.valueOf(mGPS.getLongitude());

            //Log.e("base lat", latitude);
            // Log.e("base long", longitude);

            Geocoder geocoder = new Geocoder(BaseActivity.this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(mGPS.getLatitude(), mGPS.getLongitude(), 1); //1 num of possible location returned
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); //0 to obtain first possible address
                city = addresses.get(0).getLocality();
                state = addresses.get(0).getAdminArea();
                country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                SharedPreferences pref = getSharedPreferences("city", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("city", city);
                editor.putString("state", state);
                editor.commit();
                // commented on 27 sept 19 bcoz of screen crash
                // create your custom title
                String title = address + "-" + city + "-" + state;
                /*Log.e("addresss", address);
                Log.e("title", title);

                Log.e("Dist", addresses.get(0).getSubAdminArea());
                Log.e("state", addresses.get(0).getAdminArea());
                Log.e("country", addresses.get(0).getCountryName());
                Log.e("postalCode", addresses.get(0).getPostalCode());
*/
              /*  Log.e("getSubLocality", addresses.get(0).getSubLocality());
                Log.e("city", addresses.get(0).getLocality());*/

                pref_city = getSharedPreferences("city", Context.MODE_PRIVATE);
                loadFragment(new HomeFragmentNew());


            }
        }


    }

}
