package com.app.superkart.ui.activity.Signup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.superkart.R;
import com.app.superkart.reciever.SmsListener;
import com.app.superkart.reciever.SmsReceiver;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OTPActivity extends Activity implements View.OnClickListener {

    private Context mcontext;
    private ImageView img_back;
    private LinearLayout li_back;
    private TextView btn_verify, btn_resend;
    private EditText et_1, et_2, et_3, et_4, et_5, et_6;
    private LinearLayout li_back_to_login;
    public PreferenceManager preferenceManager;
    String otp;
    SharedPreferences preferences;
    private CoordinatorLayout coordinatorLayout;
    private Snackbar snackbar;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                Log.e("message", "" + message);
                // your_edittext.setText(message);
                //Do whatever you want with the code here
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_verify_layout);
        Log.e("OTPActivity", "OTPActivity");
        mcontext = this;
        Window window = this.getWindow();
        Utils.showTitle1(window, mcontext);
        preferenceManager = new PreferenceManager(this);
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        init();
        Log.e("userid", "" + preferences.getString(PreferenceManager.user_id, ""));
        Log.e("email", "" + preferences.getString(PreferenceManager.email_id, ""));
        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Log.e("messageText", "" + messageText);
                //ed.setText(messageText);
            }
        });


        et_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (et_1.getText().toString().trim().length() == 1) {
                    et_2.requestFocus();
                }
            }
        });

        et_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (et_2.getText().toString().trim().length() == 1) {
                    et_3.requestFocus();
                }
            }
        });

        et_3.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (et_3.getText().toString().trim().length() == 1) {
                    et_4.requestFocus();
                }
            }
        });

        et_4.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (et_4.getText().toString().trim().length() == 1) {
                    et_5.requestFocus();
                }
            }
        });

        et_5.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (et_5.getText().toString().trim().length() == 1) {
                    et_6.requestFocus();

                }
            }
        });

        et_6.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // add a condition to check length here - you can give here length according to your requirement to go to next EditTexts.
                if (et_6.getText().toString().trim().length() == 1) {
                    otp = et_1.getText().toString();
                    otp += et_2.getText().toString();
                    otp += et_3.getText().toString();
                    otp += et_4.getText().toString();
                    otp += et_5.getText().toString();
                    otp += et_6.getText().toString();
                    Log.e("otp", "" + otp);
                }
            }
        });
    }

    public void init() {

        et_1 = findViewById(R.id.et_1);
        et_2 = findViewById(R.id.et_2);
        et_3 = findViewById(R.id.et_3);
        et_4 = findViewById(R.id.et_4);
        et_5 = findViewById(R.id.et_5);
        et_6 = findViewById(R.id.et_6);
        et_1.requestFocus();
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        img_back = findViewById(R.id.img_back);
        li_back = findViewById(R.id.li_back);
        btn_verify = findViewById(R.id.btn_verify);
        btn_resend = findViewById(R.id.btn_resend);
        li_back_to_login = findViewById(R.id.li_back_to_login);
        li_back.setOnClickListener(this);
        btn_verify.setOnClickListener(this);
        btn_resend.setOnClickListener(this);
        li_back_to_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.li_back:
                Utils.hideKeyboard(OTPActivity.this);
                finish();
                break;
            case R.id.btn_verify:

                Log.e("otp", "" + otp);
                Utils.hideKeyboard(OTPActivity.this);
                if (et_1.getText().toString().equals("") || et_2.getText().toString().equals("") || et_3.getText().toString().equals("") || et_4.getText().toString().equals("") || et_5.getText().toString().equals("") || et_6.getText().toString().equals("")) {
                    Utils.showSnackBarr(v, getResources().getString(R.string.pleaseenterotp), snackbar, coordinatorLayout);
                } else {
                    if (Utils.isNetworkAvailable(OTPActivity.this)) {
                        verifyOtp(v);
                    } else {
                        Utils.showSnackBarr(v, getResources().getString(R.string.check_internet), snackbar, coordinatorLayout);
                    }
                }

                break;
            case R.id.btn_resend:
                Utils.hideKeyboard(OTPActivity.this);
                if (Utils.isNetworkAvailable(OTPActivity.this)) {
                    resentOtp(v);
                } else {
                    Utils.showSnackBarr(v, getResources().getString(R.string.check_internet), snackbar, coordinatorLayout);
                }
                break;
            case R.id.li_back_to_login:

                break;
        }
    }

    private boolean checkValidations1(View v) {
        boolean result = true;
        if (et_1.getText().toString().equals("") || et_2.getText().toString().equals("") || et_3.getText().toString().equals("") || et_4.getText().toString().equals("") || et_5.getText().toString().equals("") || et_6.getText().toString().equals("")) {
            Utils.showSnackBarr(v, getResources().getString(R.string.pleaseenterotp), snackbar, coordinatorLayout);
            result = false;
        }
        return result;
    }


    public void verifyOtp(View v) {
        ProgressDialog progressDialog_main = new ProgressDialog(OTPActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDialog_main.setMessage("Loading...");
        progressDialog_main.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog_main.setIndeterminate(true);
        progressDialog_main.setCancelable(false);
        progressDialog_main.show();
        String url = Constants.verifyOtp;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog_main.dismiss();
                            Utils.hideKeyboard(OTPActivity.this);
                            JSONObject jsonobject = new JSONObject(response);
                            Log.e("response", response);
                            if (jsonobject.optString("status").equals("1")) {
                                Toast.makeText(mcontext, jsonobject.optString("msg"), Toast.LENGTH_LONG).show();
                                et_1.setText("");
                                et_2.setText("");
                                et_3.setText("");
                                et_4.setText("");
                                et_5.setText("");
                                et_6.setText("");
                                Intent i = new Intent(mcontext, BaseActivity.class);
                                startActivity(i);
                            } else {
                                Utils.showSnackBarr(v, jsonobject.optString("msg"), snackbar, coordinatorLayout);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog_main.isShowing())
                                progressDialog_main.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (progressDialog_main.isShowing())
                            progressDialog_main.dismiss();
                        String message = "";
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            // message = getResources().getString(R.string.internet_error);
                        }
                        // Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Utils.showSnackBarr(v, message, snackbar, coordinatorLayout);
                        //   showSnack(message);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("token", preferences.getString(PreferenceManager.token_in_return, ""));
                params.put("otp", otp);
                Log.e("params", "" + url + params);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void resentOtp(View v) {
        ProgressDialog progressDialog_main = new ProgressDialog(OTPActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDialog_main.setMessage("Loading...");
        progressDialog_main.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog_main.setIndeterminate(true);
        progressDialog_main.setCancelable(false);
        progressDialog_main.show();
        String url = Constants.ResendOtp;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog_main.dismiss();
                            Utils.hideKeyboard(OTPActivity.this);
                            JSONObject jsonobject = new JSONObject(response);
                            Log.e("response", response);
                            if (jsonobject.optString("status").equals("1")) {
                                Toast.makeText(mcontext, jsonobject.optString("msg"), Toast.LENGTH_LONG).show();
                            } else {
                                Utils.showSnackBarr(v, jsonobject.optString("msg"), snackbar, coordinatorLayout);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog_main.isShowing())
                                progressDialog_main.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (progressDialog_main.isShowing())
                            progressDialog_main.dismiss();
                        String message = "";
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            // message = getResources().getString(R.string.internet_error);
                        }
                        // Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Utils.showSnackBarr(v, message, snackbar, coordinatorLayout);
                        //   showSnack(message);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("token", preferences.getString(PreferenceManager.token_in_return, ""));
                Log.e("params", "" + url + params);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        Utils.hideKeyboard(OTPActivity.this);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


}
