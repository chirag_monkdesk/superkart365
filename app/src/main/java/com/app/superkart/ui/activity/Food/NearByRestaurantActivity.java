package com.app.superkart.ui.activity.Food;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.superkart.R;
import com.app.superkart.adpters.Food.FoodCouponAdpter;
import com.app.superkart.adpters.Food.NearByRastaurantAdpter;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.Utils;

import java.util.ArrayList;

public class NearByRestaurantActivity extends AppCompatActivity {
    private Context mcontext;
    private SharedPreferences preferences;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recycle_item;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<String> arrayList = new ArrayList<>();
    private NearByRastaurantAdpter nearByRastaurantAdpter;
    private TextView txt_title;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.nearby_restaurant_list);
        Log.e("NearByRestaurantActivity", "NearByRestaurantActivity");
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        init();
        BindData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);

            }
        });

        if( AppConstant.check_restaurant.equals("nearby")){
            txt_title.setText("All Restaurants Nearby");
        }else{
            txt_title.setText("Promoted Restaurants");
        }

    }

    public void init() {
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        txt_title = findViewById(R.id.txt_title);
        recycle_item = findViewById(R.id.recycle_item);
        linearLayoutManager = new LinearLayoutManager(mcontext, RecyclerView.VERTICAL, false);
    }

    public void BindData() {
        arrayList.clear();
        for (int i = 0; i < 10; i++) {
            arrayList.add("a");
        }

        recycle_item.setLayoutManager(linearLayoutManager);
        nearByRastaurantAdpter = new NearByRastaurantAdpter(mcontext, arrayList);
        recycle_item.setAdapter(nearByRastaurantAdpter);
    }
}
