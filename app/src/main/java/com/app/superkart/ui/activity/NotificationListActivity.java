package com.app.superkart.ui.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.AddressModel;
import com.app.superkart.model.Shopping.NotificationModel;
import com.app.superkart.ui.activity.Shopping.AddAdressActivity;
import com.app.superkart.ui.activity.Shopping.AddressListActivity;
import com.app.superkart.ui.activity.Shopping.DeliveryAddressActivity;
import com.app.superkart.ui.activity.Shopping.OrderdetailActivity;
import com.app.superkart.ui.activity.Shopping.SellerForyouActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationListActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private TextView txt_add, txt_title;
    private ImageView img_back;
    private RecyclerView recycle_addressList;
    private LinearLayoutManager linearLayoutManager;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private NotificationAdpter notificationAdpter;
    ArrayList<NotificationModel> arrayList = new ArrayList<>();
    private LinearLayout li_error_msg;
    String order_id = "", Payment_mode = "", Order_status = "";
    private SwipeRefreshLayout swipeRefreshLayout;
    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.notification_list_layout);
        Log.e("NotificationListActivity", "NotificationListActivity");
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        init();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                if (Utils.isNetworkAvailable(NotificationListActivity.this)) {
                    showAllPushNotification();
                } else {
                    Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_addressList = findViewById(R.id.recycle_addressList);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        linearLayoutManager = new LinearLayoutManager(NotificationListActivity.this, RecyclerView.VERTICAL, false);
        recycle_addressList.setLayoutManager(linearLayoutManager);
        txt_add = findViewById(R.id.txt_add);
        txt_title = findViewById(R.id.txt_title);
        txt_add.setVisibility(View.GONE);
        img_back = findViewById(R.id.img_back);
        li_error_msg = findViewById(R.id.li_error_msg);
        txt_add.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_title.setText("Notifications");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;

        }
    }

    private void showAllPushNotification() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.showAllPushNotification, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    arrayList.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        li_error_msg.setVisibility(View.GONE);
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            NotificationModel model = new NotificationModel();
                            model.setId(array_result.optJSONObject(i).optString("nid"));
                            model.setP_title(array_result.optJSONObject(i).optString("p_title"));
                            model.setP_desc(array_result.optJSONObject(i).optString("p_desc"));
                            model.setIs_read(array_result.optJSONObject(i).optString("is_read"));
                            model.setCreated_date(array_result.optJSONObject(i).optString("created_date"));
                            model.setOrder_id(array_result.optJSONObject(i).optString("ref_no"));
                            model.setPayment_mode(array_result.optJSONObject(i).optString("payment_mode"));
                            model.setOrder_status(array_result.optJSONObject(i).optString("order_status"));
                            arrayList.add(model);
                        }
                        if (arrayList.size() > 0) {
                            li_error_msg.setVisibility(View.GONE);
                            recycle_addressList.setVisibility(View.VISIBLE);
                            notificationAdpter = new NotificationAdpter(NotificationListActivity.this, arrayList);
                            recycle_addressList.setAdapter(notificationAdpter);
                            notificationAdpter.notifyDataSetChanged();
                        } else {
                            recycle_addressList.setVisibility(View.GONE);
                            li_error_msg.setVisibility(View.VISIBLE);
                        }

                        if (json_main.optString("totalnotification").equals("") || json_main.optString("totalnotification").equals("0") || json_main.optString("totalnotification").equals("null") || json_main.optString("totalnotification").equals(null)) {
                            BaseActivity.txt_noti_count.setVisibility(View.INVISIBLE);
                        } else {
                            BaseActivity.txt_noti_count.setVisibility(View.VISIBLE);
                            BaseActivity.txt_noti_count.setText(json_main.optString("totalnotification"));
                        }

                    } else {
                        recycle_addressList.setVisibility(View.GONE);
                        li_error_msg.setVisibility(View.VISIBLE);
                        //Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.showAllPushNotification + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.showAllPushNotification);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class NotificationAdpter extends RecyclerView.Adapter<NotificationAdpter.MyViewHolder> {
        private List<NotificationModel> dataList;
        private Context context;
        int pos = -1;

        public NotificationAdpter(Context context, List<NotificationModel> listClothingPrice) {
            this.context = context;
            this.dataList = listClothingPrice;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_adpter, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
            final NotificationModel model = dataList.get(position);

            if (pos == position) {
                holder.li_bg.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
            } else {
                holder.li_bg.setBackgroundColor(Color.parseColor("#e0e0e0"));
            }

            if (model.getIs_read().equals("unread")) {
                holder.li_bg.setBackgroundColor(Color.parseColor("#e0e0e0"));
            } else {
                holder.li_bg.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
            }

            if (model.getP_title().equals("") || model.getP_title().equals("null") || model.getP_title().equals(null)) {

            } else {
                holder.txt_storeName.setText(model.getP_title());
            }

            if (model.getP_desc().equals("") || model.getP_desc().equals("null") || model.getP_desc().equals(null)) {

            } else {
                holder.txt_desc.setText(model.getP_desc());
            }

            if (model.getCreated_date().equals("") || model.getCreated_date().equals("null") || model.getCreated_date().equals(null)) {

            } else {
                holder.txt_time.setText(model.getCreated_date());
            }

            holder.li_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = position;
                    order_id = model.getOrder_id();
                    AppConstant.order_id_deatil = model.getOrder_id();
                    Payment_mode = model.getPayment_mode();
                    Order_status = model.getOrder_status();
                    if (Utils.isNetworkAvailable(NotificationListActivity.this)) {
                        updateNotifiationPush(model.getId());
                    } else {
                        Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                    }
                    notifyDataSetChanged();
                }
            });

        }

        @Override
        public int getItemCount() {
            return dataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private LinearLayout li_bg;
            private TextView txt_storeName, txt_desc, txt_time;
            private CardView card_main1;


            public MyViewHolder(View view) {
                super(view);
                li_bg = view.findViewById(R.id.li_bg);
                txt_storeName = view.findViewById(R.id.txt_storeName);
                txt_desc = view.findViewById(R.id.txt_desc);
                txt_time = view.findViewById(R.id.txt_time);
                card_main1 = view.findViewById(R.id.card_main1);
            }
        }
    }

    private void updateNotifiationPush(String id) {
        String tag_string_req = "req";
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.updateNotifiationPush, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                try {
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        if (Utils.isNetworkAvailable(NotificationListActivity.this)) {
                            Intent i = new Intent(NotificationListActivity.this, OrderdetailActivity.class);
                            i.putExtra("order_id_full", order_id);
                            i.putExtra("payment_mode", Payment_mode);
                            i.putExtra("order_status", Order_status);
                            startActivity(i);

                        } else {
                            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nid", id);
                Log.e("params", "" + Constants.updateNotifiationPush + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.updateNotifiationPush);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onResume() {
        if (Utils.isNetworkAvailable(NotificationListActivity.this)) {
            showAllPushNotification();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
        super.onResume();
    }
}
