package com.app.superkart.ui.activity.Shopping;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.SellerForyouModel;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SellerCityStateWise extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private RecyclerView recycle_seller;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<SellerForyouModel> arrayList = new ArrayList<>();
    private SellerForYouAdpter sellerForYouAdpter;
    private EditText et_search;
    private ProgressDialog progressDialog;
    private String city = "", state = "", offer_title = "";
    private SharedPreferences pref_city;
    private TextView txt_title;
    private SwipeRefreshLayout swipeRefreshLayout;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.seller_for_you_list);
        Log.e("SellerCityStateWise", "SellerCityStateWise");
        pref_city = getSharedPreferences("city", Context.MODE_PRIVATE);
        init();
        intentData();
        if (Utils.isNetworkAvailable(SellerCityStateWise.this)) {
            nearbysellerall();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                if (Utils.isNetworkAvailable(SellerCityStateWise.this)) {
                    nearbysellerall();
                } else {
                    Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                }
            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    sellerForYouAdpter.getFilter().filter(s);
                } else {
                    sellerForYouAdpter.resetData();
                    sellerForYouAdpter.notifyDataSetChanged();
                    Utils.hideKeyboard(SellerCityStateWise.this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_seller = findViewById(R.id.recycle_seller);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        et_search = findViewById(R.id.et_search);
        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        img_back.setOnClickListener(this);
    }

    public void intentData() {
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            offer_title = (String) bd.get("offer_title");
            txt_title.setText(offer_title);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

    private void nearbysellerall() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.sellercitystatewise, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        JSONArray json_result = json_main.optJSONArray("result");
                        BindData(json_result);
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                /*params.put("city", "Ambala");
                params.put("state", "Haryana");*/
                params.put("city", pref_city.getString("city", ""));
                params.put("state", pref_city.getString("state", ""));
                Log.e("params", "" + Constants.sellercitystatewise + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.sellercitystatewise);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void BindData(JSONArray json_result) {
        arrayList.clear();
        for (int i = 0; i < json_result.length(); i++) {
            SellerForyouModel model = new SellerForyouModel();
            model.setSeller_id(json_result.optJSONObject(i).optString("seller_id"));
            model.setSeller_name(json_result.optJSONObject(i).optString("seller_name"));
            model.setSeller_image(json_result.optJSONObject(i).optString("seller_image"));
            model.setSeller_state(json_result.optJSONObject(i).optString("seller_state"));
            model.setSeller_city(json_result.optJSONObject(i).optString("seller_city"));
            model.setRating(json_result.optJSONObject(i).optInt("rating"));
            model.setTime(json_result.optJSONObject(i).optString("time"));
            arrayList.add(model);
        }
        if (arrayList.size() > 0) {
            linearLayoutManager = new LinearLayoutManager(SellerCityStateWise.this, LinearLayoutManager.VERTICAL, false);
            recycle_seller.setLayoutManager(linearLayoutManager);
            sellerForYouAdpter = new SellerForYouAdpter(SellerCityStateWise.this, arrayList);
            recycle_seller.setAdapter(sellerForYouAdpter);
        }
    }

    public class SellerForYouAdpter extends RecyclerView.Adapter<SellerForYouAdpter.MyViewHolder> {

        private ArrayList<SellerForyouModel> arrayList;
        List<SellerForyouModel> local_array = null;
        private Context context;
        private int pos = 0;
        private ItemFilter mFilter = new ItemFilter();

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item;
            public TextView txt_address, txt_storeName, txt_km, txt_rating, txt_time;
            LinearLayout li_bg;
            private RatingBar rating;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_storeName = view.findViewById(R.id.txt_storeName);
                txt_address = view.findViewById(R.id.txt_address);
                txt_km = view.findViewById(R.id.txt_km);
                txt_rating = view.findViewById(R.id.txt_rating);
                txt_time = view.findViewById(R.id.txt_time);
                rating = view.findViewById(R.id.rating);
                li_bg = view.findViewById(R.id.li_bg);

            }
        }

        public SellerForYouAdpter(Context context, ArrayList<SellerForyouModel> list) {
            this.context = context;
            this.arrayList = list;
            this.local_array = list;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adpter_seller_list, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            SellerForyouModel model = local_array.get(position);

            if (local_array.get(position).getSeller_image().equals("")) {
            } else {
              /*  Picasso.with(context)
                        .load(local_array.get(position).getSeller_image())
                        .into(holder.img_item);*/

                Glide
                        .with(context)
                        .load(local_array.get(position).getSeller_image())
                        .centerCrop()
                        .into(holder.img_item);
            }

            if (local_array.get(position).getSeller_name().equals("")) {
            } else {
                holder.txt_storeName.setText(local_array.get(position).getSeller_name());
            }
            if (local_array.get(position).getSeller_city().equals("") && local_array.get(position).getSeller_state().equals("")) {
            } else {
                holder.txt_address.setText(local_array.get(position).getSeller_city() + "," + local_array.get(position).getSeller_state());
            }

            // holder.txt_km.setText(model.getKm());

            if (pos == position) {
                holder.li_bg.setBackgroundResource(R.drawable.bg_gray);
            } else {
                holder.li_bg.setBackgroundResource(R.drawable.bg_white);
            }
            holder.li_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = position;
                    sellerForYouAdpter.notifyDataSetChanged();
                    Intent i = new Intent(context, SellerForyouDetailActivity.class);
                    i.putExtra("seller_id", model.getSeller_id());
                    i.putExtra("image", model.getSeller_image());
                    context.startActivity(i);
                }
            });

            if (local_array.get(position).getTime().equals("")) {
            } else {
                holder.txt_time.setText(local_array.get(position).getTime());
            }
            holder.txt_rating.setText(local_array.get(position).getRating() + ".0");

            holder.rating.setRating(local_array.get(position).getRating());
        }

        @Override
        public int getItemCount() {
            return local_array.size();
        }

        public void resetData() {
            local_array = arrayList;
        }

        public Filter getFilter() {
            return mFilter;
        }

        private class ItemFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();

                final List<SellerForyouModel> list = arrayList;

                int count = list.size();
                final ArrayList<SellerForyouModel> nlist = new ArrayList<>(count);

                if (constraint.length() == 0) {
                    nlist.addAll(arrayList);
                } else {
                    final String filterPattern = constraint.toString().toLowerCase().trim();
                    for (final SellerForyouModel mWords : arrayList) {
                        if ((mWords.getSeller_name().toLowerCase().contains(filterPattern))) {
                            nlist.add(mWords);
                        }
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                local_array = (List<SellerForyouModel>) results.values;
                sellerForYouAdpter.notifyDataSetChanged();
            }

        }

    }

}

