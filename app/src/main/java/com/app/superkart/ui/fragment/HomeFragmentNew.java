package com.app.superkart.ui.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.adpters.Food.FoodCouponAdpter;
import com.app.superkart.adpters.Food.Food_Home_Category_Adpter;
import com.app.superkart.adpters.Food.Food_Home_SubCategory_Adpter;
import com.app.superkart.adpters.Food.Food_seller_Adpter;
import com.app.superkart.adpters.Food.HomeFoodBannerAdpter;
import com.app.superkart.adpters.Food.Home_Promoted_Adpter;
import com.app.superkart.adpters.Food.NearByRastaurantAdpter;
import com.app.superkart.adpters.Service.HomeServiceBannerAdpter;
import com.app.superkart.adpters.Service.HomeService_Offer_BannerAdpter;
import com.app.superkart.adpters.Service.Home_Service_Adpter;
import com.app.superkart.adpters.Shopping.HomeBannerAdpter;
import com.app.superkart.adpters.Shopping.Home_Coupon_Adpter;
import com.app.superkart.adpters.Shopping.Home_TopBrand_Adpter;
import com.app.superkart.adpters.Shopping.Home_sellerforU_Adpter;
import com.app.superkart.model.Shopping.AllCouponModel;
import com.app.superkart.model.Shopping.BrandModel;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.model.Shopping.OfferModel;
import com.app.superkart.model.Shopping.SellerForyouModel;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.Food.NearByRestaurantActivity;
import com.app.superkart.ui.activity.Shopping.AllSubProductActivity;
import com.app.superkart.ui.activity.Shopping.CityWiseCouponActivity;
import com.app.superkart.ui.activity.Shopping.GlobalSearchActivity;
import com.app.superkart.ui.activity.Shopping.HomeAll_CategoryActivity;
import com.app.superkart.ui.activity.Shopping.Home_ViewAll_Category_List;
import com.app.superkart.ui.activity.Shopping.Home_View_Fashion_Detail;
import com.app.superkart.ui.activity.Shopping.SellerCityStateWise;
import com.app.superkart.ui.activity.Shopping.SellerForyouActivity;
import com.app.superkart.ui.activity.Shopping.ViewAllProdductActvity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.GPSTracker;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class HomeFragmentNew extends Fragment implements View.OnClickListener, LocationListener {


    private ProgressDialog progressDialog;

    // Shopping segment

    private RelativeLayout rl_main;
    private NestedScrollView li_main_scroll;
    private LinearLayout li_main_shopping, li_main_food;
    private LinearLayout li_btn_shopping, li_btn_food, li_btn_service;
    private ImageView img_shopping, img_food, img_service;
    private TextView txt_shopping, txt_food, txt_service, txt_view_category, txt_seller, txt_seller_viewAll;
    public static TextView txt_location, txt_title;
    private RecyclerView recycle_category, recycle_sellerforyou, recycle_topBrand, recycle_categoryDate;
    private LinearLayoutManager Cate_LayoutManager, topBrand_LayoutManager, CateData_LayoutManager, BestCoupon_LayoutManager;
    private GridLayoutManager gridLayoutManager_bestBuy, seller_LayoutManager;
    private HomeCategoryAdpter homeCategoryAdpter;
    private ArrayList<OfferModel> arrayList_bestBuy = new ArrayList<>();
    private ArrayList<OfferModel> arrayList_seller_offer = new ArrayList<>();
    private BestBuys_Adpter bestBuys_adpter;

    private Home_sellerforU_Adpter home_sellerforU_adpter;
    private Home_TopBrand_Adpter home_topBrand_adpter;
    private ArrayList<HomeCategoryModel> arrayList_homecategory = new ArrayList<>();
    private ViewPager mViewPager;
    public TabLayout tabLayout;
    private String latitude, longitude, address, city, state, country, postalCode;
    private List<Address> addresses = new ArrayList<>();
    private ArrayList<HomeCategoryModel> listimage = new ArrayList<>();
    private ArrayList<OfferModel> arrayList_cateData = new ArrayList<>();
    private ArrayList<OfferModel> arrayList_Recoupon = new ArrayList<>();
    private ArrayList<String> listimage_CateBanner = new ArrayList<>();
    private CategoryDataAdpter categoryDataAdpter;
    private Home_Coupon_Adpter home_coupon_adpter;

    private ArrayList<SellerForyouModel> arrayList_seller = new ArrayList<>();
    private ArrayList<BrandModel> arrayList_topBrand = new ArrayList<>();
    private HomeBannerAdpter homeBannerAdpter;

    private RelativeLayout rl_seller_near_you, rl_topbrand, rl_loyaltyPoints;
    private CardView card_main1;

    // Food Segment

    private RecyclerView recycle_Food_category, recycle_Food_subcategory, recycle_Food_seller, recycle_Food_coupon, recycle_promoted_restaurant, recycle_nearby_restaurant;
    private LinearLayoutManager Food_seller_LayoutManager, Food_Cate_LayoutManager, Food_Sub_Cate_LayoutManager, Food_coupon_LayoutManager, promoted_restaurant_LayoutManager, nearby_restaurant_LayoutManager;
    //   private GridLayoutManager nearby_restaurant_GridManager;
    private Food_Home_Category_Adpter food_home_category_adpter;
    private Food_Home_SubCategory_Adpter food_home_subCategory_adpter;
    private Food_seller_Adpter food_seller_adpter;
    private ArrayList<HomeCategoryModel> arrayList_food_homeCate = new ArrayList<>();
    private ArrayList<HomeCategoryModel> arrayList_food_homeSubCate = new ArrayList<>();
    private ArrayList<SellerForyouModel> arrayList_food_homeSeller = new ArrayList<>();
    private ViewPager mViewPager_food;
    private TabLayout tabLayout_food;
    private ArrayList<HomeCategoryModel> listimage_foodBanner = new ArrayList<>();
    private ArrayList<AllCouponModel> arrayList_couponFood = new ArrayList<>();
    private ArrayList<AllCouponModel> arrayList_PromotedRest = new ArrayList<>();
    private ArrayList<String> arrayList_NearBydRest = new ArrayList<>();
    private HomeFoodBannerAdpter homeFoodBannerAdpter;
    private FoodCouponAdpter foodCouponAdpter;
    private Home_Promoted_Adpter home_promoted_adpter;
    private NearByRastaurantAdpter nearByRastaurantAdpter;
    private TextView txt_nearbyViewAll, txt_promotedViewAll;

    //Service

    private LinearLayout li_main_service;
    private ViewPager mViewPager_service, mViewPager_service_offer;
    private TabLayout tabLayout_service, tabLayout_service_offer;
    private HomeServiceBannerAdpter homeServiceBannerAdpter;
    private HomeService_Offer_BannerAdpter homeService_offer_bannerAdpter;
    private RecyclerView recycle_service;
    private GridLayoutManager gridLayoutManager;
    private ArrayList<HomeCategoryModel> arrayList_service = new ArrayList<>();
    private Home_Service_Adpter home_service_adpter;

    final String TAG = "GPS";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    LocationManager locationManager;
    Location loc;
    ArrayList<String> permissions = new ArrayList<>();
    ArrayList<String> permissionsToRequest;
    ArrayList<String> permissionsRejected = new ArrayList<>();
    boolean isGPS = false;
    boolean isNetwork = false;
    boolean canGetLocation = true;
    private static final int MY_CAMERA_REQUEST_CODE = 100;

    View view;
    SharedPreferences preferences;
    SharedPreferences pref_city;
    private ProgressBar progress_bar;
    private SwipeRefreshLayout swipeRefreshLayout;

    public HomeFragmentNew() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home_new, container, false);
        Log.e("HomeFragment", "HomeFragment");
        init(view);
        ClickListener();
        bindFoodBanner();
        bind_food_coupon();
        bind_Promoted_Restaurant();
        bind_NearBy_Restaurant();
        bind_food_category();
        bind_food_Subcategory();
        bindServiceBanner(view);
        bindServiceData(view);
        bindServicOffereBanner(view);
        locationManager = (LocationManager) getActivity().getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        preferences = getActivity().getSharedPreferences(getActivity().getString(R.string.app_name), Context.MODE_PRIVATE);
        pref_city = getActivity().getSharedPreferences("city", Context.MODE_PRIVATE);
        txt_location.setText(pref_city.getString("city", ""));
        Log.e("user_id", "" + preferences.getString(PreferenceManager.user_id, ""));
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        //permissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            showSettingsAlert();
            getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            // check permissions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    Log.d(TAG, "Permission requests");
                    canGetLocation = false;
                }
            }

            // get location
            getLocation();
        }
    
       /* mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            getLocation();
            GPSTracker();
        }
*/



        AppConstant.click_main_module = "Shoppping";
        li_main_scroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
              /*  if (AppConstant.click_main_module.equals("Food")) {
                    return true;
                } else*/
                if (AppConstant.click_main_module.equals("Service")) {
                    return true;
                }
                return false;
            }
        });


        BaseActivity.li_serach.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent i = new Intent(getActivity(), GlobalSearchActivity.class);
                startActivity(i);
                return false;
            }
        });

        if (Utils.isNetworkAvailable(getActivity())) {
            HomeApi();
        } else {
            Utils.showSnackBar(view, getResources().getString(R.string.check_internet));
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                if (Utils.isNetworkAvailable(getActivity())) {
                    HomeApi();
                } else {
                    Utils.showSnackBar(view, getResources().getString(R.string.check_internet));
                }
            }
        });


        if (preferences.getString(PreferenceManager.totalnotification, "").equals("") || preferences.getString(PreferenceManager.totalnotification, "").equals("null") ||
                preferences.getString(PreferenceManager.totalnotification, "").equals(null) || preferences.getString(PreferenceManager.totalnotification, "") == null ||
                preferences.getString(PreferenceManager.totalnotification, "").equals("0")) {
            BaseActivity.txt_noti_count.setVisibility(View.INVISIBLE);
        } else {
            BaseActivity.txt_noti_count.setVisibility(View.VISIBLE);
            BaseActivity.txt_noti_count.setText(preferences.getString(PreferenceManager.totalnotification, ""));
        }

        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
            BaseActivity.slide_login.setVisibility(View.VISIBLE);
            BaseActivity.slide_logout.setVisibility(View.GONE);
        } else {
            BaseActivity.slide_login.setVisibility(View.GONE);
            BaseActivity.slide_logout.setVisibility(View.VISIBLE);
        }

        if (preferences.getString(PreferenceManager.profile_pic, "").equals("") || preferences.getString(PreferenceManager.profile_pic, "").equals("null") || preferences.getString(PreferenceManager.profile_pic, "").equals(null)) {

        } else {
            Glide
                    .with(getActivity())
                    .load(preferences.getString(PreferenceManager.profile_pic, ""))
                    .centerCrop()
                    .into(BaseActivity.profile_image);
        }


        return view;

    }

    public void init(View view) {

        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        recycle_categoryDate = view.findViewById(R.id.recycle_categoryDate);
        progress_bar = view.findViewById(R.id.progress_bar);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);

        // Shopping segment

        li_main_shopping = view.findViewById(R.id.li_main_shopping);
        li_main_shopping.setVisibility(View.VISIBLE);

        mViewPager = view.findViewById(R.id.mViewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        recycle_sellerforyou = view.findViewById(R.id.recycle_sellerforyou);
        rl_seller_near_you = view.findViewById(R.id.rl_seller_near_you);
        rl_topbrand = view.findViewById(R.id.rl_topbrand);
        recycle_topBrand = view.findViewById(R.id.recycle_topBrand);
        rl_loyaltyPoints = view.findViewById(R.id.rl_loyaltyPoints);
        card_main1 = view.findViewById(R.id.card_main1);
        txt_seller_viewAll = view.findViewById(R.id.txt_seller_viewAll);

        txt_view_category = view.findViewById(R.id.txt_view_category);
        recycle_category = view.findViewById(R.id.recycle_category);
        txt_seller = view.findViewById(R.id.txt_seller);
        txt_location = view.findViewById(R.id.txt_location);
        txt_title = view.findViewById(R.id.txt_title);

        li_btn_shopping = view.findViewById(R.id.li_btn_shopping);
        li_btn_food = view.findViewById(R.id.li_btn_food);
        li_btn_service = view.findViewById(R.id.li_btn_service);
        li_main_scroll = view.findViewById(R.id.li_main_scroll);
        rl_main = view.findViewById(R.id.rl_main);

        img_shopping = view.findViewById(R.id.img_shopping);
        img_food = view.findViewById(R.id.img_food);
        img_service = view.findViewById(R.id.img_service);

        txt_shopping = view.findViewById(R.id.txt_shopping);
        txt_food = view.findViewById(R.id.txt_food);
        txt_service = view.findViewById(R.id.txt_service);

        li_btn_shopping.setBackgroundResource(R.drawable.bg_blue);
        img_shopping.setBackgroundResource(R.drawable.shopping_white);
        txt_shopping.setTextColor(Color.parseColor("#FFFFFFFF"));

        //Food

        txt_nearbyViewAll = view.findViewById(R.id.txt_nearbyViewAll);
        txt_promotedViewAll = view.findViewById(R.id.txt_promotedViewAll);


        li_main_food = view.findViewById(R.id.li_main_food);
        li_main_food.setVisibility(View.GONE);

        mViewPager_food = view.findViewById(R.id.mViewPager_food);
        tabLayout_food = view.findViewById(R.id.tabLayout_food);

        recycle_Food_seller = view.findViewById(R.id.recycle_Food_seller);
        Food_seller_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recycle_Food_category = view.findViewById(R.id.recycle_Food_category);
        Food_Cate_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recycle_Food_subcategory = view.findViewById(R.id.recycle_Food_subcategory);
        Food_Sub_Cate_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        recycle_Food_coupon = view.findViewById(R.id.recycle_Food_coupon);
        Food_coupon_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recycle_promoted_restaurant = view.findViewById(R.id.recycle_promoted_restaurant);
        promoted_restaurant_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        recycle_nearby_restaurant = view.findViewById(R.id.recycle_nearby_restaurant);
        nearby_restaurant_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        // nearby_restaurant_GridManager = new GridLayoutManager(getActivity(),2);

        // Service

        li_main_service = view.findViewById(R.id.li_main_service);
        li_main_service.setVisibility(View.GONE);
        recycle_service = view.findViewById(R.id.recycle_service);

    }

    public void ClickListener() {

        // Shopping segment

        li_btn_shopping.setOnClickListener(this);
        li_btn_service.setOnClickListener(this);
        txt_view_category.setOnClickListener(this);
        txt_seller.setOnClickListener(this);
        txt_seller_viewAll.setOnClickListener(this);

        // Food Segment

        li_btn_food.setOnClickListener(this);
        txt_title.setOnClickListener(this);
        txt_nearbyViewAll.setOnClickListener(this);
        txt_promotedViewAll.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.li_btn_shopping:
                AppConstant.click_main_module = "Shoppping";
                li_btn_shopping.setBackgroundResource(R.drawable.bg_blue);
                img_shopping.setBackgroundResource(R.drawable.shopping_white);
                txt_shopping.setTextColor(Color.parseColor("#FFFFFFFF"));
                txt_seller.setTextColor(Color.parseColor("#1576CE"));
                txt_seller.setEnabled(true);
                li_btn_food.setBackgroundResource(R.drawable.bg_white);
                img_food.setBackgroundResource(R.drawable.soup_blue);
                txt_food.setTextColor(Color.parseColor("#1576CE"));

                li_btn_service.setBackgroundResource(R.drawable.bg_white);
                img_service.setBackgroundResource(R.drawable.constructor_blue);
                txt_service.setTextColor(Color.parseColor("#1576CE"));
                li_main_shopping.setVisibility(View.VISIBLE);
                li_main_food.setVisibility(View.GONE);
                li_main_service.setVisibility(View.GONE);
                txt_title.setVisibility(View.GONE);
                break;

            case R.id.li_btn_food:
                AppConstant.click_main_module = "Food";
                li_btn_shopping.setBackgroundResource(R.drawable.bg_white);
                img_shopping.setBackgroundResource(R.drawable.shopping_blue);
                txt_shopping.setTextColor(Color.parseColor("#1576CE"));

                li_btn_food.setBackgroundResource(R.drawable.bg_yellow);
                img_food.setBackgroundResource(R.drawable.soup_white);
                txt_food.setTextColor(Color.parseColor("#FFFFFFFF"));
                txt_seller.setTextColor(Color.parseColor("#F39603"));
                txt_seller.setEnabled(false);

                li_btn_service.setBackgroundResource(R.drawable.bg_white);
                img_service.setBackgroundResource(R.drawable.constructor_blue);
                txt_service.setTextColor(Color.parseColor("#1576CE"));
                li_main_shopping.setVisibility(View.GONE);
                li_main_food.setVisibility(View.VISIBLE);
                li_main_service.setVisibility(View.GONE);
                txt_title.setVisibility(View.GONE);
                txt_title.setText("Food\nComing Soon");
                break;
            case R.id.li_btn_service:
                AppConstant.click_main_module = "Service";
                li_btn_shopping.setBackgroundResource(R.drawable.bg_white);
                img_shopping.setBackgroundResource(R.drawable.shopping_blue);
                txt_shopping.setTextColor(Color.parseColor("#1576CE"));

                li_btn_food.setBackgroundResource(R.drawable.bg_white);
                img_food.setBackgroundResource(R.drawable.soup_blue);
                txt_food.setTextColor(Color.parseColor("#1576CE"));

                li_btn_service.setBackgroundResource(R.drawable.bg_blue);
                img_service.setBackgroundResource(R.drawable.constructor_white);
                txt_service.setTextColor(Color.parseColor("#FFFFFFFF"));
                txt_seller.setTextColor(Color.parseColor("#1576CE"));
                txt_seller.setEnabled(false);
                li_main_shopping.setVisibility(View.GONE);
                li_main_food.setVisibility(View.GONE);
                li_main_service.setVisibility(View.VISIBLE);
                txt_title.setVisibility(View.VISIBLE);
                txt_title.setText("Services\nComing Soon");
                break;
            case R.id.txt_view_category:
                Intent i = new Intent(getActivity(), HomeAll_CategoryActivity.class);
                startActivity(i);
                break;
            case R.id.txt_seller:
                Intent intent = new Intent(getActivity(), SellerForyouActivity.class);
                startActivity(intent);
                break;
            case R.id.txt_title:
                break;
            case R.id.txt_seller_viewAll:
                Intent intent1 = new Intent(getActivity(), SellerForyouActivity.class);
                startActivity(intent1);
                break;
            case R.id.txt_nearbyViewAll:
                AppConstant.check_restaurant = "nearby";
                Intent NearbyRest = new Intent(getActivity(), NearByRestaurantActivity.class);
                startActivity(NearbyRest);
                break;

            case R.id.txt_promotedViewAll:
                AppConstant.check_restaurant = "promoted";
                Intent promotedRest = new Intent(getActivity(), NearByRestaurantActivity.class);
                startActivity(promotedRest);
                break;
        }
    }

    private void HomeApi() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList_homecategory.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.HomeApi, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                //  Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Data Found")) {
                        if (json_main.optString("totalnotification").equals("") || json_main.optString("totalnotification").equals("0") || json_main.optString("totalnotification").equals("null") || json_main.optString("totalnotification").equals(null)) {
                            BaseActivity.txt_noti_count.setVisibility(View.INVISIBLE);
                        } else {
                            BaseActivity.txt_noti_count.setVisibility(View.VISIBLE);
                            BaseActivity.txt_noti_count.setText(json_main.optString("totalnotification"));
                        }

                        JSONObject json_result = json_main.optJSONObject("result");
                        if (json_result.has("cat_arr")) {
                            JSONArray array_category = json_result.optJSONArray("cat_arr");
                            bindCategoryData(array_category);
                        }
                        if (json_result.has("sellers_nearby")) {
                            JSONArray array_sellers_nearby = json_result.optJSONArray("sellers_nearby");
                            bindSeller4u4Data(array_sellers_nearby);
                            bind_food_seller(array_sellers_nearby);
                        }
                        if (json_result.has("brands")) {
                            JSONArray array_brand = json_result.optJSONArray("brands");
                            bindTopBrandData(array_brand);
                        }
                    } else {
                        Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
             /*   params.put("city", "Churu");
                params.put("state", "Haryana");*/
                params.put("city", pref_city.getString("city", ""));
                params.put("state", pref_city.getString("state", ""));
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.HomeApi + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.HomeApi);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void bindCategoryData(JSONArray array_category) {
        arrayList_homecategory.clear();
        for (int i = 0; i < array_category.length(); i++) {
            HomeCategoryModel homeCategoryModel = new HomeCategoryModel();
            homeCategoryModel.setCategory_id(array_category.optJSONObject(i).optString("category_id"));
            homeCategoryModel.setItem_name(array_category.optJSONObject(i).optString("category_name"));
            homeCategoryModel.setCate_image(array_category.optJSONObject(i).optString("category_icon"));
            homeCategoryModel.setColor_code(array_category.optJSONObject(i).optString("color_code"));
            if (array_category.optJSONObject(i).has("category_slider")) {
                homeCategoryModel.setCategory_slider(array_category.optJSONObject(i).optString("category_slider"));
            }
            if (array_category.optJSONObject(i).has("offer_arr")) {
                homeCategoryModel.setOffer_arr(array_category.optJSONObject(i).optString("offer_arr"));
            }
            arrayList_homecategory.add(homeCategoryModel);
        }
        if (arrayList_homecategory.size() > 0) {
            Cate_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recycle_category.setLayoutManager(Cate_LayoutManager);
            homeCategoryAdpter = new HomeCategoryAdpter(getActivity(), arrayList_homecategory);
            recycle_category.setAdapter(homeCategoryAdpter);
            homeCategoryAdpter.notifyDataSetChanged();
        }

    }

    public class HomeCategoryAdpter extends RecyclerView.Adapter<HomeCategoryAdpter.MyViewHolder> {

        private List<HomeCategoryModel> arrayList;
        private Context context;
        int pos = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_item;
            private TextView txt_item;
            private LinearLayout li_bg;
            private View line;
            private CardView card_main1;
            private ViewPager mViewPager;
            private TabLayout tabLayout;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_item = view.findViewById(R.id.txt_item);
                li_bg = view.findViewById(R.id.li_bg);
                line = view.findViewById(R.id.line);
                card_main1 = view.findViewById(R.id.card_main1);
                mViewPager = view.findViewById(R.id.mViewPager);
                tabLayout = view.findViewById(R.id.tabLayout);
            }
        }

        public HomeCategoryAdpter(Context context, List<HomeCategoryModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpter_home_category, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            HomeCategoryModel model = arrayList.get(position);

      /*  Random r = new Random();
        int red = r.nextInt(255 - 0 + 1) + 0;
        int green = r.nextInt(255 - 0 + 1) + 0;
        int blue = r.nextInt(255 - 0 + 1) + 0;

        GradientDrawable draw = new GradientDrawable();
        draw.setShape(GradientDrawable.RECTANGLE);
        draw.setColor(Color.rgb(red, green, blue));
        holder.li_bg.setBackground(draw);

        GradientDrawable draw1 = new GradientDrawable();
        draw1.setShape(GradientDrawable.LINE);
        draw1.setColor(Color.rgb(red, green, blue));
        holder.line.setBackgroundColor(Color.rgb(red, green, blue));*/


            if (model.getColor_code().equals("")) {
            } else {
                holder.li_bg.setBackgroundColor(Color.parseColor(model.getColor_code().trim()));
                holder.line.setBackgroundColor(Color.parseColor(model.getColor_code().trim()));
            }

            if (model.getItem_name().equals("")) {
            } else {
                holder.txt_item.setText(model.getItem_name().trim());
            }

            if (model.getCate_image().equals("")) {

            } else {
               /* Picasso.with(context)
                        .load(model.getCate_image())
                        .into(holder.img_item);*/

                Glide
                        .with(context)
                        .load(model.getCate_image())
                        .centerCrop()
                        .into(holder.img_item);
            }


            if (pos == position) {
                holder.line.setVisibility(View.VISIBLE);
                if (model.getCategory_slider().equals("")) {
                } else {
                    listimage_CateBanner.clear();
                    JSONArray array_slide = null;
                    try {
                        array_slide = new JSONArray(model.getCategory_slider());
                        for (int i = 0; i < array_slide.length(); i++) {
                            listimage_CateBanner.add(array_slide.optString(i));
                        }
                        if (listimage_CateBanner.size() > 0) {
                            mViewPager.setVisibility(View.VISIBLE);
                            tabLayout.setVisibility(View.VISIBLE);
                            homeBannerAdpter = new HomeBannerAdpter(getActivity(), listimage_CateBanner);
                            mViewPager.setAdapter(homeBannerAdpter);
                            homeBannerAdpter.notifyDataSetChanged();
                            tabLayout.setupWithViewPager(mViewPager, true);
                        } else {
                            mViewPager.setVisibility(View.GONE);
                            tabLayout.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                if (model.getOffer_arr().equalsIgnoreCase("offer_arr")) {
                } else {
                    arrayList_cateData.clear();
                    try {
                        JSONArray array_offer = new JSONArray(model.getOffer_arr());
                        for (int i = 0; i < array_offer.length(); i++) {
                            OfferModel offerModel = new OfferModel();
                            offerModel.setOffer_id(array_offer.optJSONObject(i).optString("offer_id"));
                            offerModel.setOffer_title(array_offer.optJSONObject(i).optString("offer_title"));
                            offerModel.setOffer_type(array_offer.optJSONObject(i).optString("offer_type"));
                            offerModel.setCat_id(array_offer.optJSONObject(i).optString("cat_id"));
                            offerModel.setSection_image(array_offer.optJSONObject(i).optString("section_image"));
                            if (array_offer.optJSONObject(i).has("offer_item")) {
                                offerModel.setOffer_item(array_offer.optJSONObject(i).optString("offer_item"));
                            }
                            arrayList_cateData.add(offerModel);
                        }
                        if (arrayList_cateData.size() > 0) {
                            recycle_categoryDate.setVisibility(View.VISIBLE);
                            CateData_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            recycle_categoryDate.setLayoutManager(CateData_LayoutManager);
                            categoryDataAdpter = new CategoryDataAdpter(getActivity(), arrayList_cateData);
                            recycle_categoryDate.setAdapter(categoryDataAdpter);
                            categoryDataAdpter.notifyDataSetChanged();
                        } else {
                            recycle_categoryDate.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            } else {
                holder.line.setVisibility(View.GONE);
            }


            holder.card_main1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = position;
                    try {
                        if (model.getCategory_slider().equals("")) {

                        } else {
                            listimage_CateBanner.clear();
                            JSONArray array_slide = new JSONArray(model.getCategory_slider());
                            for (int i = 0; i < array_slide.length(); i++) {
                                listimage_CateBanner.add(array_slide.optString(i));
                            }
                            if (listimage_CateBanner.size() > 0) {
                                mViewPager.setVisibility(View.VISIBLE);
                                tabLayout.setVisibility(View.VISIBLE);
                                homeBannerAdpter = new HomeBannerAdpter(getActivity(), listimage_CateBanner);
                                mViewPager.setAdapter(homeBannerAdpter);
                                tabLayout.setupWithViewPager(mViewPager, true);
                                homeBannerAdpter.notifyDataSetChanged();
                            } else {
                                mViewPager.setVisibility(View.GONE);
                                tabLayout.setVisibility(View.GONE);
                            }
                        }

                        if (model.getOffer_arr().equalsIgnoreCase("offer_arr")) {
                        } else {
                            arrayList_cateData.clear();
                            try {
                                JSONArray array_offer = new JSONArray(model.getOffer_arr());
                                for (int i = 0; i < array_offer.length(); i++) {
                                    OfferModel offerModel = new OfferModel();
                                    offerModel.setOffer_id(array_offer.optJSONObject(i).optString("offer_id"));
                                    offerModel.setOffer_title(array_offer.optJSONObject(i).optString("offer_title"));
                                    offerModel.setOffer_type(array_offer.optJSONObject(i).optString("offer_type"));
                                    offerModel.setCat_id(array_offer.optJSONObject(i).optString("cat_id"));
                                    offerModel.setSection_image(array_offer.optJSONObject(i).optString("section_image"));
                                    if (array_offer.optJSONObject(i).has("offer_item")) {
                                        offerModel.setOffer_item(array_offer.optJSONObject(i).optString("offer_item"));
                                    }
                                    arrayList_cateData.add(offerModel);
                                }
                                if (arrayList_cateData.size() > 0) {
                                    recycle_categoryDate.setVisibility(View.VISIBLE);
                                    CateData_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                    recycle_categoryDate.setLayoutManager(CateData_LayoutManager);
                                    categoryDataAdpter = new CategoryDataAdpter(getActivity(), arrayList_cateData);
                                    recycle_categoryDate.setAdapter(categoryDataAdpter);
                                    categoryDataAdpter.notifyDataSetChanged();
                                } else {
                                    recycle_categoryDate.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    notifyDataSetChanged();

                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

    public class CategoryDataAdpter extends RecyclerView.Adapter<CategoryDataAdpter.MyViewHolder> {

        private List<OfferModel> arrayList;
        private Context context;
        int pos = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView txt_bestBuy, txt_item, txt_coupon_btn;
            private RecyclerView recycle_Buys, recycle_Bestcoupon;
            private CardView card_view_bestBuy;
            private RelativeLayout rl_coupon_1;
            private LinearLayout li_bg_new;

            public MyViewHolder(View view) {
                super(view);
                txt_bestBuy = view.findViewById(R.id.txt_bestBuy);
                txt_item = view.findViewById(R.id.txt_item);
                recycle_Buys = view.findViewById(R.id.recycle_Buys);
                card_view_bestBuy = view.findViewById(R.id.card_view_bestBuy);
                recycle_Bestcoupon = view.findViewById(R.id.recycle_Bestcoupon);
                rl_coupon_1 = view.findViewById(R.id.rl_coupon_1);
                txt_coupon_btn = view.findViewById(R.id.txt_coupon_btn);
                li_bg_new = view.findViewById(R.id.li_bg_new);
                txt_item = view.findViewById(R.id.txt_item);
            }
        }

        public CategoryDataAdpter(Context context, List<OfferModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shpopping_cate_data_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            OfferModel model = arrayList.get(position);

           /* if (position % 2 == 0) {
                Log.e("if position", "" + position);
            } else {
                Log.e("else position", "" + position);
            }*/

            holder.card_view_bestBuy.setCardBackgroundColor(Color.parseColor("#F89800"));
            holder.txt_item.setTextColor(Color.parseColor("#FFFFFFFF"));
            holder.txt_item.setBackgroundResource(R.drawable.bg_blue_new);
            if (model.getOffer_type().equalsIgnoreCase("category")) {

                holder.txt_bestBuy.setText(model.getOffer_title());
                try {
                    arrayList_bestBuy.clear();
                    JSONArray jsonArray = new JSONArray(model.getOffer_item());
                    for (int j = 0; j < jsonArray.length(); j++) {
                        OfferModel offerModel = new OfferModel();
                        offerModel.setCat_id(jsonArray.optJSONObject(j).optString("cat_id"));
                        offerModel.setSub_cat_id(jsonArray.optJSONObject(j).optString("sub_cat_id"));
                        offerModel.setProduct_name(jsonArray.optJSONObject(j).optString("sub_category_name"));
                        offerModel.setProduct_image(jsonArray.optJSONObject(j).optString("sub_category_image"));
                        offerModel.setOffer_type(jsonArray.optJSONObject(j).optString("offer_type"));
                        offerModel.setOffer_title(jsonArray.optJSONObject(j).optString("offer_title"));
                        arrayList_bestBuy.add(offerModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (arrayList_bestBuy.size() > 0) {
                    holder.card_view_bestBuy.setVisibility(View.VISIBLE);
                    holder.rl_coupon_1.setVisibility(View.GONE);
                    gridLayoutManager_bestBuy = new GridLayoutManager(context, 2);
                    holder.recycle_Buys.setLayoutManager(gridLayoutManager_bestBuy);
                    bestBuys_adpter = new BestBuys_Adpter(context, arrayList_bestBuy);
                    holder.recycle_Buys.setAdapter(bestBuys_adpter);
                    // bestBuys_adpter.notifyDataSetChanged();
                } else {
                    holder.card_view_bestBuy.setVisibility(View.GONE);
                    holder.rl_coupon_1.setVisibility(View.GONE);
                }
            } else if (model.getOffer_type().equalsIgnoreCase("coupon")) {

                holder.card_view_bestBuy.setVisibility(View.GONE);
                try {
                    arrayList_Recoupon.clear();
                    JSONArray jsonArray = new JSONArray(model.getOffer_item());
                    for (int j = 0; j < jsonArray.length(); j++) {
                        OfferModel offerModel = new OfferModel();
                        offerModel.setCoupon_id(jsonArray.optJSONObject(j).optString("coupon_id"));
                        offerModel.setCoupon_image(jsonArray.optJSONObject(j).optString("coupon_image"));
                        offerModel.setCoupon_code(jsonArray.optJSONObject(j).optString("coupon_code"));
                        arrayList_Recoupon.add(offerModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (arrayList_Recoupon.size() > 0) {
                    holder.rl_coupon_1.setVisibility(View.VISIBLE);
                    holder.recycle_Bestcoupon.setVisibility(View.VISIBLE);
                    BestCoupon_LayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    holder.recycle_Bestcoupon.setLayoutManager(BestCoupon_LayoutManager);
                    home_coupon_adpter = new Home_Coupon_Adpter(getActivity(), arrayList_Recoupon);
                    holder.recycle_Bestcoupon.setAdapter(home_coupon_adpter);
                    //  home_coupon_adpter.notifyDataSetChanged();

                } else {
                    holder.rl_coupon_1.setVisibility(View.GONE);
                    holder.recycle_Bestcoupon.setVisibility(View.GONE);
                }
            } else if (model.getOffer_type().equalsIgnoreCase("product")) {
                holder.txt_bestBuy.setText(model.getOffer_title());
                try {
                    arrayList_bestBuy.clear();
                    JSONArray jsonArray = new JSONArray(model.getOffer_item());
                    for (int j = 0; j < jsonArray.length(); j++) {
                        OfferModel offerModel = new OfferModel();
                        offerModel.setProduct_id(jsonArray.optJSONObject(j).optString("product_id"));
                        offerModel.setProduct_name(jsonArray.optJSONObject(j).optString("product_name"));
                        offerModel.setProduct_image(jsonArray.optJSONObject(j).optString("product_image"));
                        offerModel.setStorename(jsonArray.optJSONObject(j).optString("storename"));
                        offerModel.setSelling_type(jsonArray.optJSONObject(j).optString("selling_type"));
                        offerModel.setOffer_type(jsonArray.optJSONObject(j).optString("offer_type"));
                        arrayList_bestBuy.add(offerModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (arrayList_bestBuy.size() > 0) {
                    holder.card_view_bestBuy.setVisibility(View.VISIBLE);
                    holder.rl_coupon_1.setVisibility(View.GONE);
                    gridLayoutManager_bestBuy = new GridLayoutManager(context, 2);
                    holder.recycle_Buys.setLayoutManager(gridLayoutManager_bestBuy);
                    bestBuys_adpter = new BestBuys_Adpter(context, arrayList_bestBuy);
                    holder.recycle_Buys.setAdapter(bestBuys_adpter);
                    //    bestBuys_adpter.notifyDataSetChanged();
                } else {
                    holder.card_view_bestBuy.setVisibility(View.GONE);
                    holder.rl_coupon_1.setVisibility(View.GONE);
                }
            } else if (model.getOffer_type().equalsIgnoreCase("seller")) {
                holder.txt_bestBuy.setText(model.getOffer_title());

                try {
                    arrayList_bestBuy.clear();
                    JSONArray jsonArray = new JSONArray(model.getOffer_item());
                    for (int j = 0; j < jsonArray.length(); j++) {
                        OfferModel offerModel = new OfferModel();
                        offerModel.setSeller_id(jsonArray.optJSONObject(j).optString("seller_id"));
                        offerModel.setProduct_name(jsonArray.optJSONObject(j).optString("seller_name"));
                        offerModel.setProduct_image(jsonArray.optJSONObject(j).optString("seller_image"));
                        offerModel.setOffer_type(jsonArray.optJSONObject(j).optString("offer_type"));
                        offerModel.setOffer_title(jsonArray.optJSONObject(j).optString("offer_title"));
                        arrayList_bestBuy.add(offerModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (arrayList_bestBuy.size() > 0) {
                    holder.card_view_bestBuy.setVisibility(View.VISIBLE);
                    holder.rl_coupon_1.setVisibility(View.GONE);
                    gridLayoutManager_bestBuy = new GridLayoutManager(context, 2);
                    holder.recycle_Buys.setLayoutManager(gridLayoutManager_bestBuy);
                    bestBuys_adpter = new BestBuys_Adpter(context, arrayList_bestBuy);
                    holder.recycle_Buys.setAdapter(bestBuys_adpter);
                    //   bestBuys_adpter.notifyDataSetChanged();
                } else {
                    holder.card_view_bestBuy.setVisibility(View.GONE);
                    holder.rl_coupon_1.setVisibility(View.GONE);
                }
            }

            holder.txt_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model.getOffer_type().equalsIgnoreCase("product")) {
                        Intent i = new Intent(context, ViewAllProdductActvity.class);
                        i.putExtra("offer_id", model.getOffer_id());
                        i.putExtra("offer_title", model.getOffer_title());
                        context.startActivity(i);
                    } else if (model.getOffer_type().equalsIgnoreCase("category")) {
                        Intent i = new Intent(getActivity(), AllSubProductActivity.class);
                        i.putExtra("offer_title", model.getOffer_title());
                        i.putExtra("category_id", model.getCat_id());
                        startActivity(i);
                    } else if (model.getOffer_type().equalsIgnoreCase("seller")) {
                        Intent i = new Intent(getActivity(), SellerCityStateWise.class);
                        i.putExtra("offer_title", model.getOffer_title());
                        startActivity(i);
                    }

                }
            });

            holder.txt_coupon_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), CityWiseCouponActivity.class);
                    startActivity(i);
                }
            });

        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

    public class BestBuys_Adpter extends RecyclerView.Adapter<BestBuys_Adpter.MyViewHolder> {

        private List<OfferModel> arrayList;
        private Context context;
        int pos = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item;
            public TextView txt_item;
            LinearLayout li_bg;


            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_item = view.findViewById(R.id.txt_item);
                li_bg = view.findViewById(R.id.li_bg);

            }
        }

        public BestBuys_Adpter(Context context, List<OfferModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.best_on_superkart_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            OfferModel model = arrayList.get(position);

            if (model.getProduct_image().equals("")) {
                holder.li_bg.setVisibility(View.GONE);
            } else {
                holder.li_bg.setVisibility(View.VISIBLE);
                Glide
                        .with(context)
                        .load(model.getProduct_image())
                        .centerCrop()
                        .into(holder.img_item);
            }
            if (model.getProduct_name().equals("")) {
                holder.txt_item.setText("");
            } else {
                holder.txt_item.setText(model.getProduct_name());
            }

            holder.li_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppConstant.check_flow = "fromHome";
                    if (model.getOffer_type().equals("product")) {
                        Intent i = new Intent(getActivity(), Home_View_Fashion_Detail.class);
                        AppConstant.product_id = model.getProduct_id();
                        i.putExtra("product_id", model.getProduct_id());
                        i.putExtra("storename", model.getStorename());
                        i.putExtra("selling_type", model.getSelling_type());
                        startActivity(i);
                    } else if (model.getOffer_type().equals("category")) {
                        Intent i = new Intent(getActivity(), AllSubProductActivity.class);
                        i.putExtra("category_id", model.getCat_id());
                        i.putExtra("offer_title", model.getOffer_title());
                        startActivity(i);
                    } else if (model.getOffer_type().equalsIgnoreCase("seller")) {
                        Intent i = new Intent(getActivity(), SellerCityStateWise.class);
                        i.putExtra("offer_title", model.getOffer_title());
                        startActivity(i);
                    }

                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

    public void bindSeller4u4Data(JSONArray array_sellers_nearby) {
        arrayList_seller.clear();
        for (int i = 0; i < array_sellers_nearby.length(); i++) {
            SellerForyouModel model = new SellerForyouModel();
            model.setSeller_id(array_sellers_nearby.optJSONObject(i).optString("seller_id"));
            model.setSeller_name(array_sellers_nearby.optJSONObject(i).optString("seller_name"));
            model.setSeller_image(array_sellers_nearby.optJSONObject(i).optString("seller_image"));
            model.setSeller_state(array_sellers_nearby.optJSONObject(i).optString("seller_state"));
            model.setSeller_city(array_sellers_nearby.optJSONObject(i).optString("seller_city"));
            arrayList_seller.add(model);
        }
        if (arrayList_seller.size() > 0) {
            rl_seller_near_you.setVisibility(View.VISIBLE);
            recycle_sellerforyou.setVisibility(View.VISIBLE);
            rl_topbrand.setVisibility(View.GONE);
            recycle_topBrand.setVisibility(View.GONE);
            rl_loyaltyPoints.setVisibility(View.VISIBLE);
            card_main1.setVisibility(View.VISIBLE);
            seller_LayoutManager = new GridLayoutManager(getActivity(), 2);
            recycle_sellerforyou.setLayoutManager(seller_LayoutManager);
            home_sellerforU_adpter = new Home_sellerforU_Adpter(getActivity(), arrayList_seller);
            recycle_sellerforyou.setAdapter(home_sellerforU_adpter);
            home_sellerforU_adpter.notifyDataSetChanged();
           /* recycle_sellerforyou.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });*/
        } else {
            rl_seller_near_you.setVisibility(View.GONE);
            recycle_sellerforyou.setVisibility(View.GONE);
        }

    }

    public void bindTopBrandData(JSONArray array_brand) {
        arrayList_topBrand.clear();
        for (int i = 0; i < array_brand.length(); i++) {
            BrandModel model = new BrandModel();
            model.setBrand_id(array_brand.optJSONObject(i).optString("brand_id"));
            model.setBrand_image(array_brand.optJSONObject(i).optString("brand_image"));
            model.setBrand_name(array_brand.optJSONObject(i).optString("brand_name"));
            arrayList_topBrand.add(model);
        }

        if (arrayList_topBrand.size() > 0) {
            rl_topbrand.setVisibility(View.VISIBLE);
            recycle_topBrand.setVisibility(View.VISIBLE);
            topBrand_LayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recycle_topBrand.setLayoutManager(topBrand_LayoutManager);
            home_topBrand_adpter = new Home_TopBrand_Adpter(getActivity(), arrayList_topBrand);
            recycle_topBrand.setAdapter(home_topBrand_adpter);
            recycle_topBrand.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });

        } else {
            rl_topbrand.setVisibility(View.GONE);
            recycle_topBrand.setVisibility(View.GONE);
        }

    }


    // Food

    public void bind_food_seller(JSONArray array_sellers_nearby) {
        arrayList_food_homeSeller.clear();
        for (int i = 0; i < array_sellers_nearby.length(); i++) {
            SellerForyouModel model = new SellerForyouModel();
            model.setSeller_id(array_sellers_nearby.optJSONObject(i).optString("seller_id"));
            model.setSeller_name(array_sellers_nearby.optJSONObject(i).optString("seller_name"));
            model.setSeller_image(array_sellers_nearby.optJSONObject(i).optString("seller_image"));
            model.setSeller_state(array_sellers_nearby.optJSONObject(i).optString("seller_state"));
            model.setSeller_city(array_sellers_nearby.optJSONObject(i).optString("seller_city"));
            arrayList_food_homeSeller.add(model);
        }
        if (arrayList_food_homeSeller.size() > 0) {
            recycle_Food_seller.setVisibility(View.VISIBLE);
            recycle_Food_seller.setLayoutManager(Food_seller_LayoutManager);
            food_seller_adpter = new Food_seller_Adpter(getActivity(), arrayList_food_homeSeller);
            recycle_Food_seller.setAdapter(food_seller_adpter);
        } else {
            recycle_Food_seller.setVisibility(View.GONE);
        }

    }

    public void bindFoodBanner() {
        listimage_foodBanner.clear();
        HomeCategoryModel homeCategoryModel;

        homeCategoryModel = new HomeCategoryModel(R.drawable.veg);
        listimage_foodBanner.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.meal);
        listimage_foodBanner.add(homeCategoryModel);

        if (listimage_foodBanner.size() > 0) {
            homeFoodBannerAdpter = new HomeFoodBannerAdpter(getActivity(), listimage_foodBanner);
            mViewPager_food.setAdapter(homeFoodBannerAdpter);
            tabLayout_food.setupWithViewPager(mViewPager_food, false);
        }

    }

    public void bind_food_category() {

        arrayList_food_homeCate.clear();
        HomeCategoryModel homeCategoryModel;

        homeCategoryModel = new HomeCategoryModel(R.drawable.pizza, "Pizza");
        arrayList_food_homeCate.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.burger, "Burger");
        arrayList_food_homeCate.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.noodle, "Noodles");
        arrayList_food_homeCate.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.coffee, "Coffee");
        arrayList_food_homeCate.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.lunch, "Lunch");
        arrayList_food_homeCate.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.pizza_banner, "Pizza");
        arrayList_food_homeCate.add(homeCategoryModel);

        recycle_Food_category.setLayoutManager(Food_Cate_LayoutManager);
        food_home_category_adpter = new Food_Home_Category_Adpter(getActivity(), arrayList_food_homeCate);
        recycle_Food_category.setAdapter(food_home_category_adpter);
    }

    public void bind_food_Subcategory() {

        arrayList_food_homeSubCate.clear();
        HomeCategoryModel homeCategoryModel;

        homeCategoryModel = new HomeCategoryModel(R.drawable.pizza, "Domino's");
        arrayList_food_homeSubCate.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.pizza_banner, "Pizza Hut");
        arrayList_food_homeSubCate.add(homeCategoryModel);

        recycle_Food_subcategory.setLayoutManager(Food_Sub_Cate_LayoutManager);
        food_home_subCategory_adpter = new Food_Home_SubCategory_Adpter(getActivity(), arrayList_food_homeSubCate);
        recycle_Food_subcategory.setAdapter(food_home_subCategory_adpter);
    }

    public void bind_food_coupon() {

        arrayList_couponFood.clear();
        AllCouponModel model;

        model = new AllCouponModel(R.drawable.party, "");
        arrayList_couponFood.add(model);

        model = new AllCouponModel(R.drawable.party, "");
        arrayList_couponFood.add(model);

        model = new AllCouponModel(R.drawable.party, "");
        arrayList_couponFood.add(model);

        model = new AllCouponModel(R.drawable.party, "");
        arrayList_couponFood.add(model);

        recycle_Food_coupon.setLayoutManager(Food_coupon_LayoutManager);
        foodCouponAdpter = new FoodCouponAdpter(getActivity(), arrayList_couponFood);
        recycle_Food_coupon.setAdapter(foodCouponAdpter);
    }

    public void bind_Promoted_Restaurant() {

        arrayList_PromotedRest.clear();
        AllCouponModel model;

        model = new AllCouponModel(R.drawable.temp_food, "Pyramid");
        arrayList_PromotedRest.add(model);

        model = new AllCouponModel(R.drawable.pizza, "Grand Pizza");
        arrayList_PromotedRest.add(model);

        model = new AllCouponModel(R.drawable.meal, "The Food Court");
        arrayList_PromotedRest.add(model);

        model = new AllCouponModel(R.drawable.veg, "Yummy Food");
        arrayList_PromotedRest.add(model);

        recycle_promoted_restaurant.setLayoutManager(promoted_restaurant_LayoutManager);
        home_promoted_adpter = new Home_Promoted_Adpter(getActivity(), arrayList_PromotedRest);
        recycle_promoted_restaurant.setAdapter(home_promoted_adpter);
    }

    public void bind_NearBy_Restaurant() {

        arrayList_NearBydRest.clear();
        /*AllCouponModel model;

        model = new AllCouponModel(R.drawable.meal, "The Food Court");
        arrayList_NearBydRest.add(model);

        model = new AllCouponModel(R.drawable.veg, "Yummy Food");
        arrayList_NearBydRest.add(model);

        model = new AllCouponModel(R.drawable.temp_food, "Piramid");
        arrayList_NearBydRest.add(model);

        model = new AllCouponModel(R.drawable.pizza, "Grand Pizza");
        arrayList_NearBydRest.add(model);

        model = new AllCouponModel(R.drawable.meal, "The Food Court");
        arrayList_NearBydRest.add(model);

        model = new AllCouponModel(R.drawable.veg, "Yummy Food");
        arrayList_NearBydRest.add(model);

        model = new AllCouponModel(R.drawable.temp_food, "Piramid");
        arrayList_NearBydRest.add(model);

        model = new AllCouponModel(R.drawable.pizza, "Grand Pizza");
        arrayList_NearBydRest.add(model);
*/

        for (int i = 0; i < 15; i++) {
            arrayList_NearBydRest.add("a");
        }
        recycle_nearby_restaurant.setLayoutManager(nearby_restaurant_LayoutManager);
        nearByRastaurantAdpter = new NearByRastaurantAdpter(getActivity(), arrayList_NearBydRest);
        recycle_nearby_restaurant.setAdapter(nearByRastaurantAdpter);
    }

    // Service

    public void bindServiceBanner(View view) {
        listimage.clear();
        HomeCategoryModel homeCategoryModel;

        homeCategoryModel = new HomeCategoryModel(R.drawable.service_banner);
        listimage.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.service_banner);
        listimage.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.service_banner);
        listimage.add(homeCategoryModel);

        mViewPager_service = view.findViewById(R.id.mViewPager_service);
        tabLayout_service = view.findViewById(R.id.tabLayout_service);
        if (listimage.size() > 0) {
            homeServiceBannerAdpter = new HomeServiceBannerAdpter(getActivity(), listimage);
            mViewPager_service.setAdapter(homeServiceBannerAdpter);
            tabLayout_service.setupWithViewPager(mViewPager_service, true);
        }

    }

    public void bindServiceData(View view) {

        arrayList_service.clear();
        HomeCategoryModel homeCategoryModel;

        homeCategoryModel = new HomeCategoryModel(R.drawable.plumber, "Plumber");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.haircutting, "Haircutting");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.cleaning, "Cleaing");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.geyser, "Geyser Service");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.salon, "Salon");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.elctrician, "Electrician");
        arrayList_service.add(homeCategoryModel);

        recycle_service = view.findViewById(R.id.recycle_service);

        gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recycle_service.setLayoutManager(gridLayoutManager);
        home_service_adpter = new Home_Service_Adpter(getActivity(), arrayList_service);
        recycle_service.setAdapter(home_service_adpter);
    }

    public void bindServicOffereBanner(View view) {
        listimage.clear();
        HomeCategoryModel homeCategoryModel;

        homeCategoryModel = new HomeCategoryModel(R.drawable.best_offer_banner);
        listimage.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.best_offer_banner);
        listimage.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.best_offer_banner);
        listimage.add(homeCategoryModel);

        mViewPager_service_offer = view.findViewById(R.id.mViewPager_service_offer);
        tabLayout_service_offer = view.findViewById(R.id.tabLayout_service_offer);
        if (listimage.size() > 0) {
            homeService_offer_bannerAdpter = new HomeService_Offer_BannerAdpter(getActivity(), listimage);
            mViewPager_service_offer.setAdapter(homeService_offer_bannerAdpter);
            tabLayout_service_offer.setupWithViewPager(mViewPager_service_offer, true);
        }
    }

    public void GPSTracker() {
        GPSTracker mGPS = new GPSTracker(getActivity());
        if (mGPS.canGetLocation) {
            mGPS.getLocation();

            latitude = String.valueOf(mGPS.getLatitude());
            longitude = String.valueOf(mGPS.getLongitude());

            Log.e("frag lat", latitude);
            Log.e("frag long", longitude);

            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(mGPS.getLatitude(), mGPS.getLongitude(), 1); //1 num of possible location returned
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); //0 to obtain first possible address
                city = addresses.get(0).getLocality();
                state = addresses.get(0).getAdminArea();
                country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();                // commented on 27 sept 19 bcoz of screen crash
                // create your custom title
                String title = address + "-" + city + "-" + state;
               /* Log.e("addresss", address);
                Log.e("title", title);*/
                /*Log.e("getPremises", addresses.get(0).getPremises());
                Log.e("getFeatureName", addresses.get(0).getFeatureName());
                Log.e("getSubLocality", addresses.get(0).getSubLocality());
                Log.e("getSubThoroughfare", addresses.get(0).getThoroughfare());
                Log.e("city", addresses.get(0).getLocality());
                Log.e("Dist", addresses.get(0).getSubAdminArea());
                Log.e("state", addresses.get(0).getAdminArea());
                Log.e("country", addresses.get(0).getCountryName());
                Log.e("postalCode", addresses.get(0).getPostalCode());
                */
                Log.e("city", "" + city);
                SharedPreferences pref = getActivity().getSharedPreferences("city", getActivity().MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("city", city);
                editor.putString("state", state);
                editor.commit();
                pref_city = getActivity().getSharedPreferences("city", Context.MODE_PRIVATE);
                txt_location.setText(pref_city.getString("city", ""));

            }
        }

    }


    @Override
    public void onLocationChanged(@NonNull Location location) {
        updateUI(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
    }

    private void getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (loc != null)
                            updateUI(loc);
                    }
                } else {
                    loc.setLatitude(0);
                    loc.setLongitude(0);
                    updateUI(loc);
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private void getLastLocation() {
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            Location location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (getActivity().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                Log.d(TAG, "onRequestPermissionsResult");
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(
                                                        new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                } else {
                    Log.d(TAG, "No rejected permissions.");
                    canGetLocation = true;
                    getLocation();
                }
                break;
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("GPS is not Enabled!");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void updateUI(Location loc) {
        Log.d(TAG, "updateUI");
         Log.e("loaction changed getLatitude", "" + Double.toString(loc.getLatitude()));
        Log.e("loaction changed getLongitude", "" + Double.toString(loc.getLongitude()));
        GPSTracker();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    public void Replace_Fragment(Fragment fr, String Name) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.frame, fr, Name)
                .addToBackStack(Name)
                .commitAllowingStateLoss();
    }


    @Override
    public void onResume() {
        if (preferences.getString(PreferenceManager.username, "").equals("") || preferences.getString(PreferenceManager.username, "").equals("null") ||
                preferences.getString(PreferenceManager.username, "").equals(null) || preferences.getString(PreferenceManager.username, "") == null) {
            BaseActivity.txt_name.setText(preferences.getString(PreferenceManager.email_id, ""));
            BaseActivity.txt_name.setText("--");
        } else {
            BaseActivity.txt_name.setText(preferences.getString(PreferenceManager.username, ""));
        }

        if (preferences.getString(PreferenceManager.totalnotification, "").equals("") || preferences.getString(PreferenceManager.totalnotification, "").equals("null") ||
                preferences.getString(PreferenceManager.totalnotification, "").equals(null) || preferences.getString(PreferenceManager.totalnotification, "") == null ||
                preferences.getString(PreferenceManager.totalnotification, "").equals("0")) {
            BaseActivity.txt_noti_count.setVisibility(View.INVISIBLE);
        } else {
            BaseActivity.txt_noti_count.setVisibility(View.VISIBLE);
            BaseActivity.txt_noti_count.setText(preferences.getString(PreferenceManager.totalnotification, ""));
        }
        if (preferences.getString(PreferenceManager.profile_pic, "").equals("") || preferences.getString(PreferenceManager.profile_pic, "").equals("null") ||
                preferences.getString(PreferenceManager.profile_pic, "").equals(null) || preferences.getString(PreferenceManager.profile_pic, "") == null) {

        } else {
            Picasso.with(getActivity())
                    .load(preferences.getString(PreferenceManager.profile_pic, ""))
                    .into(BaseActivity.profile_image);

        }
        super.onResume();
    }
}

