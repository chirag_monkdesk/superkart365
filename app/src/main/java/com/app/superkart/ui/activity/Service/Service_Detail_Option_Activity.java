package com.app.superkart.ui.activity.Service;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.adpters.Service.Home_Service_Option_Adpter;
import com.app.superkart.model.Service.ServiceModel;
import com.app.superkart.utils.DateUtils;
import com.app.superkart.utils.Utils;
import com.squareup.picasso.Picasso;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class Service_Detail_Option_Activity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private LinearLayoutManager linearLayoutManager, linearLayoutManager1;
    private RecyclerView recycle_service, recycle_sub_service;
    private ArrayList<ServiceModel> arrayList = new ArrayList<>();
    private Home_Service_Option_Adpter home_service_option_adpter;
    private Home_Service_Sub_Option_Adpter home_service_sub_option_adpter;
    private ViewGroup hiddenPanel;
    private boolean isPanelShown;
    private RelativeLayout rl_date, rl_time;
    SimpleDateFormat simpleDateFormat, simpleTimeFormat;
    String currentDate = "", currenttime = "";
    TextView txt_date, txt_Time;
    private LinearLayout li_btn_confirm;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.home_service_detail_option);
        Log.e("Service_Detail_Option_Activity", "Service_Detail_Option_Activity");
        simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        simpleTimeFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        currentDate = simpleDateFormat.format(new Date());
        currenttime = simpleTimeFormat.format(new Date());
        Log.e("currentDate", "" + currentDate);
        Log.e("currenttime", "" + currenttime);
        init();
        BindData();
        BinSubdData();
        txt_date.setText(currentDate);
        txt_Time.setText(currenttime);
    }

    public void init() {
        rl_date = findViewById(R.id.rl_date);
        rl_time = findViewById(R.id.rl_time);
        txt_date = findViewById(R.id.txt_date);
        txt_Time = findViewById(R.id.txt_Time);
        li_btn_confirm = findViewById(R.id.li_btn_confirm);
        hiddenPanel = (ViewGroup) findViewById(R.id.rl_service_bottom);
        hiddenPanel.setVisibility(View.INVISIBLE);
        isPanelShown = false;
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        rl_date.setOnClickListener(this);
        rl_time.setOnClickListener(this);
        li_btn_confirm.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.rl_date:
                DatePicker();
                break;
            case R.id.rl_time:
                TimePicker();
                break;
            case R.id.li_btn_confirm:
                Intent i = new Intent(mcontext, PalaceOrder_HomeVisitActivity.class);
                startActivity(i);
                break;

        }
    }

    public void BindData() {
        arrayList.clear();
        ServiceModel model;

        model = new ServiceModel("Basin & Sink");
        arrayList.add(model);

        model = new ServiceModel("Bath Fitting");
        arrayList.add(model);

        model = new ServiceModel("Blockage");
        arrayList.add(model);

        model = new ServiceModel("Tap & Mixer");
        arrayList.add(model);

        model = new ServiceModel("Toilet");
        arrayList.add(model);

        model = new ServiceModel("Water Tank");
        arrayList.add(model);

        model = new ServiceModel("Motor");
        arrayList.add(model);

        model = new ServiceModel("Minor Installation");
        arrayList.add(model);

        recycle_service = findViewById(R.id.recycle_service);
        linearLayoutManager = new LinearLayoutManager(Service_Detail_Option_Activity.this, LinearLayoutManager.HORIZONTAL, false);
        recycle_service.setLayoutManager(linearLayoutManager);
        home_service_option_adpter = new Home_Service_Option_Adpter(Service_Detail_Option_Activity.this, arrayList);
        recycle_service.setAdapter(home_service_option_adpter);
    }

    public void BinSubdData() {
        arrayList.clear();
        ServiceModel model;

        model = new ServiceModel(R.drawable.waste_pipe, "Waste Pipe");
        arrayList.add(model);

        model = new ServiceModel(R.drawable.washbasin_repair, "Washbasin Repair");
        arrayList.add(model);

        model = new ServiceModel(R.drawable.washbasin_install, "Washbasin Installation");
        arrayList.add(model);

        model = new ServiceModel(R.drawable.waste_pipe, "Waste Pipe");
        arrayList.add(model);

        model = new ServiceModel(R.drawable.washbasin_repair, "Washbasin Repair");
        arrayList.add(model);

        model = new ServiceModel(R.drawable.washbasin_install, "Washbasin Installation");
        arrayList.add(model);

        recycle_sub_service = findViewById(R.id.recycle_sub_service);
        linearLayoutManager1 = new LinearLayoutManager(Service_Detail_Option_Activity.this, LinearLayoutManager.VERTICAL, false);
        recycle_sub_service.setLayoutManager(linearLayoutManager1);
        home_service_sub_option_adpter = new Home_Service_Sub_Option_Adpter(Service_Detail_Option_Activity.this, arrayList);
        recycle_sub_service.setAdapter(home_service_sub_option_adpter);
    }

    public class Home_Service_Sub_Option_Adpter extends RecyclerView.Adapter<Home_Service_Sub_Option_Adpter.MyViewHolder> {

        private List<ServiceModel> arrayList;
        private Context context;
        int pos = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item;
            public TextView txt_storeName, txt_Add, tv_viewDetails;
            LinearLayout li_bg;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_storeName = view.findViewById(R.id.txt_storeName);
                txt_Add = view.findViewById(R.id.txt_Add);
                li_bg = view.findViewById(R.id.li_bg);
                tv_viewDetails = view.findViewById(R.id.tv_viewDetails);

            }
        }

        public Home_Service_Sub_Option_Adpter(Context context, List<ServiceModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.home_service_detail_option3, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ServiceModel model = arrayList.get(position);

            Picasso.with(context)
                    .load(model.getImg())
                    .into(holder.img_item);
            holder.txt_storeName.setText(model.getName());

            if (pos == position) {
                holder.txt_Add.setBackgroundResource(R.drawable.box_corner_blue);
                holder.txt_Add.setTextColor(Color.parseColor("#FFFFFFFF"));
            } else {
                holder.txt_Add.setBackgroundResource(R.drawable.bg_corner_gray);
                holder.txt_Add.setTextColor(Color.parseColor("#FF000000"));
            }

            holder.li_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = position;
                    notifyDataSetChanged();
                }
            });

            holder.tv_viewDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    slideUpDown(v);
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

    public void slideUpDown(final View view) {
        if (!isPanelShown) {
            // Show the panel
            Animation bottomUp = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_up);

            hiddenPanel.startAnimation(bottomUp);
            hiddenPanel.setVisibility(View.VISIBLE);
            isPanelShown = true;
        } else {
            // Hide the Panel
            Animation bottomDown = AnimationUtils.loadAnimation(this,
                    R.anim.bottom_down);

            hiddenPanel.startAnimation(bottomDown);
            hiddenPanel.setVisibility(View.INVISIBLE);
            isPanelShown = false;
        }
    }

    private void DatePicker() {
        final View dialogView = View.inflate(Service_Detail_Option_Activity.this, R.layout.datetimepickernew, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(Service_Detail_Option_Activity.this).create();
        alertDialog.setCancelable(false);
        final DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
        final TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);

        dialogView.findViewById(R.id.rel_ok1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePicker.setVisibility(View.GONE);
                datePicker.setMaxDate(System.currentTimeMillis());
                Calendar c = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute());
                long time = c.getTimeInMillis();
                String dateString = new SimpleDateFormat("dd MMM yyyy").format(new Date(time));
                Log.e("dateString", "" + dateString);
                txt_date.setText(dateString);
                alertDialog.dismiss();
            }
        });


        dialogView.findViewById(R.id.rel_cancel1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    private void TimePicker() {
        final View dialogView = View.inflate(Service_Detail_Option_Activity.this, R.layout.datetimepickernew, null);
        final AlertDialog alertDialog = new AlertDialog.Builder(Service_Detail_Option_Activity.this).create();
        alertDialog.setCancelable(false);
        final DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.date_picker);
        datePicker.setVisibility(View.GONE);
        final TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);
        timePicker.setVisibility(View.VISIBLE);
        dialogView.findViewById(R.id.rel_ok1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker.setMaxDate(System.currentTimeMillis());
                Calendar c = new GregorianCalendar(datePicker.getYear(),
                        datePicker.getMonth(),
                        datePicker.getDayOfMonth(),
                        timePicker.getCurrentHour(),
                        timePicker.getCurrentMinute());
                long time = c.getTimeInMillis();
                final String dateString = new SimpleDateFormat("dd MMM yyyy").format(new Date(time));
                String dateStringTime = new SimpleDateFormat("hh:mm a").format(new Date(time));
                Log.e("dateStringTime", "" + dateStringTime);
                txt_Time.setText(dateStringTime);
                alertDialog.dismiss();
            }
        });


        dialogView.findViewById(R.id.rel_cancel1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(dialogView);
        alertDialog.show();
    }
}