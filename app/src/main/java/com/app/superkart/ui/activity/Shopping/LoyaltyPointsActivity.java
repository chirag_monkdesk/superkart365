package com.app.superkart.ui.activity.Shopping;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.superkart.R;
import com.app.superkart.adpters.Shopping.NewCouponAdpter;
import com.app.superkart.model.Shopping.AllCouponModel;
import com.app.superkart.model.Shopping.ProductListModel;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.Signup.LoginActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoyaltyPointsActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private LinearLayout li_btn_shopping, li_btn_coupon, li_btn_Lpoints, li_btn_redeem;
    private LinearLayout li_shopping, li_coupon, li_points, li_redeem, li_redeemNow;
    private LinearLayout li_shopping_item, li_main_coupon, li_loyalty_points, li_reedem_points, li_redeem_main;
    private View line_shopping, line_copuon, line_points, line_redeem;
    private RecyclerView recycle_shopping, recycle_All_coupon, recycle_New_coupon;
    private GridLayoutManager gridLayoutManager, grid_All_Coupon;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<ProductListModel> arrayList_shopping = new ArrayList<>();
    private ArrayList<String> arrayList_newCoupon = new ArrayList<>();
    private ArrayList<AllCouponModel> arrayList_all_coupon = new ArrayList<>();
    private Loyalty_Shpping_Adpter loyalty_shpping_adpter;
    private All_Coupon_Adpter all_coupon_adpter;
    private NewCouponAdpter newCouponAdpter;
    private RelativeLayout rel_loyalty_popup, rel_loyalty_login;
    private TextView txt_btn_loyaltyP, txt_btn_popup_loyalty, txt_btn_RedeenNow, txt_btn_loginP, txt_name, txt_btn_redeemlogin,
            txt_loyaltyPoints, txt_totalPoints, txt_loyaltyText, txt_btn_reddemNow;
    private EditText et_pop_loyalty, et_newLoyaltyPoints, et_copuon, et_redeemAmount;
    private ImageView img_cancel, img_popclick;
    private ImageView img_home, img_hot_deal, img_bag, img_cart, img_whishlist, img_prfile, img_loyality, img_coupon, img_card, img_location, img_help, img_setting;
    public LinearLayout slide_home, slide_deal, slide_yr_order, slide_buy_again, slide_wishlist, slide_yr_account, slide_loyal_points,
            slide_my_coupon, slide_save_card, slide_address, slide_help, slide_setting, slide_login, slide_logout;

    public DrawerLayout Drawer_layout;
    private ImageView drawer_menu, profile_image, img_click, img_cancel_login;
    private String vender_code = "", seller_id = "";
    private ProgressDialog progressDialog;
    SharedPreferences preferences;
    private TextView txt_storeName, txt_city, txt_rating, txt_time, txt_cart_count, txt_msg, txt_venadename;
    private ImageView img_store, img_no_coupon;
    private RatingBar rating;
    private RelativeLayout rl_cart_items;
    private SharedPreferences pre_offline_cart_count;
    private EditText et_email, et_password;
    private TextView txt_btn_login;
    SharedPreferences preferences_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.loyalty_points_main_layout);
        Log.e("LoyaltyPointsActivity", "LoyaltyPointsActivity");
        init();
        clickListner();

        Log.e("url", "" + AppConstant.scannning_url);
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        pre_offline_cart_count = getSharedPreferences("cart_offinr_count", Context.MODE_PRIVATE);
        preferences_token = getSharedPreferences("X", Context.MODE_PRIVATE);

        if (preferences.getString(PreferenceManager.username, "").equals("") || preferences.getString(PreferenceManager.username, "").equals("null") ||
                preferences.getString(PreferenceManager.username, "").equals(null) || preferences.getString(PreferenceManager.username, "") == null) {
            txt_name.setText(preferences.getString(PreferenceManager.email_id, ""));
        } else {
            txt_name.setText(preferences.getString(PreferenceManager.username, ""));
        }

        if (preferences.getString(PreferenceManager.profile_pic, "").equals("") || preferences.getString(PreferenceManager.profile_pic, "").equals("null") ||
                preferences.getString(PreferenceManager.profile_pic, "").equals(null) || preferences.getString(PreferenceManager.profile_pic, "") == null) {

        } else {
           /* Picasso.with(LoyaltyPointsActivity.this)
                    .load(preferences.getString(PreferenceManager.profile_pic, ""))
                    .into(profile_image);*/

            Glide
                    .with(LoyaltyPointsActivity.this)
                    .load(preferences.getString(PreferenceManager.profile_pic, ""))
                    .centerCrop()
                    .into(profile_image);


        }

       /* if (pre_offline_cart_count.getString(PreferenceManager.Cart_Count, "").equals("") || pre_offline_cart_count.getString(PreferenceManager.Cart_Count, "").equals("0")) {
            rl_cart_items.setEnabled(false);
            txt_cart_count.setVisibility(View.GONE);

        } else {
            rl_cart_items.setEnabled(true);
            txt_cart_count.setVisibility(View.VISIBLE);
            txt_cart_count.setText(pre_offline_cart_count.getString(PreferenceManager.Cart_Count, ""));
        }*/

        String[] separated = AppConstant.scannning_url.split("/");
        for (int i = 0; i < separated.length; i++) {
            vender_code = separated[i];
            Log.e("vender_code", "" + vender_code);
        }

        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
            li_redeemNow.setVisibility(View.GONE);
            li_redeem_main.setVisibility(View.GONE);
            txt_btn_loginP.setVisibility(View.VISIBLE);
            txt_btn_redeemlogin.setVisibility(View.VISIBLE);
            slide_login.setVisibility(View.VISIBLE);
            slide_logout.setVisibility(View.GONE);
        } else {
            li_redeemNow.setVisibility(View.VISIBLE);
            li_redeem_main.setVisibility(View.VISIBLE);
            txt_btn_loginP.setVisibility(View.GONE);
            txt_btn_redeemlogin.setVisibility(View.GONE);
            slide_login.setVisibility(View.GONE);
            slide_logout.setVisibility(View.VISIBLE);
        }


        for (int i = 0; i < 5; i++) {
            arrayList_newCoupon.add("a");
        }

        linearLayoutManager = new LinearLayoutManager(LoyaltyPointsActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recycle_New_coupon.setLayoutManager(linearLayoutManager);
        newCouponAdpter = new NewCouponAdpter(LoyaltyPointsActivity.this, arrayList_newCoupon);
        recycle_New_coupon.setAdapter(newCouponAdpter);
    }

    public void init() {

        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        Drawer_layout = findViewById(R.id.Drawer_layout);
        drawer_menu = findViewById(R.id.drawer_menu);
        drawer_menu.setOnClickListener(onClickListenerBase);

        recycle_shopping = findViewById(R.id.recycle_shopping);
        recycle_New_coupon = findViewById(R.id.recycle_New_coupon);
        txt_storeName = findViewById(R.id.txt_storeName);
        img_store = findViewById(R.id.img_store);
        txt_city = findViewById(R.id.txt_city);
        txt_rating = findViewById(R.id.txt_rating);
        rating = findViewById(R.id.rating);
        txt_time = findViewById(R.id.txt_time);
        li_redeemNow = findViewById(R.id.li_redeemNow);
        txt_btn_loginP = findViewById(R.id.txt_btn_loginP);
        txt_name = findViewById(R.id.txt_name);
        txt_btn_redeemlogin = findViewById(R.id.txt_btn_redeemlogin);
        txt_loyaltyPoints = findViewById(R.id.txt_loyaltyPoints);
        txt_totalPoints = findViewById(R.id.txt_totalPoints);
        txt_loyaltyText = findViewById(R.id.txt_loyaltyText);
        et_pop_loyalty = findViewById(R.id.et_pop_loyalty);
        txt_btn_reddemNow = findViewById(R.id.txt_btn_reddemNow);
        et_newLoyaltyPoints = findViewById(R.id.et_newLoyaltyPoints);
        et_copuon = findViewById(R.id.et_copuon);
        et_redeemAmount = findViewById(R.id.et_redeemAmount);
        rl_cart_items = findViewById(R.id.rl_cart_items);
        txt_cart_count = findViewById(R.id.txt_cart_count);
        profile_image = findViewById(R.id.profile_image);
        rel_loyalty_login = findViewById(R.id.rel_loyalty_login);
        img_click = findViewById(R.id.img_click);
        txt_msg = findViewById(R.id.txt_msg);
        img_no_coupon = findViewById(R.id.img_no_coupon);
        txt_venadename = findViewById(R.id.txt_venadename);

        img_home = findViewById(R.id.img_home);
        img_hot_deal = findViewById(R.id.img_hot_deal);
        img_bag = findViewById(R.id.img_bag);
        img_cart = findViewById(R.id.img_cart);
        img_whishlist = findViewById(R.id.img_whishlist);
        img_prfile = findViewById(R.id.img_prfile);
        img_loyality = findViewById(R.id.img_loyality);
        img_coupon = findViewById(R.id.img_coupon);
        img_card = findViewById(R.id.img_card);
        img_location = findViewById(R.id.img_location);
        img_help = findViewById(R.id.img_help);
        img_setting = findViewById(R.id.img_setting);

        slide_home = findViewById(R.id.slide_home);
        slide_deal = findViewById(R.id.slide_deal);
        slide_yr_order = findViewById(R.id.slide_yr_order);
        slide_buy_again = findViewById(R.id.slide_buy_again);
        slide_wishlist = findViewById(R.id.slide_wishlist);
        slide_yr_account = findViewById(R.id.slide_yr_account);
        slide_loyal_points = findViewById(R.id.slide_loyal_points);
        slide_my_coupon = findViewById(R.id.slide_my_coupon);
        slide_save_card = findViewById(R.id.slide_save_card);
        slide_address = findViewById(R.id.slide_address);
        slide_help = findViewById(R.id.slide_help);
        slide_setting = findViewById(R.id.slide_setting);
        slide_login = findViewById(R.id.slide_login);
        slide_logout = findViewById(R.id.slide_logout);

        img_cancel_login = findViewById(R.id.img_cancel_login);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        txt_btn_login = findViewById(R.id.txt_btn_login);
        recycle_All_coupon = findViewById(R.id.recycle_All_coupon);

        rel_loyalty_popup = findViewById(R.id.rel_loyalty_popup);
        txt_btn_loyaltyP = findViewById(R.id.txt_btn_loyaltyP);
        txt_btn_popup_loyalty = findViewById(R.id.txt_btn_popup_loyalty);
        img_cancel = findViewById(R.id.img_cancel);
        img_popclick = findViewById(R.id.img_popclick);
        txt_btn_RedeenNow = findViewById(R.id.txt_btn_RedeenNow);

        li_btn_shopping = findViewById(R.id.li_btn_shopping);
        li_btn_coupon = findViewById(R.id.li_btn_coupon);
        li_btn_Lpoints = findViewById(R.id.li_btn_Lpoints);
        li_btn_redeem = findViewById(R.id.li_btn_redeem);
        li_shopping = findViewById(R.id.li_shopping);
        li_coupon = findViewById(R.id.li_coupon);
        li_points = findViewById(R.id.li_points);
        li_redeem = findViewById(R.id.li_redeem);
        line_shopping = findViewById(R.id.line_shopping);
        line_copuon = findViewById(R.id.line_copuon);
        line_points = findViewById(R.id.line_points);
        line_redeem = findViewById(R.id.line_redeem);
        li_shopping_item = findViewById(R.id.li_shopping_item);
        li_main_coupon = findViewById(R.id.li_main_coupon);
        li_loyalty_points = findViewById(R.id.li_loyalty_points);
        li_reedem_points = findViewById(R.id.li_reedem_points);
        li_redeem_main = findViewById(R.id.li_redeem_main);
        li_shopping_item.setVisibility(View.VISIBLE);
        li_main_coupon.setVisibility(View.GONE);
        li_loyalty_points.setVisibility(View.GONE);
        li_reedem_points.setVisibility(View.GONE);

        li_shopping.setBackgroundResource(R.drawable.bg_yellow_square);
        li_coupon.setBackgroundResource(R.drawable.gray_corner);
        li_points.setBackgroundResource(R.drawable.gray_corner);
        li_redeem.setBackgroundResource(R.drawable.gray_corner);

        line_shopping.setVisibility(View.VISIBLE);
        line_copuon.setVisibility(View.GONE);
        line_points.setVisibility(View.GONE);
        line_redeem.setVisibility(View.GONE);
    }

    public void clickListner() {
        li_btn_shopping.setOnClickListener(this);
        li_btn_coupon.setOnClickListener(this);
        li_btn_Lpoints.setOnClickListener(this);
        li_btn_redeem.setOnClickListener(this);
        txt_btn_loyaltyP.setOnClickListener(this);
        txt_btn_popup_loyalty.setOnClickListener(this);
        img_cancel.setOnClickListener(this);
        img_popclick.setOnClickListener(this);
        txt_btn_RedeenNow.setOnClickListener(this);
        txt_btn_loginP.setOnClickListener(this);
        txt_btn_redeemlogin.setOnClickListener(this);
        txt_btn_reddemNow.setOnClickListener(this);
        rl_cart_items.setOnClickListener(this);
        img_click.setOnClickListener(this);
        img_cancel_login.setOnClickListener(this);
        txt_btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.li_btn_shopping:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                li_shopping.setBackgroundResource(R.drawable.bg_yellow_square);
                li_coupon.setBackgroundResource(R.drawable.gray_corner);
                li_points.setBackgroundResource(R.drawable.gray_corner);
                li_redeem.setBackgroundResource(R.drawable.gray_corner);
                line_shopping.setVisibility(View.VISIBLE);
                line_copuon.setVisibility(View.GONE);
                line_points.setVisibility(View.GONE);
                line_redeem.setVisibility(View.GONE);
                li_shopping_item.setVisibility(View.VISIBLE);
                li_main_coupon.setVisibility(View.GONE);
                li_loyalty_points.setVisibility(View.GONE);
                li_reedem_points.setVisibility(View.GONE);
                break;
            case R.id.li_btn_coupon:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                li_shopping.setBackgroundResource(R.drawable.gray_corner);
                li_coupon.setBackgroundResource(R.drawable.bg_yellow_square);
                li_points.setBackgroundResource(R.drawable.gray_corner);
                li_redeem.setBackgroundResource(R.drawable.gray_corner);
                line_shopping.setVisibility(View.GONE);
                line_copuon.setVisibility(View.VISIBLE);
                line_points.setVisibility(View.GONE);
                line_redeem.setVisibility(View.GONE);
                li_shopping_item.setVisibility(View.GONE);
                li_main_coupon.setVisibility(View.VISIBLE);
                li_loyalty_points.setVisibility(View.GONE);
                li_reedem_points.setVisibility(View.GONE);
                break;
            case R.id.li_btn_Lpoints:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                li_shopping.setBackgroundResource(R.drawable.gray_corner);
                li_coupon.setBackgroundResource(R.drawable.gray_corner);
                li_points.setBackgroundResource(R.drawable.bg_yellow_square);
                li_redeem.setBackgroundResource(R.drawable.gray_corner);
                line_shopping.setVisibility(View.GONE);
                line_copuon.setVisibility(View.GONE);
                line_points.setVisibility(View.VISIBLE);
                line_redeem.setVisibility(View.GONE);
                li_shopping_item.setVisibility(View.GONE);
                li_main_coupon.setVisibility(View.GONE);
                li_loyalty_points.setVisibility(View.VISIBLE);
                li_reedem_points.setVisibility(View.GONE);
                break;
            case R.id.li_btn_redeem:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                li_shopping.setBackgroundResource(R.drawable.gray_corner);
                li_coupon.setBackgroundResource(R.drawable.gray_corner);
                li_points.setBackgroundResource(R.drawable.gray_corner);
                li_redeem.setBackgroundResource(R.drawable.bg_yellow_square);
                line_shopping.setVisibility(View.GONE);
                line_copuon.setVisibility(View.GONE);
                line_points.setVisibility(View.GONE);
                line_redeem.setVisibility(View.VISIBLE);
                li_shopping_item.setVisibility(View.GONE);
                li_main_coupon.setVisibility(View.GONE);
                li_loyalty_points.setVisibility(View.GONE);
                li_reedem_points.setVisibility(View.VISIBLE);
                break;
            case R.id.txt_btn_loyaltyP:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i_cart = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                    startActivity(i_cart);
                } else {
                    rel_loyalty_popup.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.img_cancel:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                rel_loyalty_popup.setVisibility(View.GONE);
                break;
            case R.id.img_popclick:
                break;
            case R.id.txt_btn_popup_loyalty:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                if (Utils.isNetworkAvailable(LoyaltyPointsActivity.this)) {
                    String loyalty_amount = et_pop_loyalty.getText().toString().trim();
                    loyaltyPointRequest(loyalty_amount, v);
                } else {
                    Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.txt_btn_RedeenNow:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                li_shopping.setBackgroundResource(R.drawable.gray_corner);
                li_coupon.setBackgroundResource(R.drawable.gray_corner);
                li_points.setBackgroundResource(R.drawable.gray_corner);
                li_redeem.setBackgroundResource(R.drawable.bg_yellow_square);
                line_shopping.setVisibility(View.GONE);
                line_copuon.setVisibility(View.GONE);
                line_points.setVisibility(View.GONE);
                line_redeem.setVisibility(View.VISIBLE);
                li_shopping_item.setVisibility(View.GONE);
                li_main_coupon.setVisibility(View.GONE);
                li_loyalty_points.setVisibility(View.GONE);
                li_reedem_points.setVisibility(View.VISIBLE);
                break;
            case R.id.txt_btn_loginP:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                AppConstant.qr_code_login = "1";
                AppConstant.scannning_view = "1";
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                Intent i = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.txt_btn_redeemlogin:
                AppConstant.qr_code_login = "1";
                AppConstant.scannning_view = "1";
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                Intent intent = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.txt_btn_reddemNow:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                if (checkValidations1(v)) {
                    if (Utils.isNetworkAvailable(LoyaltyPointsActivity.this)) {
                        String previousloyalty = txt_totalPoints.getText().toString().trim();
                        String loyaltyPoints = et_newLoyaltyPoints.getText().toString().trim();
                        String coupon = et_copuon.getText().toString().trim();
                        String amount = et_redeemAmount.getText().toString().trim();
                        redeemRewards(previousloyalty, loyaltyPoints, coupon, amount);
                    } else {
                        Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.rl_cart_items:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                AppConstant.scannning_view = "1";
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i_cart = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                    startActivity(i_cart);
                } else {
                    Intent i_order = new Intent(LoyaltyPointsActivity.this, offlineCartListing_NewActivity.class);
                    startActivity(i_order);
                }
                break;
            case R.id.img_click:
                break;
            case R.id.img_cancel_login:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                rel_loyalty_login.setVisibility(View.GONE);
                et_email.setText("");
                et_password.setText("");

                break;
            case R.id.txt_btn_login:
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                String str_email = et_email.getText().toString().trim();
                String str_password = et_password.getText().toString().trim();
                Utils.hideKeyboard(LoyaltyPointsActivity.this);
                if (checkValidationslogin(v)) {
                    if (Utils.isNetworkAvailable(LoyaltyPointsActivity.this)) {
                        Login(v, str_email, str_password);
                    } else {
                        Utils.showSnackBar(v, getResources().getString(R.string.check_internet));
                    }
                }
                break;
        }
    }

    public void Login(View v, String email, String password) {
        ProgressDialog progressDialog_main = new ProgressDialog(LoyaltyPointsActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDialog_main.setMessage("Loading...");
        progressDialog_main.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog_main.setIndeterminate(true);
        progressDialog_main.setCancelable(false);
        progressDialog_main.show();
        String url = Constants.Login;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog_main.dismiss();
                            Utils.hideKeyboard(LoyaltyPointsActivity.this);
                            JSONObject obj_main = new JSONObject(response);
                            Log.e("login response", response);
                            if (obj_main.optString("status").equals("1")) {
                                JSONObject obj_result = obj_main.getJSONObject("result");
                                SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString(PreferenceManager.user_id, obj_result.optString("user_id"));
                                editor.putString(PreferenceManager.email_id, obj_result.optString("email"));
                                editor.putString(PreferenceManager.username, obj_result.optString("username"));
                                editor.putString(PreferenceManager.phone_number, obj_result.optString("mobile"));
                                editor.putString(PreferenceManager.address_1, obj_result.optString("address_line1"));
                                editor.putString(PreferenceManager.address_2, obj_result.optString("address_line2"));
                                editor.putString(PreferenceManager.postal_code, obj_result.optString("zip"));
                                editor.putString(PreferenceManager.state, obj_result.optString("state"));
                                editor.putString(PreferenceManager.city, obj_result.optString("city"));
                                editor.putString(PreferenceManager.user_type, obj_result.optString("user_type"));
                                editor.putString(PreferenceManager.token_in_return, obj_result.optString("token"));
                                editor.putString(PreferenceManager.profile_pic, obj_result.optString("profile_pic"));
                                editor.putString(PreferenceManager.totalnotification, obj_main.optString("totalnotification"));
                                editor.commit();

                                if (obj_result.optString("username").equals("") || obj_result.optString("username").equals("null") || obj_result.optString("username").equals(null)) {

                                } else {
                                    txt_name.setText(obj_result.optString("username"));
                                }

                                if (obj_result.optString("profile_pic").equals("") || obj_result.optString("profile_pic").equals("null") || obj_result.optString("profile_pic").equals(null)) {

                                } else {

                                    Glide
                                            .with(LoyaltyPointsActivity.this)
                                            .load(obj_result.optString("profile_pic"))
                                            .centerCrop()
                                            .into(profile_image);
//                                    Picasso.with(LoyaltyPointsActivity.this)
//                                            .load(obj_result.optString("profile_pic"))
//                                            .into(profile_image);
                                }

                                if (obj_result.optString("username").equals("") || obj_result.optString("username").equals("null") || obj_result.optString("username").equals(null)) {
                                    BaseActivity.txt_name.setText(obj_result.optString("email"));
                                } else {
                                    BaseActivity.txt_name.setText(obj_result.optString("username"));
                                }
                                if (obj_result.optString("profile_pic").equals("") || obj_result.optString("profile_pic").equals("null") || obj_result.optString("profile_pic").equals(null)) {

                                } else {
                                    Glide
                                            .with(LoyaltyPointsActivity.this)
                                            .load(obj_result.optString("profile_pic"))
                                            .centerCrop()
                                            .into(BaseActivity.profile_image);
                                }


                                Toast.makeText(LoyaltyPointsActivity.this, "Now you can add product in cart.", Toast.LENGTH_LONG).show();
                                rel_loyalty_login.setVisibility(View.GONE);
                                et_email.setText("");
                                et_password.setText("");
                                vendorinstore();
                            } else {
                                Utils.showSnackBar(v, obj_main.optString("msg"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog_main.isShowing())
                                progressDialog_main.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (progressDialog_main.isShowing())
                            progressDialog_main.dismiss();
                        String message = "";
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            // message = getResources().getString(R.string.internet_error);
                        }
                        // Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Utils.showSnackBar(v, message);
                        //   showSnack(message);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                //params.put("Content-Type", "multipart/form-data");
                //  params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                params.put("device_id", preferences_token.getString(PreferenceManager.DeivceToken, ""));
                params.put("regi_from", Constants.regi_from);
                //params.put("token", preferences.getString(PreferenceManager.token_in_return, ""));
                Log.e("params", "" + url + params);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void vendorinstore() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.vendorinstore, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONObject json_result = json_main.optJSONObject("result");
                        txt_loyaltyPoints.setText("₹" + json_result.optString("loyalty_rewards"));
                        txt_totalPoints.setText(json_result.optString("loyalty_rewards"));
                        txt_loyaltyText.setText("You have won ₹" + json_result.optString("loyalty_rewards") + " Loyalty Points");
                        JSONArray array_vendor_info = json_result.optJSONArray("vendor_info");
                        for (int i = 0; i < array_vendor_info.length(); i++) {
                            seller_id = array_vendor_info.optJSONObject(i).optString("seller_id");
                            AppConstant.seller_id = seller_id;
                            txt_storeName.setText(array_vendor_info.optJSONObject(i).optString("seller_name"));
                            txt_venadename.setText(array_vendor_info.optJSONObject(i).optString("seller_name"));
                            if (array_vendor_info.optJSONObject(i).optString("seller_image").equals("")) {
                            } else {
                                Picasso.with(mcontext)
                                        .load(array_vendor_info.optJSONObject(i).optString("seller_image"))
                                        .into(img_store);
                            }
                            if (array_vendor_info.optJSONObject(i).optString("seller_city").equals("")) {
                            } else {
                                txt_city.setText(array_vendor_info.optJSONObject(i).optString("seller_city") + "," + array_vendor_info.optJSONObject(i).optString("seller_state"));
                            }
                            if (array_vendor_info.optJSONObject(i).optString("rating").equals("")) {
                            } else {
                                txt_rating.setText(array_vendor_info.optJSONObject(i).optString("rating") + ".0");
                                rating.setRating(array_vendor_info.optJSONObject(i).optInt("rating"));
                            }
                            if (array_vendor_info.optJSONObject(i).optString("time").equals("")) {
                            } else {
                                txt_time.setText(array_vendor_info.optJSONObject(i).optString("time"));
                            }
                            JSONArray array_vendor_products = json_result.optJSONArray("vendor_products");
                            vendor_products(array_vendor_products);
                            JSONArray array_vendor_coupons = json_result.optJSONArray("vendor_coupons");
                            BindAllCoupon(array_vendor_coupons);

                            if (json_main.optString("totalCart").equals("") || json_main.optString("totalCart").equals("0")) {
                                txt_cart_count.setVisibility(View.GONE);
                                SharedPreferences cart_count_offine = getSharedPreferences("cart_offinr_count", 0);
                                cart_count_offine.edit().clear().commit();
                            } else {
                                SharedPreferences pref_cart_offline = getSharedPreferences("cart_offinr_count", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor2 = pref_cart_offline.edit();
                                editor2.putString(PreferenceManager.Cart_Count, json_main.optString("totalCart"));
                                editor2.commit();
                                txt_cart_count.setVisibility(View.VISIBLE);
                                txt_cart_count.setText(json_main.optString("totalCart"));
                            }
                        }
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("seller_id", vender_code);
                Log.e("params", "" + Constants.vendorinstore + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.vendorinstore);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void loyaltyPointRequest(String loyalty_amount, View v) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.loyaltyPointRequest, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        Toast.makeText(mcontext, json_main.optString("message"), Toast.LENGTH_LONG).show();
                        rel_loyalty_popup.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("seller_id", vender_code);
                params.put("loyalty_amount", loyalty_amount);
                Log.e("params", "" + Constants.loyaltyPointRequest + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.loyaltyPointRequest);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void redeemRewards(String previousloyalty, String loyaltyPoints, String coupon, String amount) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.redeemRewards, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        Toast.makeText(mcontext, json_main.optString("message"), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("seller_id", vender_code);
                params.put("previousloyalty", previousloyalty);
                params.put("couponCode", coupon);
                params.put("loyaltyPoints", loyaltyPoints);
                params.put("orderTotal", amount);
                Log.e("params", "" + Constants.redeemRewards + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.vendorinstore);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    View.OnClickListener onClickListenerBase = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.drawer_menu:
                    openDrawer();
                    break;
            }
        }
    };

    public void vendor_products(JSONArray vendor_products) {
        arrayList_shopping.clear();
        for (int i = 0; i < vendor_products.length(); i++) {
            ProductListModel model = new ProductListModel();
            model.setProduct_id(vendor_products.optJSONObject(i).optString("product_id"));
            model.setProduct_image(vendor_products.optJSONObject(i).optString("product_image"));
            model.setProduct_name(vendor_products.optJSONObject(i).optString("product_name"));
            model.setSale_price(vendor_products.optJSONObject(i).optString("sale_price"));
            model.setRegular_price(vendor_products.optJSONObject(i).optString("regular_price"));
            model.setIs_fav(vendor_products.optJSONObject(i).optString("is_fav"));
            arrayList_shopping.add(model);
        }
        if (arrayList_shopping.size() > 0) {
            gridLayoutManager = new GridLayoutManager(LoyaltyPointsActivity.this, 2);
            recycle_shopping.setLayoutManager(gridLayoutManager);
            loyalty_shpping_adpter = new Loyalty_Shpping_Adpter(LoyaltyPointsActivity.this, arrayList_shopping, LoyaltyPointsActivity.this);
            recycle_shopping.setAdapter(loyalty_shpping_adpter);
        }

    }

    public class Loyalty_Shpping_Adpter extends RecyclerView.Adapter<Loyalty_Shpping_Adpter.MyViewHolder> {

        private List<ProductListModel> arrayList;
        private Context context;
        private int pos = 0;
        SharedPreferences preferences;
        Activity activity;
        private ProgressDialog progressDialog;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_item, img_fav, img_add_cart;
            private TextView txt_item, txt_salePrice, txt_RegularPrice;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_item = view.findViewById(R.id.txt_item);
                txt_salePrice = view.findViewById(R.id.txt_salePrice);
                txt_RegularPrice = view.findViewById(R.id.txt_RegularPrice);
                img_fav = view.findViewById(R.id.img_fav);
                img_add_cart = view.findViewById(R.id.img_add_cart);
            }
        }

        public Loyalty_Shpping_Adpter(Context context, List<ProductListModel> arrayList, LoyaltyPointsActivity activity) {
            this.context = context;
            this.arrayList = arrayList;
            this.activity = activity;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.loyalty_shopping_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ProductListModel model = arrayList.get(position);
            preferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
            progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
            if (model.getProduct_name().equals("")) {

            } else {
                Picasso.with(context)
                        .load(model.getProduct_image())
                        .into(holder.img_item);
            }
            if (model.getProduct_name().equals("")) {

            } else {
                holder.txt_item.setText(model.getProduct_name());
            }

            if (model.getSale_price().equals("")) {

            } else {
                holder.txt_salePrice.setText("₹" + model.getSale_price());
            }
            if (model.getRegular_price().equals("")) {

            } else {
                holder.txt_RegularPrice.setText(model.getRegular_price());
            }
            if (model.getIs_fav().equals("0")) {
                holder.img_fav.setBackgroundResource(R.drawable.dislike);
            } else {
                holder.img_fav.setBackgroundResource(R.drawable.like);
            }
            holder.img_add_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                        Utils.showSnackBar(v, "Please login to add product into cart.");
                        rel_loyalty_login.setVisibility(View.VISIBLE);
                    } else {
                        if (Utils.isNetworkAvailable(LoyaltyPointsActivity.this)) {
                            AddToCartOffline(model.getProduct_id());
                        } else {
                            Toast.makeText(LoyaltyPointsActivity.this, R.string.check_internet, Toast.LENGTH_LONG).show();
                        }
                    }

                }
            });

            holder.img_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                        Utils.showSnackBar(v, "Please Login to add this product whistlist.");
                    } else {
                        if (Utils.isNetworkAvailable(activity)) {
                            if (model.getIs_fav().equals("0")) {
                                addtowishlist(model.getProduct_id());
                            } else {
                                removewishlist(model.getProduct_id());
                            }

                        } else {
                            Toast.makeText(context, R.string.check_internet, Toast.LENGTH_LONG).show();
                        }

                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        private void addtowishlist(String product_id) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.addtowishlist, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("status").equalsIgnoreCase("1")) {
                            vendorinstore();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("product_id", product_id);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.addtowishlist + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.addtowishlist);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

        private void removewishlist(String product_id) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.removewishlist, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("status").equalsIgnoreCase("1")) {
                            vendorinstore();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("product_id", product_id);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.removewishlist + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.removewishlist);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

        private void AddToCartOffline(String product_id) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.AddToCartOffline, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("RESULT").equalsIgnoreCase("1")) {
                            Toast.makeText(context, json_main.optString("MSG"), Toast.LENGTH_SHORT).show();
                            if (json_main.optString("totalCart").equals("") || json_main.optString("totalCart").equals("0")) {
                                txt_cart_count.setVisibility(View.GONE);
                                SharedPreferences cart_count_offine = getSharedPreferences("cart_offinr_count", 0);
                                cart_count_offine.edit().clear().commit();
                            } else {
                                SharedPreferences pref_cart_offline = getSharedPreferences("cart_offinr_count", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor2 = pref_cart_offline.edit();
                                editor2.putString(PreferenceManager.Cart_Count, json_main.optString("totalCart"));
                                editor2.commit();
                                txt_cart_count.setVisibility(View.VISIBLE);
                                txt_cart_count.setText(json_main.optString("totalCart"));
                            }

                        } else {
                            txt_cart_count.setVisibility(View.GONE);
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("product_id", product_id);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    params.put("qty", "1");
                    Log.e("params", "" + Constants.AddToCartOffline + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.AddToCartOffline);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

    }

    public void SlideMenu(View v) {
        Drawer_layout.closeDrawers();
        switch (v.getId()) {
            case R.id.slide_home:
                AppConstant.scannning_view = "0";
                img_home.setBackgroundResource(R.drawable.home_blue);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                Intent home = new Intent(LoyaltyPointsActivity.this, BaseActivity.class);
                startActivity(home);
                break;
            case R.id.slide_deal:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal_blue);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_yr_order:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_blue);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_order = new Intent(LoyaltyPointsActivity.this, MyOrderListActivity.class);
                    startActivity(i_order);
                }
                break;
            case R.id.slide_buy_again:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart_blue);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_wishlist:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist_blue);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_whish = new Intent(LoyaltyPointsActivity.this, WhishListActivity.class);
                    startActivity(i_whish);
                }
                break;
            case R.id.slide_yr_account:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile_blue);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_loyal_points:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty_blue);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_addreess = new Intent(LoyaltyPointsActivity.this, UserLoyaltyPointsActivity.class);
                    startActivity(i_addreess);
                }
                break;
            case R.id.slide_my_coupon:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons_blue);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_save_card:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card_blue);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_address:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location_blue);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Intent i = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i_addreess = new Intent(LoyaltyPointsActivity.this, AddressListActivity.class);
                    startActivity(i_addreess);
                }
                break;
            case R.id.slide_help:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help_blue);
                img_setting.setBackgroundResource(R.drawable.settings);
                break;
            case R.id.slide_setting:
                img_home.setBackgroundResource(R.drawable.home);
                img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
                img_bag.setBackgroundResource(R.drawable.shopping_bag);
                img_cart.setBackgroundResource(R.drawable.cart);
                img_whishlist.setBackgroundResource(R.drawable.wishlist);
                img_prfile.setBackgroundResource(R.drawable.profile);
                img_loyality.setBackgroundResource(R.drawable.loyalty);
                img_coupon.setBackgroundResource(R.drawable.my_coupons);
                img_card.setBackgroundResource(R.drawable.card);
                img_location.setBackgroundResource(R.drawable.location);
                img_help.setBackgroundResource(R.drawable.help);
                img_setting.setBackgroundResource(R.drawable.settings_blue);
                break;
            case R.id.slide_login:
                AppConstant.scannning_view = "1";
                Intent i = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
                break;
            case R.id.slide_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(LoyaltyPointsActivity.this, R.style.AppCompatAlertDialogStyle);
                builder.setTitle(R.string.app_name);
                builder.setMessage("Do you want to logout?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        AppConstant.scannning_view = "1";
                        SharedPreferences preferences = getSharedPreferences(mcontext.getString(R.string.app_name), 0);
                        preferences.edit().clear().commit();
                        SharedPreferences cart_count = getSharedPreferences("cart_count", 0);
                        cart_count.edit().clear().commit();

                        SharedPreferences cart_count_offine = getSharedPreferences("cart_offinr_count", 0);
                        cart_count_offine.edit().clear().commit();
                        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                            li_redeemNow.setVisibility(View.GONE);
                            li_redeem_main.setVisibility(View.GONE);
                            txt_btn_loginP.setVisibility(View.VISIBLE);
                            txt_btn_redeemlogin.setVisibility(View.VISIBLE);
                            slide_login.setVisibility(View.VISIBLE);
                            slide_logout.setVisibility(View.GONE);
                            txt_name.setText("--");
                        } else {
                            if (preferences.getString(PreferenceManager.username, "").equals("") || preferences.getString(PreferenceManager.username, "").equals("null") ||
                                    preferences.getString(PreferenceManager.username, "").equals(null) || preferences.getString(PreferenceManager.username, "") == null) {
                                txt_name.setText(preferences.getString(PreferenceManager.email_id, ""));
                            } else {
                                txt_name.setText(preferences.getString(PreferenceManager.username, ""));
                            }
                            li_redeemNow.setVisibility(View.VISIBLE);
                            li_redeem_main.setVisibility(View.VISIBLE);
                            txt_btn_loginP.setVisibility(View.GONE);
                            txt_btn_redeemlogin.setVisibility(View.GONE);
                            slide_login.setVisibility(View.GONE);
                            slide_logout.setVisibility(View.VISIBLE);
                        }
                        Intent i = new Intent(LoyaltyPointsActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alert = builder.create();
                alert.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface arg0) {
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
                        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                        //dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.black));
                    }
                });
                alert.show();
                break;
        }
    }

    public void openDrawer() {
        if (Drawer_layout.isDrawerVisible(GravityCompat.START)) {
            Drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            Drawer_layout.openDrawer(GravityCompat.START);
        }
    }

    public void BindAllCoupon(JSONArray array_vendor_coupons) {
        arrayList_all_coupon.clear();
        for (int i = 0; i < array_vendor_coupons.length(); i++) {
            AllCouponModel model = new AllCouponModel();
            model.setNewImage_coupon(array_vendor_coupons.optJSONObject(i).optString("image"));
            model.setName(array_vendor_coupons.optJSONObject(i).optString("disc"));
            model.setCoupon_code(array_vendor_coupons.optJSONObject(i).optString("Coupon_Code"));
            model.setDiscount_Price(array_vendor_coupons.optJSONObject(i).optString("Discount_Price"));
            arrayList_all_coupon.add(model);
        }
        if (arrayList_all_coupon.size() > 0) {
            txt_msg.setVisibility(View.GONE);
            img_no_coupon.setVisibility(View.GONE);
            recycle_All_coupon.setVisibility(View.VISIBLE);
            grid_All_Coupon = new GridLayoutManager(LoyaltyPointsActivity.this, 2);
            recycle_All_coupon.setLayoutManager(grid_All_Coupon);
            all_coupon_adpter = new All_Coupon_Adpter(LoyaltyPointsActivity.this, arrayList_all_coupon);
            recycle_All_coupon.setAdapter(all_coupon_adpter);
        } else {
            txt_msg.setVisibility(View.GONE);
            img_no_coupon.setVisibility(View.VISIBLE);
            recycle_All_coupon.setVisibility(View.GONE);
        }
    }

    public class All_Coupon_Adpter extends RecyclerView.Adapter<All_Coupon_Adpter.MyViewHolder> {

        private List<AllCouponModel> arrayList;
        private Context context;
        private int pos = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_item;
            private TextView txt_code, txt_name, txt_copy;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_code = view.findViewById(R.id.txt_code);
                txt_name = view.findViewById(R.id.txt_name);
                txt_copy = view.findViewById(R.id.txt_copy);
            }
        }

        public All_Coupon_Adpter(Context context, List<AllCouponModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.all_coupon_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            AllCouponModel model = arrayList.get(position);

            if (model.getNewImage_coupon().equals("")) {

            } else {
                Picasso.with(context)
                        .load(model.getNewImage_coupon())
                        .into(holder.img_item);
            }

            if (model.getCoupon_code().equals("")) {
                holder.txt_code.setText("");
            } else {
                holder.txt_code.setText(model.getCoupon_code());
            }
            if (model.getName().equals("")) {
                holder.txt_name.setText("");
            } else {
                holder.txt_name.setText(model.getName());
            }

            holder.txt_copy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    cm.setText(holder.txt_code.getText());
                    Toast.makeText(context, "Code copied", Toast.LENGTH_SHORT).show();
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }


    private boolean checkValidations1(View v) {
        boolean result = true;
        if (et_redeemAmount.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.pleaseenterorderamount));
            result = false;
        } else if (et_newLoyaltyPoints.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.pleaseenterloyaltypoints));
            result = false;
        }
        return result;
    }

    private boolean checkValidationslogin(View v) {
        boolean result = true;
        if (et_email.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.pleaseenteremail));
            result = false;
        } else if (!Utils.emailValidator(et_email.getText().toString())) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertvalidemail));
            result = false;
        } else if (et_password.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertpassword));
            result = false;
        }
        return result;
    }

    @Override
    protected void onResume() {

        if (Utils.isNetworkAvailable(LoyaltyPointsActivity.this)) {
            vendorinstore();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
        img_home.setBackgroundResource(R.drawable.home);
        img_hot_deal.setBackgroundResource(R.drawable.hot_deal);
        img_bag.setBackgroundResource(R.drawable.shopping_bag);
        img_cart.setBackgroundResource(R.drawable.cart);
        img_whishlist.setBackgroundResource(R.drawable.wishlist);
        img_prfile.setBackgroundResource(R.drawable.profile);
        img_loyality.setBackgroundResource(R.drawable.loyalty_blue);
        img_coupon.setBackgroundResource(R.drawable.my_coupons);
        img_card.setBackgroundResource(R.drawable.card);
        img_location.setBackgroundResource(R.drawable.location);
        img_help.setBackgroundResource(R.drawable.help);
        img_setting.setBackgroundResource(R.drawable.settings);
        super.onResume();
    }
}
