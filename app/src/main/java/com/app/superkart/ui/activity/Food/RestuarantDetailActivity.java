package com.app.superkart.ui.activity.Food;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.superkart.R;
import com.app.superkart.adpters.Food.RecommendedAdpter;
import com.app.superkart.utils.Utils;

import java.util.ArrayList;

public class RestuarantDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private RecyclerView recycle_item;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<String> arrayList = new ArrayList<>();
    private RecommendedAdpter recommendedAdpter;
    private SwipeRefreshLayout swipeRefreshLayout;
    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.restaurant_detail_layout);
        Log.e("RestuarantDetailActivity", "RestuarantDetailActivity");
        init();

        for (int i = 0; i < 10; i++) {
            arrayList.add("a");
        }
        recommendedAdpter = new RecommendedAdpter(mcontext, arrayList);
        recycle_item.setAdapter(recommendedAdpter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    public void init() {
        img_back = findViewById(R.id.img_back);
        recycle_item = findViewById(R.id.recycle_item);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        linearLayoutManager = new LinearLayoutManager(mcontext, RecyclerView.VERTICAL, false);
        recycle_item.setLayoutManager(linearLayoutManager);
        img_back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }
}
