package com.app.superkart.ui.activity.Shopping;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.LoyaltyModel;
import com.app.superkart.model.Shopping.OrderModel;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserLoyaltyPointsActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mcontext;
    private RecyclerView recycle_list;
    private LinearLayoutManager linearLayoutManager;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private ArrayList<LoyaltyModel> arrayList = new ArrayList();
    private UserLoyaltyAdpter userLoyaltyAdpter;
    private ImageView img_back;
    private LinearLayout li_error_msg, li_title;
    private SwipeRefreshLayout swipeRefreshLayout;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.user_loyalty_points_list);
        Log.e("UserLoyaltyPointsActivity", "UserLoyaltyPointsActivity");
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        init();

        if (Utils.isNetworkAvailable(UserLoyaltyPointsActivity.this)) {
            userLoyaltyPoint();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {

                } else {
                    if (Utils.isNetworkAvailable(UserLoyaltyPointsActivity.this)) {
                        userLoyaltyPoint();
                    } else {
                        Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public void init() {
        img_back = findViewById(R.id.img_back);
        li_error_msg = findViewById(R.id.li_error_msg);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        li_title = findViewById(R.id.li_title);
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_list = findViewById(R.id.recycle_list);
        linearLayoutManager = new LinearLayoutManager(UserLoyaltyPointsActivity.this, RecyclerView.VERTICAL, false);
        recycle_list.setLayoutManager(linearLayoutManager);
        img_back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                if (AppConstant.chk_payment_view.equals("check")) {
                    Intent intent = new Intent(UserLoyaltyPointsActivity.this, BaseActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    AppConstant.chk_payment_view = "";
                    finish();
                }

        }
    }

    private void userLoyaltyPoint() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.userLoyaltyPoint, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    li_error_msg.setVisibility(View.GONE);
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            LoyaltyModel model = new LoyaltyModel();
                            model.setVid(array_result.optJSONObject(i).optString("vid"));
                            model.setImage(array_result.optJSONObject(i).optString("image"));
                            model.setSellername(array_result.optJSONObject(i).optString("sellername"));
                            model.setLoyalitypoint(array_result.optJSONObject(i).optString("loyalitypoint"));
                            arrayList.add(model);
                        }
                        if (arrayList.size() > 0) {
                            recycle_list.setVisibility(View.VISIBLE);
                            li_title.setVisibility(View.VISIBLE);
                            userLoyaltyAdpter = new UserLoyaltyAdpter(UserLoyaltyPointsActivity.this, arrayList);
                            recycle_list.setAdapter(userLoyaltyAdpter);
                            userLoyaltyAdpter.notifyDataSetChanged();
                        } else {
                            recycle_list.setVisibility(View.GONE);
                            li_title.setVisibility(View.GONE);
                            li_error_msg.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recycle_list.setVisibility(View.GONE);
                        li_title.setVisibility(View.GONE);
                        li_error_msg.setVisibility(View.VISIBLE);

                        //Toast.makeText(MyOrderListActivity.this, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.userLoyaltyPoint + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.userLoyaltyPoint);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class UserLoyaltyAdpter extends RecyclerView.Adapter<UserLoyaltyAdpter.MyViewHolder> {

        private List<LoyaltyModel> arrayList;
        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView txt_name, txt_points;
            private ImageView img_item;

            public MyViewHolder(View view) {
                super(view);
                txt_name = view.findViewById(R.id.txt_name);
                txt_points = view.findViewById(R.id.txt_points);
                img_item = view.findViewById(R.id.img_item);
            }
        }

        public UserLoyaltyAdpter(Context context, List<LoyaltyModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.user_loyalty_points_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            LoyaltyModel model = arrayList.get(position);
            if (model.getSellername().equals("") | model.getSellername().equals("null") || model.getSellername().equals(null)) {
                holder.txt_name.setText("");
            } else {
                holder.txt_name.setText(model.getSellername());
            }
            if (model.getImage().equals("") | model.getImage().equals("null") || model.getImage().equals(null)) {

            } else {
               /* Picasso.with(context)
                        .load(model.getImage())
                        .into(holder.img_item);*/
                Glide
                        .with(context)
                        .load(model.getImage())
                        .centerCrop()
                        .into(holder.img_item);
            }
            if (model.getLoyalitypoint().equals("") | model.getLoyalitypoint().equals("null") || model.getLoyalitypoint().equals(null)) {
                holder.txt_points.setText("");
            } else {
                holder.txt_points.setText(model.getLoyalitypoint());
            }
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }
}
