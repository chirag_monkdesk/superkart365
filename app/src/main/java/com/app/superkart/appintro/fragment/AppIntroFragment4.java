package com.app.superkart.appintro.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.superkart.R;
import com.app.superkart.ui.activity.Signup.LoginActivity;

public class AppIntroFragment4 extends Fragment {
    private TextView txt_skip;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_app_intro4, container, false);
        Log.e("AppIntroFragment4", "AppIntroFragment4");
        init(rootview);
        txt_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
            }
        });
        return rootview;
    }

    public void init(View rootview) {
        txt_skip = rootview.findViewById(R.id.txt_skip);
    }

    @Override
    public void onResume() {

        super.onResume();
    }
}
