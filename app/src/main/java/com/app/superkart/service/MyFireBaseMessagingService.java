package com.app.superkart.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.app.superkart.R;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.utils.PreferenceManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFireBaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFireBaseMessagingService.class.getSimpleName();
    private NotificationUtil notificationUtils;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public PreferenceManager preferenceManager;

    @Override
    public void onNewToken(@NonNull String string) {
        super.onNewToken(string);
        Log.e("NEW_TOKEN", "" + string);
        preferenceManager = new PreferenceManager(this);
        SharedPreferences preferences = getSharedPreferences("X", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PreferenceManager.DeivceToken, string);
        editor.commit();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage == null)
            return;

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e("Remote Message Data", "" + remoteMessage.getData());
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e("On Message Received", "" + e.getMessage());
            }
        } else {
            Log.e(TAG, "Data Payload Empty");
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e("Notification Body", "" + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtil.isAppIsInBackground(getApplicationContext())) {
            Log.e("MFMS", "## handleNotification if");
            Intent resultIntent = new Intent(getApplicationContext(), BaseActivity.class);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("FromFirebase", "2");
            // check for image attachment
            showNotificationMessage(getApplicationContext(), getResources().getString(R.string.app_name), message, "", resultIntent);
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(NotificationConfig.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            resultIntent.putExtra("FromFirebase", "2");
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            // play notification sound
            NotificationUtil notificationUtils = new NotificationUtil(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            Log.e("MFMS", "## handleNotification else");
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e("Json", "" + json);
        try {
            JSONObject data = json.getJSONObject("data");
            String title = data.getString("title");
            String message = data.getString("body");
            String Type = data.getString("type");
            Log.e("Json Title", "" + title);
            Log.e("Json Message", "" + message);
            if (!NotificationUtil.isAppIsInBackground(getApplicationContext())) {
                Log.e(TAG, "Handle Data Message In Background");
                // play notification sound
                NotificationUtil notificationUtils = new NotificationUtil(this);
                notificationUtils.playNotificationSound();
                Intent resultIntent = new Intent(getApplicationContext(), BaseActivity.class);
                resultIntent.putExtra("title", title);
                resultIntent.putExtra("message", message);
                resultIntent.putExtra("FromFirebase", "2");
                SharedPreferences sharedPreferences = getSharedPreferences("X", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("From", "2");
                editor.commit();

                // check for image attachment
                showNotificationMessage(getApplicationContext(), title, message, "", resultIntent);
                Intent pushNotification = new Intent(NotificationConfig.PUSH_NOTIFICATION);
                pushNotification.putExtra("title", title);
                pushNotification.putExtra("message", message);
                resultIntent.putExtra("FromFirebase", "2");
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            } else {
                NotificationUtil notificationUtils = new NotificationUtil(this);
                notificationUtils.playNotificationSound();
                Log.e(TAG, "Handle Data Message In Foreground");
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), BaseActivity.class);
                resultIntent.putExtra("title", title);
                resultIntent.putExtra("message", message);
                resultIntent.putExtra("FromFirebase", "2");
                SharedPreferences sharedPreferences = getSharedPreferences("X", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("From", "2");
                editor.commit();
                showNotificationMessage(getApplicationContext(), title, message, "", resultIntent);
            }
        } catch (JSONException e) {
            Log.e("Json Exception", "" + e.getMessage());
        } catch (Exception e) {
            Log.e("Exception", "" + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtil(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtil(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
