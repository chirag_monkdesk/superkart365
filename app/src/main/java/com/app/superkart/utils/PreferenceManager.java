package com.app.superkart.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.superkart.R;

public class PreferenceManager {
    public SharedPreferences mSharedPreferences;
    public static String id = "id";
    public static String user_id = "user_id";
    public static String email_id = "email_id";
    public static String phone_number = "phone_number";
    public static String profile_pic = "profile_pic";
    public static String username = "username";
    public static String gender = "gender";
    public static String user_type = "user_type";
    public static String address_1 = "address_1";
    public static String address_2 = "address_2";
    public static String postal_code = "postal_code";
    public static String state = "state";
    public static String city = "city";
    public static String Cart_Count = "cart_count";
    public static String token_in_return = "token_in_return";
    public static String IS_SIGNUP = "Is_signup";
    public static  String DeivceToken = "";
    public static  String totalnotification = "";
    Context _context;

    public PreferenceManager(Context context) {
        super();
        if (context != null) {
            this._context = context;
            mSharedPreferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        }
    }

    public void setPreference(String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key) {
        return mSharedPreferences.getString(key, null);
    }

    public void createLoginSession() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(IS_SIGNUP, true);
        editor.commit();
    }

    public void clearData() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
}
