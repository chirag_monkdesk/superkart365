package com.app.superkart.utils;


public class Constants {

    //Test URL
  // public static final String New_BASE_URL = "https://superkart365.com/dev/webservices/Api/";

    //Live URL
   public static final String New_BASE_URL = "https://superkart365.com/webservices/api/";

    public static final String Register = New_BASE_URL + "registerUser";
    public static final String verifyOtp = New_BASE_URL + "verifyOtp";
    public static final String Login = New_BASE_URL + "loginApi";
    public static final String ResendOtp = New_BASE_URL + "resentOtp";
    public static final String forgotPassword = New_BASE_URL + "forgotPassword";
    public static final String changepassword = New_BASE_URL + "changepassword";

    public static final String HomeApi = New_BASE_URL + "home";
    public static final String AllCategory = New_BASE_URL + "allshopcats";
    public static final String allsubcats = New_BASE_URL + "allsubcats";
    public static final String productlist = New_BASE_URL + "productlist";
    public static final String productdetail = New_BASE_URL + "productdetail";
    public static final String nearbysellerall = New_BASE_URL + "nearbysellerall";
    public static final String addtowishlist = New_BASE_URL + "addtowishlist";
    public static final String favoritelist = New_BASE_URL + "favoritelist";
    public static final String removewishlist = New_BASE_URL + "removewishlist";
    public static final String sellerdetail = New_BASE_URL + "sellerdetail";
    public static final String vendorinstore = New_BASE_URL + "vendorinstore";
    public static final String loyaltyPointRequest = New_BASE_URL + "loyaltyPointRequest";
    public static final String redeemRewards = New_BASE_URL + "redeemRewards";
    public static final String updateprofile = New_BASE_URL + "updateprofile";
    public static final String profile = New_BASE_URL + "profile";
    public static final String addtocart = New_BASE_URL + "addtocart";
    public static final String addaddress = New_BASE_URL + "addaddress";
    public static final String myaddresslist = New_BASE_URL + "myaddresslist";
    public static final String deleteaddress = New_BASE_URL + "deleteaddress";
    public static final String updateaddress = New_BASE_URL + "updateaddress";
    // public static final String cartlist = New_BASE_URL + "cartlist";
    public static final String deletecart = New_BASE_URL + "deletecart";
    public static final String updatecart = New_BASE_URL + "updatecart";
    public static final String offerproductlist = New_BASE_URL + "offerproductlist";
    public static final String addorder = New_BASE_URL + "addorder";
    public static final String revieworder = New_BASE_URL + "revieworder";
    public static final String AddToCartOffline = New_BASE_URL + "AddToCartOffline";
    public static final String cartlistoffline = New_BASE_URL + "cartlistoffline";
    public static final String updatecartoffline = New_BASE_URL + "updatecartoffline";
    public static final String deletecartoffline = New_BASE_URL + "deletecartoffline";
    public static final String paymentshippingcharges = New_BASE_URL + "paymentshippingcharges";
    public static final String cashfree_token_api = New_BASE_URL + "cashfree_token_api";
    public static final String successorder = New_BASE_URL + "successorder";
    public static final String orderhistory = New_BASE_URL + "orderhistory";
    public static final String orderdetail = New_BASE_URL + "orderdetail";
    public static final String gb_search = New_BASE_URL + "gb_search";
    public static final String cartlisttest = New_BASE_URL + "cartlisttest";
    public static final String cartReedemRequest = New_BASE_URL + "cartReedemRequest";
    //   public static final String cartReedemRequestOffline = New_BASE_URL + "cartReedemRequestOffline";
    //public static final String cartVendorCoupon = New_BASE_URL + "cartVendorCoupon";
    public static final String orderCancel = New_BASE_URL + "orderCancel";
    public static final String userLoyaltyPoint = New_BASE_URL + "userLoyaltyPoint";
    public static final String profilePic = New_BASE_URL + "profilePic";
    public static final String gb_search_list = New_BASE_URL + "gb_search_list";
    public static final String orderoffline = New_BASE_URL + "orderoffline";
    public static final String applyCoupon = New_BASE_URL + "applyCoupon";
    public static final String removeCoupon = New_BASE_URL + "removeCoupon";
    public static final String cityWiseCoupon = New_BASE_URL + "cityWiseCoupon";
    public static final String cartProductCoupon = New_BASE_URL + "cartProductCoupon";
    public static final String cartProductCouponOffline = New_BASE_URL + "cartProductCouponOffline";
    public static final String applyCouponOffline = New_BASE_URL + "applyCouponOffline";
    public static final String removeCouponOffline = New_BASE_URL + "removeCouponOffline";
    public static final String showAllPushNotification = New_BASE_URL + "showAllPushNotification";
    public static final String updateNotifiationPush = New_BASE_URL + "updateNotifiationPush";
    public static final String returnOrder = New_BASE_URL + "returnOrder";
    public static final String sellercitystatewise = New_BASE_URL + "sellercitystatewise";

    public static final String DeivceToken = "";
    public static final String regi_from = "2";


}
