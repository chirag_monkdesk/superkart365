package com.app.superkart.retrofit_config;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APi {
    @FormUrlEncoded
    @POST("generate_cftoken")
    Call<Res> getToken(
            @Field("orderAmount") String orderAmount,
            @Field("orderId") String orderId,
            @Field("orderCurrency") String orderCurrency);


   /* @FormUrlEncoded
    @POST("cashfree_token_api")
    Call<Res> cashfree_token_api(@Field("appId") String appId,
                                 @Field("orderId") String orderId,
                                 @Field("orderAmount") String orderAmount,
                                 @Field("name") String name,
                                 @Field("phone") String phone,
                                 @Field("email") String email,
                                 @Field("note") String note,
                                 @Field("orderCurrency") String orderCurrency);*/
}
